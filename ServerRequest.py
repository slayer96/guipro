# -*- coding: utf-8 -*-

import pymongo
import numpy as np
from Constant import *
from LogParser import getDicoIndicateur
import time 
import sklearn
from sklearn import linear_model
from sklearn.decomposition import PCA
from bson.binary import Binary
import pickle
from PyQt4 import QtCore, QtGui
import itertools
import styleSheet
from pymongo.errors import BulkWriteError
from sklearn.metrics import matthews_corrcoef

orderPrincipalOutput = ['Assis', 'Inclinaisonlaterale_Gauche-Droite',
                        'Flexion-Extension', 'Fluiditemouvement_Oui-Non',
                        'Stabilitebassin_Oui-Non', 'Repartitionmouvement_X-Y-Z']


def mergeDict(x, y):
    z = x.copy()
    z.update(y)
    return z


class ServerRequest:
    def __init__(self):
        raise Exception
        try:
            conn=pymongo.MongoClient('dalkoon.com')
            print "Connected successfully!!!"    
        except pymongo.errors.ConnectionFailure, e:
            print "Could not connect to MongoDB: %s" % e
        self.db = conn['koondalTest']
        self.teamKoondal = self.getTeamKoondal()
        self.listOutput = self.getOutput(exist='False')
        listIndicateurlvl = getDicoIndicateur(path=DIRINDICATEURLVL)
        self.listIndicateur = ['Alt',
                               'Ax', 'Ay', 'Az',
                               'Gx', 'Gy', 'Gz',
                               'Tx', 'Ty', 'Tz',
                               'timeStamp']+listIndicateurlvl[10:]

    def addNoise(self, outputName, outputTest):
        outputNoise = outputTest
        bruit = abs(np.random.randn()/5)
        if outputName != 'Repartitionmouvement_X-Y-Z':
            if outputTest == -1:
                outputNoise = (outputTest - bruit)
            elif outputTest == 1:
                outputNoise = (outputTest + bruit)
        else:
            if outputTest == 0:
                outputNoise = (outputTest - bruit)
            elif outputTest == 3:
                outputNoise = (outputTest + bruit)
        return outputNoise
        
    def parseCsvDataAndInsert(self, collection, dataname, y, user, testId, label='Assis'):
        with open(dataname, 'rb') as f:
            print 'open file '+ dataname
            f.readline()
            lines = f.readlines()
            listEntry = []
            i = 0
            
            for line in lines:
                line = str(line).replace('\r', '')
                line = str(line).replace('\n', '')

                newEntry = {}
                newEntry['y'] = float(y)
                newEntry['user'] = user
                newEntry['id'] = testId
                newEntry['x'] = {}

                try:
                    lineFloat = map(float, line.split(','))
                    newEntry['x'] = {self.listIndicateur[i]: lineFloat[i] for i in range(len(self.listIndicateur))}
                except Exception,e:
                    print e
                    print i
                    i += 1
                    continue
                newEntry['yNoise'] = float(self.addNoise(label, newEntry['y']))
                listEntry.append(newEntry)
                i += 1
            if listEntry:
                print 'insert'+str(len(listEntry))+'...'
                i = 0
                bulkop = collection.initialize_unordered_bulk_op()
                for entry in listEntry:
                    i += 1
                    bulkop.insert(entry)
                    
                bulkop.execute()
                print 'after insert'
            
    def uploadTest(self, listTestFile):
        dico, listTest =self.parseAllTestFile(listTestFile,
                                              [output for output in self.listOutput if output not in self.teamKoondal])
        
        n = sum([len(dico[output]) for output in dico.keys() ])
        progress = QtGui.QProgressDialog(u"Envoie du fichier test...", "Cancel", 0, n-1)
        progress.setCancelButton(None)            
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        i = 0
        for output,value in dico.items():
            for tupleValue in value:
                dataname = tupleValue[0]
                y=tupleValue[1]
                user = tupleValue[2]
                testId = tupleValue[3]
                try:
                    self.parseCsvDataAndInsert(self.db[output], dataname, y, user, testId, label=output)
                except BulkWriteError as bwe:
                    print(bwe.code)  
                    print(bwe.message) 
                    print(bwe.args)  
                    print(bwe.details['nInserted'])
                    i += 1
                    print dataname
                    print 'ERRRRRRRRRRRROR  '+str(i)
                    continue
                progress.setValue(progress.value()+1)
                QtCore.QCoreApplication.processEvents()
                
        collection = self.db['test']
        if len(listTest):
            collection.insert(listTest)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
        print 'test uploaded'
        progress.close()

    def getAllTestId(self):
        collection = self.db['test']
        return [l['id'] for l in list(collection.find({}, {'id': 1, '_id': 0}))]
    
    def getTestDescription(self, testId):
        collection = self.db['test']
        test = collection.find_one({'id': str(testId)})
        string = 'Utilisateur:  ' + str(test['user']) + '\n'
        string += 'test ID:  '+ str(test['id']) + '\n\n'
        listOutput = ', '.join(test['output'])
        string += 'Outputs :  '+listOutput + '\n'
        for output in test['output']:
            string += str(output) + ':  ' + str(test[output]) + '\n'
        string += '\n'
        string += 'Description:\n'
        string += test['description'] + '\n'
        string += 'duree:  ' + test['duree'] + '\n'
        return string
        
    def parseAllTestFile(self, listTestFile, listOutputTest):
        
        dicoOutput = {output: []for output in listOutputTest}
        listTest = []
        dicoTest=[]
        for testFile in listTestFile:
            if testFile == 'Directory':
                continue
            dicoOutput, dicoTest = self.parseTest(testFile, dicoOutput, listTest, listOutputTest)
        return dicoOutput, dicoTest
    
    def parseTest(self, filepath, dicoOutput, listTest, listOutputTest):
        dico = dicoOutput
        dicoTmp = {}
        listT = listTest
        description = u''
        desc = False
        update = False
        memberIn = False
        
        with open(filepath, 'rb') as f:
            reader = f.readlines()
            if '/Log' in reader[0]:
                dataname = reader[0].split('/Log')[-1]
                dataname = str(dataname).replace('\r', '')
                dataname = str(dataname).replace('\n', '')
                dataname = DIRPATH + '/Log' + dataname
            else:
                dataname = reader[0].split('/Data/')[-1]
                dataname = str(dataname).replace('\r', '')
                dataname = str(dataname).replace('\n', '')
                dataname = DIRPATH + '/Log/Data/' + dataname
            
            listOutputFormat = [output+':' for output in listOutputTest+self.teamKoondal+['Guest']]
            for row in reader[1:]:
                row = row.replace('\r', '')
                if row.split(' ')[0] == 'Durée:':
                    duree = str(row.split(' ')[-1])[:-1]
                elif row in 'Description:\n':
                    desc = True
                    continue
                elif row.split(' ')[0] == 'TestId:':
                    testId = str(row.split(' ')[-1])[:-1]
                elif row.split(' ')[0] in listOutputFormat:
                    desc = False
                    outputName =  row.split(':')[0]
                    outputTest = float(row.split(' ')[-1])
                    if outputName != 'Repartitionmouvement_X-Y-Z':
                        outputTest = -1 if outputTest == 0 else outputTest
                    dicoTmp[outputName] = outputTest
                    if outputName not in listOutputTest and outputName in self.teamKoondal+['Guest'] and outputTest == 1.0:
                        user = outputName
                        memberIn = True
                    if outputName in listOutputTest:
                        update = True
                else:
                    if desc:
                        description += unicode(row, 'utf-8')
            dicoTest = {}            
            dicoTest['id'] = testId
            dicoTest['description'] = description
            dicoTest['user'] = user
            dicoTest['output'] = [output for output in set(dicoTmp.keys()).intersection(listOutputTest)]
            dicoTest['duree'] = duree
            for output in dicoTest['output']:
                dicoTest[output] = dicoTmp[output]
        
            if self.db['test'].find_one({'id':testId}) == None:
                if update and memberIn:
                    for output in set(dicoTmp.keys()).intersection(listOutputTest):
                        dico[output].append((dataname, dicoTmp[output], user, k2testId))
                listT.append(dicoTest)
            else:
                print 'already in '+str(testId)
            return dico, listT
    
    def getOutput(self, exist='True'):
        collection = self.db['Outputs']
        if exist == 'False':
            listOutputName = [elemOutput['outputName'] for elemOutput in list(collection.find({}, {'outputName': 1, '_id': 0}))]
        else:
            listOutputName = [elemOutput['outputName'] for elemOutput in list(collection.find({'exist': exist}, {'outputName': 1, '_id': 0}))]
        return listOutputName
    
    def getTeamKoondal(self):
        collection = self.db['teamKoondal']
        return [document['name'] for document in list(collection.find())]
    def ServerRemoveOutput(self, output):
        collection = self.db['Outputs']
        if output not in orderPrincipalOutput+['Mourougan', 'Agathe', 'Paul', 'Rouis', 'Tarik', 'Maxime']:
            collection.remove({'outputName':output})
    
    def ServerAddOutput(self,output):
        collection = self.db['Outputs']
        elemOutput = {'outputName': output,
                      'exist': 'False'}
        if list(collection.find({'outputName': output})) == []:
            print 'insert output'
            collection.insert(elemOutput)
            
    def requestRegression(self, output, listIndicateur, subsetUser, pca, typeRegression, progress=None):
        collectionOutput = self.db[output]
        cursorMX = collectionOutput.find({'user': {"$in": subsetUser}}, mergeDict({'x.'+indicateur: 1 for indicateur in listIndicateur}, {'_id': 0}))
        cursorMY = collectionOutput.find({'user': {"$in": subsetUser}}, {'y': 1, '_id': 0})
        cursorMYNoise = collectionOutput.find({'user': {"$in": subsetUser}}, {'yNoise': 1, '_id': 0})
        
        print 'Requete serveur en cours'

        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        arrayDictX = [line['x'] for line in list(cursorMX)]
        print 'arratDictX done'
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        if len(arrayDictX) == 0:
            return {'fonction': None,
                'pca': None,
                'scoreIn': None,
                'scoreOut': None,
                'nbTest': None,
                'advice': u'Conseil: Assurer de téléverser vos tests (Onglet de Test)',
                'error': u'Aucun test trouvé pour ces personnes'}
        arrayX = np.array([[line[indicateur]for indicateur in listIndicateur] for line in arrayDictX])
        progress.setValue(progress.value()+1)
        print 'arrayX done'
        QtCore.QCoreApplication.processEvents()
        arrayY = np.array([line['y'] for line in list(cursorMY)])
        print 'arrayY done'
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        arrayYNoise = np.array([line['yNoise'] for line in list(cursorMYNoise)])
        print 'arrayYNoise done'
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        middle = len(arrayX)/2

        #equilibrage et meleange
        ratio = None
        if output != 'Repartitionmouvement_X-Y-Z':
            listIndexTrue = []
            listIndexFalse = []
            for i in range(len(arrayY)):
                if arrayY[i] == 1:
                    listIndexTrue += [i]
                else:
                    listIndexFalse += [i]
            print 'separete True from FalseDone'
            nFalse =len(listIndexFalse)
            nTrue = len(listIndexTrue)
            if nFalse ==0 or nTrue == 0:
                #pas de test
                return {'fonction': None,
                'pca': None,
                'scoreIn': None,
                'scoreOut': None,
                'nbTest': None,
                'advice': u'Conseil: Assurer de téléverser vos tests (Onglet de Test) et vérifier si vous avez bien fait des tests Vrai ET Faux',
                'error': 'Pas assez de test Vrai ou de test Faux', }
            print 'shuffle matrice'
            np.random.shuffle(listIndexFalse)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            np.random.shuffle(listIndexTrue)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            print 'shuffle both matrice done'

            minSize = min(nFalse,nTrue)
            iters = [iter(listIndexFalse[:minSize]), iter(listIndexTrue[:minSize])]
            listAllIndexes = list(it.next() for it in itertools.cycle(iters))
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            if max(nFalse, nTrue)/minSize > 1.3:
                ratio = float(max(nFalse, nTrue))/float(minSize)
                biggestResponse = 'True' if nTrue >= nFalse else 'False'
                lowestResponse= 'False' if nTrue >= nFalse else 'True'
            
            print 'Two list merge done'
            arrayX = [arrayX[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            
            arrayY = [arrayY[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            
            arrayYNoise = [arrayYNoise[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
        
            print 'three Array re ordonned done'

        print len(arrayX)
        print len(arrayY)
        middle = len(arrayX)/2
        #découpage en training et testingSet
        trainingX = arrayX[:middle]
        trainingY = arrayY[:middle]
        trainingYNoise = arrayYNoise[:middle]
        testingX = arrayX[middle:]
        testingY = arrayY[middle:]
        
        dico = self.regression(trainingX, trainingY, trainingYNoise, typeRegression, testingX=testingX, testingY=testingY,
                               pcaComponent=pca, output=output, subsetUser=subsetUser, indicateurNames=listIndicateur)
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()        
        nbTestList = list(self.db['test'].find({'output': {'$in': [output]}, 'user': {"$in": subsetUser}}, {'id':1, '_id': 0}))
        dico['nbTest'] = len(nbTestList)
        dico['error'] = None
        if ratio is not None:
            ratio = str("%.2f ") % ratio
            dico['advice'] =u'Warning: La quantité de test('+output+') ' + biggestResponse + \
                            ' est ' + ratio + ' fois plus grand que '+ lowestResponse
        else:
            dico['advice'] = None
        self.db['Outputs'].find_and_modify({'outputName': output}, {'$set': {'exist': 'True'}})
        return dico
        
    def regression(self, trainingX, trainingY, trainingYNoise, typeRegression, testingX=[], testingY=[],
                   pcaComponent=0,output='Assis', subsetUser=['Mourougan','Tarik','Paul'],
                   indicateurNames=['alt','ax','ay','az','gx','gy','gz','tx','ty','tz']):
        if pcaComponent == 0:
            pca = None
        else:
            pca = PCA(pcaComponent)
            trainingX = pca.fit_transform(trainingX)
            testingX = pca.fit_transform(testingX)
        print 'print wait for fitting'
        if typeRegression == 'Ridge':
            # On utilise une array Y avec du bruit
            regressionFonction = linear_model.Ridge(normalize=True)
            regressionFonction.fit(trainingX, trainingYNoise)
        elif typeRegression == 'Logistiquecv':
            regressionFonction = linear_model.LogisticRegressionCV(penalty='l2', tol=0.0001)
            regressionFonction.fit(trainingX, trainingY)
        elif typeRegression == 'Logistique':
            regressionFonction = linear_model.LogisticRegression(penalty='l2', tol=0.0001)
            regressionFonction.fit(trainingX, trainingY)
        else:
            regressionFonction = linear_model.LogisticRegression(penalty='l2', tol=0.0001)
            regressionFonction.fit(trainingX, trainingY)
        
        if typeRegression == 'Logistique' or typeRegression == 'LogistiqueCV':
            scoreIn = regressionFonction.score(trainingX, trainingY)
            arrayY_pred = regressionFonction.predict(testingX)
            scoreOut = matthews_corrcoef(testingY, arrayY_pred)
        else:
            scoreIn = regressionFonction.score(trainingX,trainingY)
            arrayY_pred = regressionFonction.predict(testingX) 
            scoreOut = matthews_corrcoef(testingY, arrayY_pred)

        dicoFonction = {output:regressionFonction}
        
        dico = {'fonction': dicoFonction,
                'pca': pca,
                'scoreIn': scoreIn,
                'scoreOut': scoreOut,
        }
        return dico
    
    def createConfigurationFonction(self, regressionFonction, output, scoreIn, scoreOut, sampleUser, indicateurNames, pca):
        scoreCoeff = {}
        if isinstance(regressionFonction, sklearn.linear_model.Ridge):
            scoreCoeff['0'] = {indicateurNames[i]: regressionFonction.coef_[i] for i in range(len(regressionFonction.coef_))}
        else:
            for classeIndice in range(len(regressionFonction.coef_)):
                scoreCoeff[str(classeIndice)] = {indicateurNames[i]: regressionFonction.coef_[classeIndice][i] for i in range(len(regressionFonction.coef_[classeIndice]))}
        
        regressionByte = pickle.dumps(regressionFonction)
        pcaByte = pickle.dumps(pca)
        configuration = {}
        configuration['output'] = output
        configuration['scoreIn'] = scoreIn
        configuration['scoreOut'] = scoreOut
        configuration['subsetUser'] = sampleUser
        configuration['indicateurNames'] = indicateurNames
        configuration['fonction'] = Binary(regressionByte)
        configuration['pca'] = Binary(pcaByte)
        configuration['coefficient'] = scoreCoeff
        configuration['timestamp'] = time.time()
        
        return configuration

    def downloadFonction(self, output, user):
        try:
            col = self.db[user]
            bestFonction = col.find_one({'outputName': output})['array']
        except:
            return None
        fonction = bestFonction[0]['fonction']
        fonction = pickle.loads(fonction)
        return fonction
        
    def downloadPCA(self, output, user):
        try:
            col = self.db[user]
            bestPCA = col.find_one({'outputName': output})['array']
        except:
            return None
        pca = bestPCA[0]['pca']
        pca = pickle.loads(pca)
        return pca
   
    def downloadIndicateur(self,output,user):
        try:
            col = self.db[user]
            bestFonction = col.find_one({'outputName': output})['array']
        except:
            #requete na rien trouver , exemple:  cette fonction n'existe pas
            return None
        return bestFonction[0]['indicateurNames']
