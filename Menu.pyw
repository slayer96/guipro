# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 09:55:31 2015

@author: Mourougan
"""

# coding: utf-8
import sys

import time
from datetime import datetime
import csv
import pandas
import random
import shutil
import serial.tools.list_ports

from Constant import *
import Capteur
import Data
from ServerRequest import ServerRequest
import OfflineRequest
from LogParser import parseOutput, copyanything, createFileScore, getDicoIndicateur, dumpDicoIndicateur, \
    dumpListMember, getListMember, saveFunctionConfiguration, getFunctionByLevel
import styleSheet
import WidgetTest

import numpy as np

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QThread
from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QCheckBox
from PyQt4.QtGui import QComboBox
from PyQt4.QtGui import QFont
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

cmdToSend = ""
# DIRPATH =os.path.dirname(os.path.realpath(__file__))

with open(DIRPATH + '/Compte/ListUser.txt', 'rb') as f:
    teamKoondal = f.readlines()
teamKoondal = map(lambda name: name.replace('\n', ''), teamKoondal)
teamKoondal = map(lambda name: name.replace('\r', ''), teamKoondal)

orderPrincipalOutput = ['Assis', 'Inclinaisonlaterale_Gauche-Droite', 'Flexion-Extension', 'Fluiditemouvement_Oui-Non',
                        'Stabilitebassin_Oui-Non', 'Repartitionmouvement_X-Y-Z']

try:
    print 'start'
except:
    print 'error connection Serveur'
    pass

''' 
Purpose: ecrit toute les lignes de 'lines' dans le pathFile, avec comme premiere ligne les parametre title'
'''


def writeAllLineInFile(lines, pathFile, title=[]):
    if len(lines):
        try:
            f = open(pathFile, 'wb')
            writer = csv.writer(f)
            if title:
                writer.writerow(title)
            for line in lines:
                writer.writerow(line)
        finally:
            f.close()


'''
Purpose parse a dat file CSV and return a dictionary of list, of all Value
'''


def parseFile(filename):
    with open(filename) as f:
        listVariable = f.readline()[:-1].split(',')
    try:
        fileCsv = pandas.read_csv(filename)
        dicoList = {}
        for indicateurBrut in listVariable:
            dicoList[indicateurBrut] = fileCsv[indicateurBrut]

        return dicoList
    except Exception, e:
        print 'Error in parseFile : ' + e


'''
Purpose: general methode pour mettre a jour un minuteur
'''


def updateMinuteur(label, seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    label.repaint()
    label.setText("%d:%02d:%02d" % (h, m, s))


""" 
Purpose: creer un checkboxpersonel et le connect a une fonction
Param : connectFoction; fonction a connecter, check: 1 veux dire coché et 0sinon, label:label 
Return:     return a checkbox widget
"""


def createCheckbox(classObject, label, connectFonction, check=1):
    checkBox = QCheckBox(label)
    checkBox.setChecked(check)
    checkBox.setFont(QFont("Arial", pointSize=12, weight=QFont.Bold))
    if connectFonction != None:
        classObject.connect(checkBox, SIGNAL("clicked()"), connectFonction)
    return checkBox


'''
Purpose: creer un checkboxpersonel et le connect a une fonction
Param : connectFoction; fonction a connecter, check: 1 veux dire coché et 0sinon, label:label 
Return : personnel combo box
'''


def createComboBox(listItem):
    comboBox = QComboBox(parent=None)
    for item in listItem:
        comboBox.addItem(item, userData=None)
    return comboBox


'''
Purpose : Thread démarer par widget user qui prend des lignes du capteur et les renvoie par signal, qui sont attrapé par les widget user et widget graphe
'''


class WorkerUser(QThread):
    def __init__(self, capteur, parent=None):

        QThread.__init__(self, parent)
        self.exiting = False
        self.capteur = capteur

    def __del__(self):

        self.exiting = True
        if self.capteur != 'capteur':
            self.capteur.disconnect()
            self.terminate()

    def run(self):
        if not self.exiting:
            try:
                self.capteur = Capteur.Capteur()
            except Exception, e:
                if str(e) == 'NOPORT':
                    self.emit(SIGNAL("output(QString)"), str(e))
                else:
                    self.emit(SIGNAL("output(QString)"), 'ERRORWINDOWS')
                return
            self.exiting = True
        lastLine = ''
        cpt = 0
        t = time.time()
        while True:
            cpt += 1
            try:
                if cmdToSend == "L":
                    self.capteur.stopLive()
                    time.sleep(0.01)
                    continue
                line = self.capteur.askForOneLine()
                time.sleep(0.01)
                if line is None:
                    continue

                if len(line) != 11:
                    continue

            except Exception, e:
                print e
                self.emit(SIGNAL("output(QString)"), str(e))
                if str(e) == 'TIMEOUT' or str(e) == 'INACTIF':
                    return
            map(str, line)
            string = ':'.join(line)
            if lastLine != string:
                print time.time() - t
                t = time.time()
                self.emit(SIGNAL("output(QString)"), string)
                lastLine = string


'''
Purpose : Widget principal qui affiche la capteur de capteur
'''


class WidgetUser(QtGui.QWidget):
    def __init__(self, parent=None, qThread=None, qData='fake', serverRequest=None):
        super(WidgetUser, self).__init__()
        self.thread = qThread
        self.data = qData

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.boolUpdate = True
        self.initUI()

    def raiseError(self, errorType, errorMessage):
        result = QtGui.QMessageBox.question(errorType + "...", errorMessage,
                                            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)

        if result == QtGui.QMessageBox.Ok:
            exit()

    def initUI(self, user='Guest'):
        self.lastUpdate = time.time()
        self.button = QtGui.QPushButton('CONNECTION KOONDAL', self)
        self.button.clicked[bool].connect(self.capture)
        self.buttonStop = QtGui.QPushButton('DECONNECTION KOONDAL', self)
        self.buttonStop.clicked[bool].connect(self.stop)
        self.buttonStop.setEnabled(False)
        self.comboboxUser = createComboBox(['Guest'] + sorted([member for member in teamKoondal if member != 'Guest']))
        self.buttonConnection = QtGui.QPushButton('CONNECTION USER', self)
        self.buttonConnection.clicked[bool].connect(self.changeUser)
        self.buttonSaveFunction = QtGui.QPushButton('Save functions', self)
        self.buttonSaveFunction.clicked[bool].connect(self.saveFunction)
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput(checkScore=True).keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        listOutput = self.outputPrimaire + teamKoondal
        listOutput2 = [output for output in listOutput if output not in teamKoondal]
        self.labelOutput = {output: QtGui.QLabel(output) for output in listOutput2}
        self.reponseCouleurOutput = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseCouleurOutputNEW = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseCouleurOutputSERVER = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseChiffreOutput = {output: QtGui.QLabel('0') for output in listOutput2}
        self.reponseChiffreOutputNEW = {output: QtGui.QLabel('0') for output in listOutput2}
        self.reponseChiffreOutputSERVER = {output: QtGui.QLabel('0') for output in listOutput2}

        for output in self.labelOutput.keys():
            self.labelOutput[output].setObjectName('labelPageOne')
            self.reponseChiffreOutput[output].setObjectName('labelPageOne')
            self.reponseChiffreOutputNEW[output].setObjectName('labelPageOne')
            self.reponseCouleurOutput[output].setObjectName('reponsePageOne')
            self.reponseCouleurOutputNEW[output].setObjectName('reponsePageOne')
            self.reponseCouleurOutputSERVER[output].setObjectName('reponsePageOne')
            self.reponseChiffreOutputSERVER[output].setObjectName('labelPageOne')

            self.reponseCouleurOutput[output].setMinimumWidth(self.labelOutput[output].width() / 5)
            self.reponseCouleurOutputNEW[output].setMinimumWidth(self.labelOutput[output].width() / 5)
            self.reponseCouleurOutputSERVER[output].setMinimumWidth(self.labelOutput[output].width() / 5)

        gridIntern = QtGui.QGridLayout()
        gridIntern.setSpacing(0)

        v0 = QtGui.QHBoxLayout()
        h0 = QtGui.QHBoxLayout()
        v1 = QtGui.QVBoxLayout()
        v2 = QtGui.QVBoxLayout()
        v3 = QtGui.QVBoxLayout()
        v4 = QtGui.QVBoxLayout()
        v5 = QtGui.QVBoxLayout()
        v6 = QtGui.QVBoxLayout()

        v0.setSpacing(0)
        v1.setSpacing(0)
        v2.setSpacing(0)
        v3.setSpacing(0)
        v4.setSpacing(0)
        v5.setSpacing(0)
        for output in listOutput2:
            h = QtGui.QHBoxLayout()
            h.addWidget(self.reponseCouleurOutput[output])
            h.addWidget(self.reponseCouleurOutputNEW[output])
            h.addWidget(self.reponseCouleurOutputSERVER[output])
            v1.addWidget(self.labelOutput[output])
            v2.addLayout(h)
            v3.addWidget(self.reponseChiffreOutput[output])
            v4.addWidget(self.reponseChiffreOutputNEW[output])
            v5.addWidget(self.reponseChiffreOutputSERVER[output])

        v0.addWidget(self.comboboxUser)
        v0.addWidget(self.buttonConnection)
        self.labelConnected = QtGui.QLabel('Guest')
        self.labelConnected.setObjectName('user')
        self.initUserGuest()
        h0.addWidget(QtGui.QLabel(u'Connecté en tant que:   '))
        h0.addWidget(self.labelConnected)

        hmodeLabel = QtGui.QHBoxLayout()
        hmodeLabel.addWidget(QtGui.QLabel(u'Serveur:'))
        modelabel = QtGui.QLabel('Online') if self.online else QtGui.QLabel('Offline')
        modelabel.setStyleSheet("color:green") if self.online else modelabel.setStyleSheet("color:red")
        hmodeLabel.addWidget(modelabel)
        labelUser = QtGui.QLabel(u'Utilisateur détecté')
        self.currentUserNAME = QtGui.QLabel('XXX')
        self.currentUserCHIFFRE = QtGui.QLabel('0')
        self.currentUserCHIFFRENEW = QtGui.QLabel('0')
        self.currentUserCHIFFRESERVER = QtGui.QLabel('0')
        self.currentUserNAME.setObjectName('reponsePageOne')
        self.currentUserCHIFFRE.setObjectName('reponsePageOne')
        self.currentUserCHIFFRENEW.setObjectName('reponsePageOne')
        self.currentUserCHIFFRESERVER.setObjectName('reponsePageOne')
        labelUser.setObjectName('reponsePageOne')
        v1.addWidget(labelUser)
        v2.addWidget(self.currentUserNAME)
        v3.addWidget(self.currentUserCHIFFRE)
        v4.addWidget(self.currentUserCHIFFRENEW)
        v5.addWidget(self.currentUserCHIFFRESERVER)

        v6.addWidget(self.button)
        v6.addWidget(self.buttonStop)

        groupOutput = QtGui.QGroupBox('OUTPUT')
        groupOutput.setLayout(v1)
        groupReponseIA = QtGui.QGroupBox('REPONSE IA(Interpretation)')
        groupReponseIA.setLayout(v2)
        groupReponseIAC = QtGui.QGroupBox('REPONSE IA BEST')
        groupReponseIAC.setLayout(v3)
        groupReponseIACNEW = QtGui.QGroupBox('REPONSE IA NORMAL')
        groupReponseIACNEW.setLayout(v4)
        groupReponseIACSERVER = QtGui.QGroupBox('REPONSE IA SERVER')
        groupReponseIACSERVER.setLayout(v5)
        gridIntern.setRowMinimumHeight(1, 30)
        gridIntern.addLayout(v0, 0, 8, 1, 1)
        gridIntern.addLayout(hmodeLabel, 0, 0, 1, 1)
        gridIntern.addLayout(h0, 1, 8, 1, 1)
        # gridIntern.addWidget(self.buttonConnection,0,6,1,1)
        gridIntern.addWidget(groupOutput, 2, 0, 10, 1)
        gridIntern.addWidget(groupReponseIA, 2, 2, 10, 1)
        gridIntern.addWidget(groupReponseIAC, 2, 4, 10, 1)
        gridIntern.addWidget(groupReponseIACNEW, 2, 6, 10, 1)
        gridIntern.addWidget(groupReponseIACSERVER, 2, 8, 10, 1)
        gridIntern.addWidget(self.buttonSaveFunction, 12, 6, 1, 1)
        gridIntern.addLayout(v6, 13, 8, 1, 1)

        self.setLayout(gridIntern)
        return gridIntern

    def connecting(self):
        self.connect(self.thread, SIGNAL("output(QString)"), self.updateUi)

    def disconnecting(self):
        self.disconnect(self.thread, SIGNAL("output(QString)"), self.updateUi)

    def saveFunction(self):
        user = str(self.labelConnected.text())
        d = datetime.now()
        for output in self.outputPrimaire:
            nameSrc = DIRPATH + '/Compte/' + user + '/' + output
            nameDst = DIRPATH + '/Backup/' + str(d).replace(' ', '_').replace(':', '-')[:-7] + '/' + output
            copyanything(nameSrc, nameDst)

    def changeUser(self):
        self.stop()
        progress = QtGui.QProgressDialog(u"Téléchargement des nouvelles information ...", "Cancel", 0, 0)
        progress.setCancelButton(None)
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        QtCore.QCoreApplication.processEvents()
        self.labelConnected.setText(self.comboboxUser.currentText())
        self.data.initialize(user=str(self.comboboxUser.currentText()))
        self.button.setEnabled(True)
        self.buttonStop.setEnabled(False)
        progress.close()

    def initUserGuest(self):
        '''
        with open(DIRPATH+'/Compte/CurrentUser.txt','wb') as f:
            f.write('Guest')
        '''

    def stopUpdate(self):
        self.boolUpdate = False

    def startUpdate(self):
        self.boolUpdate = True

    def updateUi(self, line):
        if not self.boolUpdate:
            return
        if line == 'NOPORT':
            self.raiseError('NOPORT', 'Aucun dongle koondal trouver, veuillez redemarrer')
        elif line == 'ERRORWINDOWS':
            self.raiseError('Erreur koondal',
                            (
                            'Error de port windows, veuillez redemarer l\'application \n et changer le dongle de port').encode(
                                "utf-8"))
        elif line == 'TIMEOUT':
            self.raiseError('DECONNEXION', 'Dongle deconnecte')
            self.thread.sleep()
        elif line == 'INACTIF':
            self.raiseError('DECONNEXION',
                            'Koondal inacitf trop longtemps, raison :\n\t-plus de batterie\n\t-distance trop longue pendant une longue period\nVeuillez redemarer')
            self.thread.sleep()
        elif line == 'BADPORT':
            self.raiseError('MAUVAIS PORT', 'Erreur de port de la carte, veuillez redemarrer')
        ll = line.split(':')
        # obtention des reponse
        # print time.time() - self.lastUpdate
        dicoReponse = self.data.updateDataAndReturnAnswer(ll)
        self.lastUpdate = time.time()
        state = self.data.getState()
        for output in self.outputPrimaire:
            outputInServer = len(state[output]) == 3
            if dicoReponse[output] == None or output in teamKoondal:
                self.reponseCouleurOutput[output].repaint()
                self.reponseChiffreOutput[output].repaint()
                self.reponseChiffreOutputNEW[output].repaint()
                self.reponseCouleurOutput[output].setText('XXX')
                self.reponseCouleurOutputNEW[output].setText('XXX')
                self.reponseCouleurOutputSERVER[output].setText('XXX')
                self.reponseChiffreOutput[output].setText('XXX')
                self.reponseChiffreOutputNEW[output].setText('XXX')
                self.reponseChiffreOutputSERVER[output].setText('XXX')
                self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                continue

            self.reponseCouleurOutput[output].repaint()
            self.reponseCouleurOutputNEW[output].repaint()
            self.reponseChiffreOutput[output].repaint()
            self.reponseChiffreOutputNEW[output].repaint()
            self.reponseChiffreOutputSERVER[output].repaint()
            if output == 'Inclinaisonlaterale_Gauche-Droite':
                if state[output][0] == ' ... ':
                    self.reponseCouleurOutput[output].setText('Gauche')
                else:
                    self.reponseCouleurOutput[output].setText('Droite')
                if state[output][1] == ' ... ':
                    self.reponseCouleurOutputNEW[output].setText('Gauche')
                else:
                    self.reponseCouleurOutputNEW[output].setText('Droite')

            elif output == 'Flexion-Extension':
                if state[output][0] == ' ... ':
                    self.reponseCouleurOutput[output].setText('Flexion')
                else:
                    self.reponseCouleurOutput[output].setText('Extension')
                if state[output][1] == ' ... ':
                    self.reponseCouleurOutputNEW[output].setText('Flexion')
                else:
                    self.reponseCouleurOutputNEW[output].setText('Extension')
            else:
                if output != 'Assis':
                    if state[output][0] == ' ... ':
                        self.reponseCouleurOutput[output].setText('Non')
                    else:
                        self.reponseCouleurOutput[output].setText('Oui')
                    if state[output][1] == ' ... ':
                        self.reponseCouleurOutputNEW[output].setText('Non')
                    else:
                        self.reponseCouleurOutputNEW[output].setText('Oui')
                else:
                    self.reponseCouleurOutput[output].setText(state[output][0])
                    self.reponseCouleurOutputNEW[output].setText(state[output][1])
            if outputInServer:
                # self.reponseCouleurOutputSERVER[output].repaint()
                # self.reponseCouleurOutputSERVER[output].setText(state[output][2])
                if output == 'Inclinaisonlaterale_Gauche-Droite':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Gauche')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Droite')

                elif output == 'Flexion-Extension':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Flexion')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Extension')
                elif output != 'Assis':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Non')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Oui')
                else:
                    self.reponseCouleurOutputSERVER[output].setText(state[output][2])

            self.reponseChiffreOutput[output].setText(str(dicoReponse[output][0]))
            self.reponseChiffreOutputNEW[output].setText(str(dicoReponse[output][1]))
            self.reponseChiffreOutputSERVER[output].setText(str(dicoReponse[output][2]))

            for i in range(len(state[output])):
                if output == 'Repartitionmouvement_X-Y-Z':
                    if state[output][i] == '0':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:green")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:green")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:green")
                    elif state[output][i] == '1':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:yellow")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:yellow")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:yellow")

                    elif state[output][i] == '2':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:orange")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:orange")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:orange")
                    elif state[output][i] == '3':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:red")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:red")
                else:
                    if state[output][i] != ' ... ':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:green")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:green")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:green")
                    else:
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:red")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:red")

        self.currentUserNAME.repaint()
        self.currentUserCHIFFRE.repaint()
        self.currentUserCHIFFRENEW.repaint()
        self.currentUserCHIFFRESERVER.repaint()
        teamKoondalNoGuest = [member for member in teamKoondal if member not in ['Guest', 'Maxime']]
        maxUserChiffre = max([dicoReponse[team][0] for team in teamKoondalNoGuest])
        maxUserChiffreNew = max([dicoReponse[team][1][1] for team in teamKoondalNoGuest])
        for key, duo in dicoReponse.items():
            if key not in teamKoondalNoGuest:
                continue
            if duo[0] == maxUserChiffre:
                name = key
        teamReponse = {dicoReponse[team][1][1]: team for team in teamKoondalNoGuest}
        self.currentUserCHIFFRE.setText(str(maxUserChiffre))
        self.currentUserCHIFFRENEW.setText(str(teamReponse[maxUserChiffreNew]))
        self.currentUserCHIFFRESERVER.setText(str(teamReponse[maxUserChiffreNew]))
        self.currentUserNAME.setText(name)

    def stop(self):
        global cmdToSend
        cmdToSend = "L"
        self.button.setEnabled(True)
        self.buttonStop.setEnabled(False)
        self.disconnecting()
        # self.stopped = True
        # self.thread.wait()

    def capture(self):
        global cmdToSend
        self.button.setEnabled(False)
        self.buttonStop.setEnabled(True)
        self.connecting()
        cmdToSend = "l"
        self.thread.start()
        self.t0 = time.time()


'''
Purpose : widget qui represente un plot
'''


class MatplotlibWidget(QtGui.QWidget):
    def __init__(self, parent=None, yName=' ', xName='', ymin=-10, ymax=10):
        super(MatplotlibWidget, self).__init__(parent)

        self.figure = Figure()
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.axis = self.figure.add_subplot(111, xlabel='Time(sec)', ylabel=yName)

        self.figure.subplots_adjust(left=0.08, bottom=0.12, right=0.90, top=0.98)
        self.axis.minorticks_on()
        self.axis.grid(True, which='both')
        self.layoutVertical = QtGui.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.canvas)
        self.setMinimumHeight(400)

    def savefig(self, filePath):
        originalInch = self.figure.get_size_inches()
        self.figure.set_size_inches(18.5, 10.5)
        self.figure.savefig(filePath)
        self.figure.set_size_inches(originalInch)


'''
Purpose : Widget qui est la page d'affichage temps reel de la courbe + enregistrement
'''


class WidgetPlot(QtGui.QWidget):
    def __init__(self, parent=None, qThread=None, qData='fake', serverRequest=None):
        super(WidgetPlot, self).__init__()
        self.dicoAllIndicateur = getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p')
        self.t0 = time.time()
        self.cpt = 0
        self.firstPoint = True
        self.thread = qThread
        self.connected = False
        self.currentPointNumber = 0
        self.MaxPoint = 100
        self.recordBool = False
        self.llAx = [0] * self.MaxPoint
        self.llAy = [0] * self.MaxPoint
        self.llAz = [0] * self.MaxPoint
        self.dicoF = {}
        self.fonctionAdded = False
        self.data = qData

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.initUI()

    def connecting(self):
        self.connect(self.thread, SIGNAL("output(QString)"), self.updateGraphe)

    def disconnecting(self):
        self.disconnect(self.thread, SIGNAL("output(QString)"), self.updateGraphe)

    def raiseError(self, errorType, errorMessage):
        result = QtGui.QMessageBox.question(self, errorType + "...", errorMessage,
                                            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        if result == QtGui.QMessageBox.Ok:
            exit()

    '''
    Purpose : sauvegarde d'un fichier test pour description , ainsi que la ligne de test dans test.csv
    '''

    def saveFile(self, path=None):
        try:
            if path == None:
                filename, extension = QtGui.QFileDialog.getSaveFileNameAndFilter(self, 'Save File',
                                                                                 DIRPATH + '/TestRecord',
                                                                                 filter=QApplication.translate(
                                                                                     'etmEditor', 'txt File (*.txt)'))
            else:
                filename = path
        except:
            print 'error saveFile'
        else:
            try:
                f = open(filename, 'w')
                f.write(self.dataFileName + '\n')
                f.write('Durée: ')
                seconds = float(time.time()) - float(self.lastT0)
                m, s = divmod(seconds, 60)
                h, m = divmod(m, 60)
                f.write("%d:%02d:%02d\n" % (h, m, s))
                f.write('Description:\n')
                f.write(unicode(self.description.toPlainText()).encode("utf-8") + '\n')
                assis = '1' if str(self.dicoComboxOutput['Assis'].currentText()) == 'Oui' else '-1'
                repartitionMouvement = str(self.comboboxRepartitionMouvement.currentIndex() - 1)
                if str(self.dicoComboxOutput['Assis'].currentText()) != 'Pas test':
                    f.write('Assis:  ')
                    f.write(assis + '\n')
                if str(self.comboboxRepartitionMouvement.currentText()) != 'Pas test':
                    f.write('Repartitionmouvement_X-Y-Z:  ')
                    f.write(str(repartitionMouvement) + '\n')
                if str(self.comboboxFlexionExtension.currentText()) != 'Pas test':
                    f.write('Flexion-Extension:  ')
                    value = str(self.comboboxFlexionExtension.currentIndex() - 1)
                    value = '1' if value == '1' else '-1'
                    f.write(str(value) + '\n')
                if str(self.comboboxInclinaisonLateral.currentText()) != 'Pas test':
                    f.write('Inclinaisonlaterale_Gauche-Droite:  ')
                    value = str(self.comboboxInclinaisonLateral.currentIndex() - 1)
                    value = '1' if value == '1' else '-1'
                    f.write(str(value) + '\n')
                for output, combox in self.dicoComboxOutput.items():
                    if output != 'Assis' and str(combox.currentText()) != 'Pas test':
                        f.write(output + ':  ')
                        f.write('1\n' if str(combox.currentText()) == 'Oui' else '-1\n')
                # user part
                '''
                with open(DIRPATH+'/Compte/CurrentUser.txt','rb')as fUser:
                    currentUser = str(fUser.readline())
                '''
                currentUser = 'Guest' if self.userCombox.currentIndex() == 0 else self.userCombox.currentText()
                f.write(currentUser + ':  1\n')
                for user in teamKoondal + ['Guest']:
                    if user != currentUser:
                        f.write(user + ':  -1\n')
                f.write('TestId:  ' + str(int(time.time())) + '\n')
                f.close()
            except Exception, e:
                print e
                print 'first file fail'
            else:
                f.close()
                with open(DIRPATH + '/Log/RefreshData/refreshData.txt', 'ab') as f:
                    f.write(self.dataFileName + '\n')
                return filename

    def record(self):
        if self.recordBool == True:
            self.recordBool = not self.recordBool
            self.button.repaint()
            self.button.setText("Start test")
            updateMinuteur(self.minuteur, 0)
            self.createRecord()
        else:
            self.createTestDirectory()
            self.button.repaint()
            self.button.setText("Stop")
            self.lastT0 = time.time()
            self.t0 = time.time()
            self.numberOfFileTest = 0
            self.lines = []
            self.recordBool = not self.recordBool

    def createTestDirectory(self):
        d = datetime.now()
        self.nameDir = DIRTESTRECORD + '/' + str(d).replace(' ', '_').replace(':', '-')[:-7]
        os.makedirs(self.nameDir)

    def createRecord(self):
        print 'create part ' + str(self.numberOfFileTest)
        self.createFileData()
        self.saveFile(path=self.nameDir + '/Part_' + str(self.numberOfFileTest) + '.txt')
        self.numberOfFileTest += 1
        self.lastT0 = time.time()

    def createFileData(self):
        listParam = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz', 'timeStamp'] + \
                    self.data.getListCurrentOutput()
        self.dataFileName = DIRPATH + '/Log/Data/Data' + str(time.time()) + '.csv'
        lines = self.lines
        writeAllLineInFile(lines, self.dataFileName, title=listParam)

    def initUI(self):
        self.dataVariable = {}
        self.dicoF = {}
        self.plot1 = MatplotlibWidget(self, yName='Acceleration(m./s2)', ymin=-45000, ymax=45000)
        self.navi_toolbar1 = NavigationToolbar(self.plot1.canvas, self)
        l = self.navi_toolbar1.actions()
        for i in l:
            if i.text() == 'Save':
                customizeAction = i
            if i.text() == 'Subplots':
                subplotAction = i
        self.navi_toolbar1.removeAction(customizeAction)
        self.navi_toolbar1.removeAction(subplotAction)
        self.lastMaxPoint = []
        self.lastMinPoint = []
        box = self.plot1.axis.get_position()
        self.plot1.axis.set_position([box.x0, box.y0, box.width * 0.85, box.height])

        self.currentCourbe = 'Accelerometre'
        self.minuteur = QtGui.QLabel("00:00:00")
        self.button = QtGui.QPushButton('Start Test', self)
        self.button.clicked[bool].connect(self.record)
        self.button.setFixedWidth(100)

        grid = QtGui.QGridLayout()
        grid.setSpacing(1)
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput().keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        legendLabel1 = QtGui.QLabel('                 ')
        if self.online:
            listOutputNoCheck = self.serverRequest.getOutput(exist='False')
        else:
            listOutputNoCheck = parseOutput(checkScore=False).keys()
        listAllIndicateur = sorted(getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p').keys())
        self.comboxCourbe1 = createComboBox(['Giroscope', 'Accelerometre', 'Altimetre', 'Magnetometre'] +
                                            self.listOutput + listAllIndicateur)
        self.buttonCourbe1 = QtGui.QPushButton('ajouter', self)
        # self.buttonCourbe1.clicked[bool].connect(self.switchCourbe1)
        self.buttonCourbe1.clicked[bool].connect(self.addFonction)
        self.comboxCourbe1.setCurrentIndex(1)

        buttonHideShow = QtGui.QPushButton('HIDE/SHOW test', self)
        buttonHideShow.clicked[bool].connect(self.hideShow)
        buttonPauseStart = QtGui.QPushButton('PAUSE/START test', self)
        buttonPauseStart.clicked[bool].connect(self.pauseStart)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        self.widgetFonction = QtGui.QWidget()

        self.verticalFonction = QtGui.QVBoxLayout()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)
        groupFonction.setMaximumWidth(200)

        self.dicoComboxOutput = {output: createComboBox(['Pas test', 'Non', 'Oui']) for output in listOutputNoCheck if
                                 output not in ['Flexion-Extension', 'Inclinaisonlaterale_Gauche-Droite',
                                                'Repartitionmouvement_X-Y-Z'] + teamKoondal}
        self.userCombox = createComboBox([u'utilisateur invité'] + teamKoondal)
        self.levelRepartitionMouvement = ['Pas test', 'Frontale', 'Sagitale', 'Transversal']
        self.comboboxRepartitionMouvement = createComboBox(self.levelRepartitionMouvement)
        repartionMouvementLabel = QtGui.QLabel('Repartitionmouvement_X-Y-Z:')
        self.comboboxInclinaisonLateral = createComboBox(['Pas test', 'Gauche', 'Droite'])
        inclinaisonLateralLabel = QtGui.QLabel('Inclinaisonlaterale_Gauche-Droite:')
        self.comboboxFlexionExtension = createComboBox(['Pas test', 'Flexion', 'Extension'])
        flexionExtensionLabel = QtGui.QLabel('Flexion-Extension:')

        descriptionLabel = QtGui.QLabel('Description:')
        self.description = QtGui.QTextEdit()

        groupOutput = QtGui.QGroupBox("Output test")
        scrollArea = QtGui.QScrollArea()
        scrollArea.setWidgetResizable(True)
        h = QtGui.QHBoxLayout()
        v = QtGui.QVBoxLayout()
        i = 0
        for output, combox in sorted(self.dicoComboxOutput.items()):
            ligne = QtGui.QHBoxLayout()
            labelCombobox = QtGui.QLabel(output + ':')
            labelCombobox.setObjectName('comboboxLabel')
            ligne.addWidget(labelCombobox)
            ligne.addWidget(combox)
            v.addLayout(ligne)
            i += 1
            if i % 2 == 0:
                h.addLayout(v)
                v = QtGui.QVBoxLayout()

        if i % 2 != 0:
            ho = QtGui.QHBoxLayout()
            ho.addWidget(repartionMouvementLabel)
            ho.addWidget(self.comboboxRepartitionMouvement)
            v.addLayout(ho)
            h.addLayout(v)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(inclinaisonLateralLabel)
            ho.addWidget(self.comboboxInclinaisonLateral)
            va = QtGui.QVBoxLayout()
            va.addLayout(ho)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(flexionExtensionLabel)
            ho.addWidget(self.comboboxFlexionExtension)
            va.addLayout(ho)
            h.addLayout(va)
        else:
            ho = QtGui.QHBoxLayout()
            ho.addWidget(inclinaisonLateralLabel)
            ho.addWidget(self.comboboxInclinaisonLateral)
            va = QtGui.QVBoxLayout()
            va.addLayout(ho)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(flexionExtensionLabel)
            ho.addWidget(self.comboboxFlexionExtension)
            va.addLayout(ho)
            h.addLayout(va)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(repartionMouvementLabel)
            ho.addWidget(self.comboboxRepartitionMouvement)
            h.addLayout(ho)

        groupOutput.setLayout(h)
        scrollArea.setWidget(groupOutput)

        v2 = QtGui.QVBoxLayout()
        h2 = QtGui.QHBoxLayout()
        h2.addWidget(self.button)
        h2.addWidget(self.minuteur)
        h2.setSpacing(0)
        v2.addLayout(h2)

        h3 = QtGui.QHBoxLayout()
        h3.addWidget(self.navi_toolbar1)
        h3.addWidget(legendLabel1)
        h3.addWidget(self.comboxCourbe1)
        h3.addWidget(self.buttonCourbe1)
        grid.addLayout(h3, 1, 0, 2, 11)
        grid.addWidget(self.plot1, 3, 0, 15, 11)

        grid.addWidget(groupFonction, 1, 11, 7, 1)

        grid.addWidget(buttonPauseStart, 16, 11)
        grid.addWidget(buttonHideShow, 17, 11)

        h = QtGui.QHBoxLayout()
        h.addWidget(descriptionLabel)
        h.addWidget(self.description)

        horizontalBas = QtGui.QHBoxLayout()
        horizontalBas.addWidget(scrollArea)
        horizontalBas.addWidget(self.userCombox)
        horizontalBas.addLayout(h)
        horizontalBas.addLayout(v2)
        horizontalBas.setSpacing(15)
        s = QtGui.QWidget()
        s.setLayout(horizontalBas)
        self.dock = QtGui.QDockWidget('Test')
        self.dock.setFeatures(QtGui.QDockWidget.DockWidgetMovable)
        self.dock.setWidget(s)
        self.dock.hide()
        grid.addWidget(self.dock, 19, 0, 2, 12)
        self.setLayout(grid)
        self.addFonction()
        return grid

    def resetFonction(self):
        self.dicoF = {}
        QtGui.QWidget().setLayout(self.verticalFonction)
        # self.setLayout(self.initUI())
        self.verticalFonction = QtGui.QV1BoxLayout()

    def hideShow(self):
        if self.dock.isHidden():
            self.dock.show()
        else:
            self.dock.hide()

    def pauseStart(self):
        if self.connected:
            self.disconnecting()
            self.connected = False
        else:
            self.connecting()
            self.thread.start()

    def changeMode(self):
        self.labelSld.repaint()
        if 'BUFFER' in str(self.labelSld.text()):
            self.labelSld.setText('Mode CLASSIC : ')
        else:
            self.labelSld.setText('Mode BUFFER : ')

    def addFonction(self):
        indicateur = str(self.comboxCourbe1.currentText())
        if indicateur not in self.dicoF:
            if indicateur == 'Accelerometre':
                if 'ax' in self.dicoF:
                    return

                listIndicateurAdd = ['ax', 'ay', 'az']
            elif indicateur == 'Giroscope':
                if 'gx' in self.dicoF:
                    return

                listIndicateurAdd = ['gx', 'gy', 'gz']
            elif indicateur == 'Magnetometre':
                if 'tx' in self.dicoF:
                    return

                listIndicateurAdd = ['tx', 'ty', 'tz']
            elif indicateur == 'Altimetre':
                if 'alt' in self.dicoF:
                    return

                listIndicateurAdd = ['alt']
            elif indicateur in self.listOutput:
                if indicateur + 'BEST' in self.dicoF:
                    return

                listIndicateurAdd = [indicateur + 'BEST', indicateur + 'NEW', indicateur + 'SERVER']
            else:
                listIndicateurAdd = [indicateur]
            for indicateurtoAdd in listIndicateurAdd:
                self.dicoF[indicateurtoAdd] = {}
                self.dicoF[indicateurtoAdd]['ll'] = [0] * self.MaxPoint
                r = lambda: random.randint(0, 255)
                randomColor = '#%02X%02X%02X' % (r(), r(), r())
                self.dicoF[indicateurtoAdd]['line'], = self.plot1.axis.plot(self.dicoF[indicateurtoAdd]['ll'],
                                                                            color=randomColor, label=indicateurtoAdd)
                self.dicoF[indicateurtoAdd]['checkbox'] = createCheckbox(self.widgetFonction, indicateurtoAdd,
                                                                         self.updateCourbeFonction, check=1)
                self.switch(self.plot1)
                self.verticalFonction.addWidget(self.dicoF[indicateurtoAdd]['checkbox'])

    def updateCourbeFonction(self):
        self.switch(self.plot1)

    def switch(self, plot):
        lineFonction = []
        outputFonction = []
        for output, dicoLine in self.dicoF.items():
            if dicoLine['checkbox'].isChecked():
                lineFonction.append(dicoLine['line'])
                outputFonction.append(output)

        plot.axis.legend(lineFonction, outputFonction, loc=2, borderaxespad=0., bbox_to_anchor=(1.05, 1))
        plot.canvas.draw()

    def addPoint(self, dicoLine, dicoReponse):
        if self.currentPointNumber < self.MaxPoint:
            self.addPlot(dicoLine, dicoReponse)
        else:
            self.graphePop()
            self.appendPlot(dicoLine, dicoReponse)
        self.currentPointNumber += 1

    def addPlot(self, dicoLine, dicoReponse):
        currentIndex = self.currentPointNumber
        first = True
        for indicateur, dicoLineF in self.dicoF.items():
            if self.dicoF[indicateur]['checkbox'].isChecked():
                self.dicoF[indicateur]['line'].set_visible(True)
                if indicateur in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
                    point = dicoLine[indicateur]
                elif indicateur in [output + 'BEST' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-4]][0]
                elif indicateur in [output + 'NEW' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-3]][1]
                elif indicateur in [output + 'SERVER' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-6]][2]
                else:
                    point = float(
                        self.dicoAllIndicateur[indicateur](dicoLine, self.data.getLastLine(), self.dataVariable))
                if isinstance(point, np.ndarray):
                    point = point[0]
                elif isinstance(point, list):
                    point = point[-1]
                if first:
                    minPoint = point
                    maxPoint = point
                    first = False
                else:
                    if point < minPoint:
                        minPoint = point
                    if point > maxPoint:
                        maxPoint = point

                dicoLineF['ll'][currentIndex] = point
            else:
                self.dicoF[indicateur]['line'].set_visible(False)

        if not first:
            if len(self.lastMaxPoint) > 100:
                self.lastMaxPoint.pop(0)
            if len(self.lastMinPoint) > 100:
                self.lastMinPoint.pop(0)
            self.lastMaxPoint.append(maxPoint)
            self.lastMinPoint.append(minPoint)

    def appendPlot(self, dicoLine, dicoReponse):

        first = True
        for indicateur, dicoLineF in self.dicoF.items():
            if self.dicoF[indicateur]['checkbox'].isChecked():
                self.dicoF[indicateur]['line'].set_visible(True)
                if indicateur in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
                    point = dicoLine[indicateur]
                elif indicateur in [output + 'BEST' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-4]][0]
                elif indicateur in [output + 'NEW' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-3]][1]
                elif indicateur in [output + 'SERVER' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-6]][2]
                else:
                    point = float(
                        self.dicoAllIndicateur[indicateur](dicoLine, self.data.getLastLine(), self.dataVariable))

                if isinstance(point, np.ndarray):
                    point = float(point[0])
                elif isinstance(point, list):
                    point = float(point[-1])

                if first:
                    minPoint = point
                    maxPoint = point
                    first = False
                else:
                    if point < minPoint:
                        minPoint = point
                    if point > maxPoint:
                        maxPoint = point

                dicoLineF['ll'].append(point)
            else:
                self.dicoF[indicateur]['line'].set_visible(False)

        if not first:
            if len(self.lastMaxPoint) > 100:
                self.lastMaxPoint.pop(0)
            if len(self.lastMinPoint) > 100:
                self.lastMinPoint.pop(0)
            self.lastMaxPoint.append(maxPoint)
            self.lastMinPoint.append(minPoint)

    def graphePop(self):
        for output, dicoLine in self.dicoF.items():
            if self.dicoF[output]['checkbox'].isChecked():
                dicoLine['ll'].pop(0)

    def updateGraphe(self, line):
        if len(self.dicoF.keys()) == 0:
            return

        if line == 'NOPORT':
            self.raiseError('NOPORT', 'Aucun dongle koondal trouver, veuillez redemarrer')
        elif line == 'ERRORWINDOWS':
            self.raiseError('Erreur koondal', (
            'Error de port windows, veuillez redemarer l\'application \n et changer le dongle de port').encode("utf-8"))
        elif line == 'TIMEOUT':
            self.raiseError('DECONNEXION', 'Dongle deconnecte')
        elif line == 'INACTIF':
            self.raiseError('DECONNEXION',
                            'Koondal inacitf trop longtemps, raison :\n\t-plus de batterie\n\t-distance trop longue pendant une longue period\nVeuillez redemarer')
        elif line == 'BADPORT':
            self.raiseError('MAUVAIS PORT', 'Erreur de port de la carte, veuillez redemarrer')
        else:
            self.connected = True
        ll = line.split(':')
        ll2 = map(float, ll)

        dicoReponse = self.data.updateDataAndReturnAnswer(ll)

        dicoLine = {'alt': ll2[0],
                    'ax': ll2[1], 'ay': ll2[2], 'az': ll2[3],
                    'gx': ll2[4], 'gy': ll2[5], 'gz': ll2[6],
                    'tx': ll2[7], 'ty': ll2[8], 'tz': ll2[9],
                    'time': ll2[10]}

        self.addPoint(dicoLine, dicoReponse)
        # redraw line

        self.plot1.axis.clear()
        self.plot1.axis.draw_artist(self.plot1.axis.patch)
        if self.lastMaxPoint:
            self.cpt += 1
            newMax = max(self.lastMaxPoint)
            newMin = min(self.lastMinPoint)
            if self.firstPoint:
                self.lastMax = newMax
                self.lastMin = newMin
            diff = (newMax - newMin) / 5
            if newMax > self.lastMax + diff or newMin < self.lastMin - diff or \
                    self.firstPoint or newMin > self.lastMin + diff or newMax < self.lastMax - diff:
                self.lastMax = newMax
                self.lastMin = newMin
                self.plot1.axis.set_ylim([newMin - diff - 0.1, newMax + diff + 0.1])
                self.switch(self.plot1)
                self.cpt = 0
                self.firstPoint = False

        # draw indicateur
        for indicateur, dico in self.dicoF.items():
            if dico['checkbox'].isChecked():
                dico['line'].set_ydata(dico['ll'])
                self.plot1.axis.draw_artist(dico['line'])

        self.plot1.canvas.update()

        if self.recordBool:
            if len(self.lines) >= 300:
                self.createRecord()
                self.lines = []
            self.lines += [ll]
            seconds = float(ll[10]) - float(self.t0)
            updateMinuteur(self.minuteur, seconds)


'''
Purpose : widget de choix des indicateurs
'''


class WidgetChoixIndicateur(QtGui.QWidget):
    def __init__(self, parent=None):
        super(WidgetChoixIndicateur, self).__init__(parent)
        self.initUI()
        self.setMinimumSize(600, 400)
        self.hideOnce = False

    def initUI(self, first=True):
        grid = QtGui.QGridLayout()
        grid.setSpacing(1)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        groupFonction.setFixedWidth(300)
        self.widgetFonction = QtGui.QWidget()
        self.verticalFonction = QtGui.QVBoxLayout()
        self.loadAllIndicateur()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)

        buttonAddAll = QtGui.QPushButton('Tout ajouter', self)
        buttonAddAll.clicked[bool].connect(self.addAll)

        buttonAdd = QtGui.QPushButton('Ajouter la selection', self)
        buttonAdd.clicked[bool].connect(self.add)

        buttonReset = QtGui.QPushButton('Reset', self)
        buttonReset.clicked[bool].connect(self.reset)

        buttonOK = QtGui.QPushButton('OK', self)
        buttonOK.clicked[bool].connect(self.ok)

        selectionFonction = QtGui.QScrollArea()
        selectionFonction.setWidgetResizable(True)
        selectionFonction.setFixedWidth(300)
        widgetSelection = QtGui.QWidget()
        self.verticalSelectionFonction = QtGui.QVBoxLayout()
        self.loadAllCurrentIndicateur(first)
        widgetSelection.setLayout(self.verticalSelectionFonction)
        selectionFonction.setWidget(widgetSelection)

        grid.setRowMinimumHeight(0, 10)
        grid.addWidget(groupFonction, 1, 0, 5, 4)
        grid.setColumnMinimumWidth(4, 10)
        grid.setColumnMinimumWidth(6, 10)
        grid.addWidget(buttonAdd, 2, 5, 1, 1)
        grid.addWidget(buttonAddAll, 3, 5, 1, 1)
        grid.addWidget(buttonReset, 4, 5, 1, 1)
        grid.addWidget(selectionFonction, 1, 7, 5, 4)
        grid.addWidget(buttonOK, 8, 8, 1, 1)

        self.setLayout(grid)
        self.setStyleSheet(styleSheet.getStyle())

        return grid

    def loadAllIndicateur(self):
        dicoIndicateur = getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p')
        dicoCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        self.dicoCheckBox = {}
        brut = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']
        for output in brut + sorted(dicoIndicateur.keys()):
            if output in dicoCurrentIndicateur:
                checkbox = createCheckbox(self, output, None, check=1)
            else:
                checkbox = createCheckbox(self, output, None, check=0)
            self.dicoCheckBox[output] = checkbox
            self.verticalFonction.addWidget(checkbox)

    def loadAllCurrentIndicateur(self, first):
        dicoCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        self.dicoLabelBox = {}
        for output in sorted(self.dicoCheckBox.keys()):
            if first:
                if output in dicoCurrentIndicateur:
                    label = QtGui.QLabel(output)
                    self.dicoLabelBox[output] = label
                    self.verticalSelectionFonction.addWidget(label)

    def ok(self):
        self.hideIT()
        brut = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']
        listIndicateurBrut = [mesure for mesure in brut if mesure in self.dicoLabelBox.keys()]
        listIndicateurNonBrut = [indicateur for indicateur in self.dicoLabelBox.keys() if indicateur not in brut]
        dumpDicoIndicateur(listIndicateurBrut + listIndicateurNonBrut, path=DIRPATH + "/Log/Pickle/indicateur.p")
        return self.dicoLabelBox

    def addAll(self):
        for output in self.dicoCheckBox.keys():
            if output not in self.dicoLabelBox.keys():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def add(self):
        for output, checkbox in self.dicoCheckBox.items():
            if output not in self.dicoLabelBox.keys() and checkbox.isChecked():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def reset(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI(first=False))
        self.dicoLabelBox = {}
        for output, checkbox in self.dicoCheckBox.items():
            checkbox.setChecked(False)

    def showIT(self):
        self.show()

    def hideIT(self):
        self.hideOnce = True
        self.hide()


'''
Purpose : widget de choix des user à prendre en compte
'''


class WidgetChoixMember(QtGui.QWidget):
    def __init__(self, parent=None):
        super(WidgetChoixMember, self).__init__(parent)
        self.initUI()
        self.setMinimumSize(600, 400)

    def initUI(self, first=True):
        grid = QtGui.QGridLayout()
        grid.setSpacing(1)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        self.widgetFonction = QtGui.QWidget()
        self.verticalFonction = QtGui.QVBoxLayout()
        self.loadAllMember()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)

        buttonAddAll = QtGui.QPushButton('Tout ajouter', self)
        buttonAddAll.clicked[bool].connect(self.addAll)

        buttonAdd = QtGui.QPushButton('Ajouter la selection', self)
        buttonAdd.clicked[bool].connect(self.add)

        buttonReset = QtGui.QPushButton('Reset', self)
        buttonReset.clicked[bool].connect(self.reset)

        buttonOK = QtGui.QPushButton('OK', self)
        buttonOK.clicked[bool].connect(self.ok)

        selectionFonction = QtGui.QScrollArea()
        selectionFonction.setWidgetResizable(True)
        widgetSelection = QtGui.QWidget()
        self.verticalSelectionFonction = QtGui.QVBoxLayout()
        self.loadAllCurrentMember(first)
        widgetSelection.setLayout(self.verticalSelectionFonction)
        selectionFonction.setWidget(widgetSelection)

        grid.setRowMinimumHeight(0, 10)
        grid.addWidget(groupFonction, 1, 0, 5, 4)
        grid.setColumnMinimumWidth(4, 10)
        grid.setColumnMinimumWidth(6, 10)
        grid.addWidget(buttonAdd, 2, 5, 1, 1)
        grid.addWidget(buttonAddAll, 3, 5, 1, 1)
        grid.addWidget(buttonReset, 4, 5, 1, 1)
        grid.addWidget(selectionFonction, 1, 7, 5, 4)
        grid.addWidget(buttonOK, 8, 8, 1, 1)

        self.setLayout(grid)
        self.setStyleSheet(styleSheet.getStyle())
        return grid

    def loadAllMember(self):
        self.dicoCheckBox = {}
        listCurrentMember = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/member.p')
        for member in sorted(teamKoondal + ['Guest']):
            if member in listCurrentMember:
                checkbox = createCheckbox(self, member, None, check=1)
            else:
                checkbox = createCheckbox(self, member, None, check=0)
            self.dicoCheckBox[member] = checkbox
            self.verticalFonction.addWidget(checkbox)

    def loadAllCurrentMember(self, first):
        listCurrentMember = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/member.p')
        self.dicoLabelBox = {}
        for output in sorted(self.dicoCheckBox.keys()):
            if first:
                if output in listCurrentMember:
                    label = QtGui.QLabel(output)
                    self.dicoLabelBox[output] = label
                    self.verticalSelectionFonction.addWidget(label)

    def ok(self):
        self.hideIT()
        listMember = [member for member in self.dicoLabelBox.keys()]
        dumpListMember(listMember)

    def addAll(self):
        for output in self.dicoCheckBox.keys():
            if output not in self.dicoLabelBox.keys():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def add(self):
        for output, checkbox in self.dicoCheckBox.items():
            if output not in self.dicoLabelBox.keys() and checkbox.isChecked():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def reset(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI(first=False))
        self.dicoLabelBox = {}
        for output, checkbox in self.dicoCheckBox.items():
            checkbox.setChecked(False)

    def showIT(self):
        self.show()

    def hideIT(self):
        self.hide()


'''
Purpose : widget add new output
'''


class WidgetIA(QtGui.QWidget):
    def __init__(self, parent=None, qData='jojo', widUser='user', widPlot='plot', serverRequest=None):
        super(WidgetIA, self).__init__(parent)
        self.data = qData
        self.widgetIndicateur = WidgetChoixIndicateur()
        self.widgetIndicateur.hideIT()
        self.widgetMember = WidgetChoixMember()
        self.widgetMember.hideIT()
        self.widU = widUser
        self.widP = widPlot

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.initUI()

    def checkAll(self):
        if self.check:
            for output, checkbox in self.checkOutput.items():
                self.checkOutput[output].setChecked(False)
        else:
            for output, checkbox in self.checkOutput.items():
                self.checkOutput[output].setChecked(True)
        self.check = not self.check

    def initUI(self):
        gridIntern = QtGui.QGridLayout()
        gridIntern.setSpacing(1)

        if self.online:
            listOutputScore = self.serverRequest.getOutput(exist='False')
        else:
            listOutputScore = parseOutput(checkScore=False).keys()
        listCheckLabel = sorted([outputi for outputi in listOutputScore + ['USER'] if outputi not in teamKoondal])

        self.checkOutput = {output: createCheckbox(self, output, None, check=0) for output in listCheckLabel}
        self.scoreOutput = {output: self.getScore(output) for output in listOutputScore}
        self.check = False
        buttonCheck = QtGui.QPushButton('Check/Uncheck OUTPUT', self)
        buttonCheck.clicked[bool].connect(self.checkAll)

        self.outputLineEdit = QtGui.QLineEdit()
        button = QtGui.QPushButton('ajouter', self)
        button.clicked[bool].connect(self.addOutput)
        buttonR = QtGui.QPushButton('supprimer', self)
        buttonR.clicked[bool].connect(self.removeOutput)
        self.buttonRegressionPartiel = QtGui.QPushButton('Lancer Regression', self)
        self.buttonRegressionPartiel.clicked[bool].connect(self.regressionPartiel)

        lenPcaMax = len(getDicoIndicateur())
        self.comboboxPca = createComboBox(['Pas de PCA'] + [str(i + 1) for i in range(lenPcaMax + 10)])
        self.comboboxRegression = createComboBox(
            ['Regression Logistique', 'Regression LogistiqueCV', 'Regression Ridge'])
        self.labelIA = QtGui.QLabel('En attente de regression ... ')
        self.labelAdviceIA = QtGui.QLabel('')
        if self.online:
            onlineMember = ['Guest'] + self.serverRequest.getTeamKoondal()
        else:
            onlineMember = ['Guest', 'Mourougan', 'Tarik', 'Paul']
        self.comboboxUser = createComboBox(onlineMember)

        buttonIndicateur = QtGui.QPushButton('Choix des indicateurs', self)
        buttonIndicateur.clicked[bool].connect(self.chooseIndicateur)
        buttonMember = QtGui.QPushButton('  Sources des tests  ', self)
        buttonMember.clicked[bool].connect(self.chooseMember)

        hAjout = QtGui.QHBoxLayout()
        hPca = QtGui.QHBoxLayout()

        if self.online:
            listOutput = self.serverRequest.getOutput(exist='False')
        else:
            listOutput = parseOutput(checkScore=False)
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        listCheckLabel = sorted([outputi for outputi in listOutputScore + ['USER'] if outputi not in teamKoondal])
        self.labelScoreIn = {output: QtGui.QLabel(str("%.2f %% " % (float(self.scoreOutput[output][0]) * 100))) for
                             output in listOutputScore}
        self.labelScoreOut = {output: QtGui.QLabel(str("%.2f %% " % (float(self.scoreOutput[output][1]) * 100))) for
                              output in listOutputScore}
        self.labelNbTest = {output: QtGui.QLabel(str(self.scoreOutput[output][2])) for output in listOutputScore}
        self.hLine = {output: QtGui.QHBoxLayout() for output in listCheckLabel if output not in teamKoondal}

        for output in listCheckLabel:
            if output == 'USER':
                nbTest = 512
                self.hLine[output].addWidget(self.checkOutput[output])
                self.hLine[output].addWidget(self.labelScoreIn['Mourougan'])
                self.hLine[output].addWidget(self.labelScoreOut['Mourougan'])
                self.hLine[output].addWidget(QtGui.QLabel(str(nbTest)))
            else:
                self.hLine[output].addWidget(self.checkOutput[output])
                self.hLine[output].addWidget(self.labelScoreIn[output])
                self.hLine[output].addWidget(self.labelScoreOut[output])
                self.hLine[output].addWidget(self.labelNbTest[output])

        v = QtGui.QVBoxLayout()
        for output in listCheckLabel:
            v.addLayout(self.hLine[output])

        h0 = QtGui.QHBoxLayout()
        h0.addWidget(buttonCheck)
        h0.addWidget(QtGui.QLabel('SCORE IN'))
        h0.addWidget(QtGui.QLabel('SCORE OUT'))
        h0.addWidget(QtGui.QLabel('NOMBRE DE TEST'))

        hAjout.addWidget(self.outputLineEdit)
        hAjout.addWidget(button)
        hAjout.setSpacing(0)
        hPca.addWidget(QtGui.QLabel('PCA:'))
        hPca.addWidget(self.comboboxPca)
        hPca.setSpacing(0)
        h0.setSpacing(0)

        gridIntern.addLayout(h0, 0, 0, 1, 4)
        gridIntern.addLayout(v, 1, 0, 4, 4)

        gridIntern.addWidget(buttonR, 5, 0, 1, 1)
        gridIntern.addLayout(hAjout, 5, 1, 1, 1)
        gridIntern.addLayout(hPca, 6, 0, 1, 1)
        gridIntern.addWidget(buttonIndicateur, 6, 1, 1, 1)
        gridIntern.addWidget(buttonMember, 6, 2, 1, 1)
        gridIntern.addWidget(self.comboboxRegression, 6, 3, 1, 1)

        h = QtGui.QHBoxLayout()
        h.addWidget(QtGui.QLabel('Utilisateur actuel: '))
        h.addWidget(self.comboboxUser)
        gridIntern.addLayout(h, 6, 4, 1, 1)
        gridIntern.addWidget(self.buttonRegressionPartiel, 6, 5, 1, 1)
        h = QtGui.QVBoxLayout()
        h.addWidget(self.labelIA)
        h.addWidget(self.labelAdviceIA)
        gridIntern.addLayout(h, 7, 3, 1, 2)

        self.setLayout(gridIntern)
        return gridIntern

    def chooseIndicateur(self):
        self.widgetIndicateur.showIT()

    def chooseMember(self):
        self.widgetMember.showIT()

    def addOutput(self):
        filepath = DIRPATH + '/Log/Output/output.csv'
        dataCsv = pandas.read_csv(filepath, sep=';')
        outputName = str(unicode(self.outputLineEdit.text()).encode("utf-8")).title()
        try:
            self.serverRequest.ServerAddOutput(outputName)
            print 'ajouter'
        except:
            pass
        if outputName in list(dataCsv['OutputName']):
            print 'wtf'
            return
        with open(filepath, 'ab') as f:
            if str(self.outputLineEdit.text()) != '':
                f.write(outputName + ';[-1 1]\n')
                createFileScore(outputName)
                directory = DIRPATH + '/Log/Pickle/' + outputName
                if not os.path.exists(directory):
                    os.makedirs(directory)
                directory = DIRPATH + 'Log/Pickle/BEST/' + outputName
                if not os.path.exists(directory):
                    os.makedirs(directory)

                for member in teamKoondal + ['Guest']:
                    directory = DIRPATH + '/Compte/' + member + '/' + outputName
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                    directory = DIRPATH + '/Compte/' + member + '/BEST/' + outputName
                    if not os.path.exists(directory):
                        os.makedirs(directory)

        currentUser = self.comboboxUser.currentText()
        with open(DIRPATH + '/Log/Output/update.txt', 'wb')as fU:
            fU.write('output: ' + outputName + '\n')
            fU.write('user: ' + currentUser + '\n')
        self.refreshUi()

    def regressionPartiel(self):
        self.buttonRegressionPartiel.setEnabled(False)
        self.labelIA.repaint()
        self.labelIA.setText(' ')
        self.labelAdviceIA.repaint()
        self.labelAdviceIA.setText('')
        if not self.online:
            self.labelIA.setText(u'La connexion au serveur à échoué')
            self.buttonRegressionPartiel.setEnabled(True)

        listCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        listCurrentIndicateur = getFunctionByLevel(listCurrentIndicateur)

        currentUser = self.comboboxUser.currentText()
        outputs = [output for output in self.checkOutput.keys() if
                   self.checkOutput[output].isChecked() and output != 'USER']
        if self.checkOutput['USER'].isChecked():
            outputs += teamKoondal

        listUser = getListMember()
        if listUser == []:
            self.labelIA.repaint()
            self.labelIA.setText(u'Aucune source d\'utilisateur selectionné')
            self.buttonRegressionPartiel.setEnabled(True)
            return
        if outputs == []:
            self.labelIA.repaint()
            self.labelIA.setText(u'Aucune fonction output selectionné')
            self.buttonRegressionPartiel.setEnabled(True)
            return
        pca = int(str(self.comboboxPca.currentText())) if self.comboboxPca.currentIndex() != 0 else 0
        t0 = time.time()
        for output in outputs:
            if output in teamKoondal:
                continue
            self.labelIA.repaint()
            self.labelIA.setText('Mise a jour de ' + str(output) + ' en cours ...')
            QtCore.QCoreApplication.processEvents()
            typeRegression = str(self.comboboxRegression.currentText()).split()[-1]

            if self.online:
                progress = QtGui.QProgressDialog(u"Regression en cours ...", "Cancel", 0, 12)
                progress.setCancelButton(None)
                progress.setStyleSheet(styleSheet.getStyle())
                progress.show()
                QtCore.QCoreApplication.processEvents()
                dico = self.serverRequest.requestRegression(output, listCurrentIndicateur, listUser, pca,
                                                            typeRegression, progress=progress)
            else:
                OfflineRequest.updateLocalData()
                progress = QtGui.QProgressDialog(u"Regression en cours ...", "Cancel", 0, 12)
                progress.setCancelButton(None)
                progress.setStyleSheet(styleSheet.getStyle())
                progress.show()
                QtCore.QCoreApplication.processEvents()
                dico = OfflineRequest.requestRegression(output, listCurrentIndicateur, listUser, pca,
                                                        typeRegression, progress=progress)

            if dico['error'] == None:
                saveFunctionConfiguration(currentUser, dico, listCurrentIndicateur, outputName=output)
            else:
                self.labelIA.repaint()
                self.labelIA.setText(dico['error'])
                self.buttonRegressionPartiel.setEnabled(True)
                if output == outputs[-1]:
                    return
                else:
                    continue
            if dico['advice']:
                self.labelAdviceIA.repaint()
                self.labelAdviceIA.setText(dico['advice'])

            QtCore.QCoreApplication.processEvents()
            self.labelIA.repaint()
            self.labelIA.setText('Mise a jour de ' + str(output) + ' OK')

        self.labelIA.repaint()
        timeRegression = str("%.1f" % (time.time() - t0))
        self.labelIA.setText('Temps regression(' + timeRegression + ')sec')

        self.updateScore()
        self.data.initialize(user=currentUser)
        progress.setValue(progress.value() + 2)
        QtCore.QCoreApplication.processEvents()
        dumpDicoIndicateur(listCurrentIndicateur, path=DIRPATH + '/Log/Pickle/lastIndicateur.p')
        QtCore.QCoreApplication.processEvents()
        self.buttonRegressionPartiel.setEnabled(True)

        self.widU.stop()
        QtCore.QCoreApplication.processEvents()
        QtGui.QWidget().setLayout(self.widU.layout())
        self.widU.setLayout(self.widU.initUI(user=currentUser))
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()

    def refreshUi(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI())
        with open(DIRPATH + '/Log/Output/update.txt', 'rb')as f:
            str(f.readline()).split()[-1]
            user = str(f.readline()).split()[-1]
        QtGui.QWidget().setLayout(self.widU.layout())
        self.widU.setLayout(self.widU.initUI(user=user))
        self.widU.stop()
        self.widP.disconnecting()
        self.widP.thread.terminate()
        QtGui.QWidget().setLayout(self.widP.layout())
        self.widP.setLayout(self.widP.initUI())
        self.data.initialize(user=user)

    def getScore(self, label):
        if not os.path.exists(DIRPATH + '/Log/Score/' + label + '.csv'):
            return ('0', '0', '0')
        else:
            dataCsv = pandas.read_csv(DIRPATH + '/Log/Score/' + label + '.csv')
            if len(list(dataCsv['ScoreIn'])) >= 1:
                return (list(dataCsv['ScoreIn'])[-1], list(dataCsv['ScoreOut'])[-1], list(dataCsv['NombreDeTest'])[-1])
            else:
                return ('0', '0', '0')

    def updateScore(self):
        if self.online:
            listOutputScore = self.serverRequest.getOutput(exist='True')
        else:
            listOutputScore = parseOutput(checkScore='True')
        for output in listOutputScore:
            self.scoreOutput[output] = self.getScore(output)

        for output in self.labelScoreIn.keys():
            self.labelScoreIn[output].repaint()
            self.labelScoreOut[output].repaint()
            self.labelNbTest[output].repaint()
            self.labelScoreIn[output].setText(str("%.2f %%" % (float(self.scoreOutput[output][0]) * 100)))
            self.labelScoreOut[output].setText(str("%.2f %%" % (float(self.scoreOutput[output][1]) * 100)))
            self.labelNbTest[output].setText(str(self.scoreOutput[output][2]))

    def removeOutput(self):
        result = QtGui.QMessageBox.question(self,
                                            "Confirm Suppression...",
                                            "Are you sure you want to remove those output ?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        filepath = DIRPATH + '/Log/Output/output.csv'
        if result == QtGui.QMessageBox.Yes:
            listWithoutTeam = [output for output in self.listOutput if output not in teamKoondal]
            listOutputKeep = [output for output in listWithoutTeam if (not self.checkOutput[output].isChecked()) or
                              (output in orderPrincipalOutput + ['User'] + teamKoondal)]
            dataCsv = pandas.read_csv(filepath, sep=';')
            dataCsv = dataCsv[dataCsv['OutputName'].isin(listOutputKeep + teamKoondal)]
            dataCsv.to_csv(filepath, sep=';', index_label=False, index=False)
            for output in [output for output in self.checkOutput.keys() if output not in listOutputKeep]:
                if output == 'User':
                    continue
                try:
                    self.serverRequest.ServerRemoveOutput(output)
                except:
                    pass
                directory = DIRPATH + '/Log/Pickle/' + output
                if os.path.exists(directory):
                    shutil.rmtree(directory)
                    line = '0' + ',' + '0' + ',' + '0' + ',' + '0' + ',' + 'NONE' + ',' + 'NONE' + '\n'
                    with open(DIRPATH + '/Log/Score/' + output + '.csv', 'ab') as f:
                        f.write(line)
                for member in teamKoondal + ['Guest']:
                    directory = DIRPATH + '/Compte/' + member + '/' + output
                    if os.path.exists(directory):
                        shutil.rmtree(directory)

            self.refreshUi()


'''
Purpose : Widget qui Ouvre jusqu'a 3 Test
'''


class WidgetCompare(QtGui.QWidget):
    def __init__(self, parent=None, serverRequest=None):
        super(WidgetCompare, self).__init__(parent)
        self.currentOpen = 0
        self.Listdico = [{}, {}, {}]
        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False

        self.initUI()

    def getCanvas(self):
        return self.plot.canvas

    def openFile(self):
        try:
            filename, extension = QtGui.QFileDialog.getOpenFileNameAndFilter(self, 'Open File', DIRPATH + '/TestRecord',
                                                                             filter=QApplication.translate('etmEditor',
                                                                                                           'txt File (*.txt)'))
            with open(filename, 'rb') as f:
                dataName = f.readline()
                path_split = dataName.split("/")
                dataName = DIRPATH + '/Log/Data/' + path_split[-1][:-2]
                dicoList = parseFile(dataName)
                if self.currentOpen == 3:
                    self.currentOpen = 0

                t0 = float(dicoList['timeStamp'][0])
                dicoList['timeStamp'] = [float(t) - t0 for t in dicoList['timeStamp']]
                self.Listdico[self.currentOpen] = dicoList
                self.dicoCheckboxCourbe['Courbe' + str(self.currentOpen)].setChecked(True)
                self.currentOpen += 1
                self.updatePlot()
        except Exception, e:
            print 'Error in openFile (Widget Compare) : ' + str(e)

    def isOpen(self):
        return self.currentOpen > 0

    def updatePlot(self):
        self.plot.axis.clear()
        listLine2D = []
        listNameLine = []
        for name, checkbox in self.dicoCheckbox.items():
            if checkbox.isChecked():
                for i in range(self.currentOpen):
                    if self.dicoCheckboxCourbe['Courbe' + str(i)].isChecked():
                        line = self.plot.axis.plot(self.Listdico[i]['timeStamp'], self.Listdico[i][str(name)],
                                                   label=str(name))
                        listLine2D.append(line)
                        nameCourbe = 'Courbe' + str(i) + ' ' + str(name)
                        listNameLine.append(nameCourbe)
        self.plot.axis.legend(listLine2D, listNameLine, loc=2, borderaxespad=0., bbox_to_anchor=(1.05, 1))
        self.plot.canvas.draw()

    def initUI(self):
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput(checkScore=True).keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        button = QtGui.QPushButton('open graphe', self)
        button.clicked[bool].connect(self.openFile)
        self.plot = MatplotlibWidget(self, yName='Acceleration(m./sec2) - Rotation(rad/sec)')
        self.navi_toolbar = NavigationToolbar(self.plot.canvas, self)
        dicoCheckboxInput = {inputIndic: createCheckbox(self, inputIndic, self.updatePlot, check=0)
                             for inputIndic in ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']}

        dicoCheckboxOutput = {output: createCheckbox(self, output, self.updatePlot, check=0) for output in
                              self.listOutput}

        self.dicoCheckbox = {}
        self.dicoCheckbox.update(dicoCheckboxInput)
        self.dicoCheckbox.update(dicoCheckboxOutput)
        self.dicoCheckboxCourbe = {'Courbe' + str(i): createCheckbox(self, 'Courbe' + str(i), self.updatePlot, check=0)
                                   for i in range(len(self.Listdico))}
        layoutHorizontal1 = QtGui.QHBoxLayout()

        for inputIndic, checkbox in dicoCheckboxInput.items():
            layoutHorizontal1.addWidget(checkbox)
        layoutHorizontal = QtGui.QHBoxLayout()
        for inputIndic, checkbox in self.dicoCheckboxCourbe.items():
            layoutHorizontal.addWidget(checkbox)
        layoutHorizontal2 = QtGui.QHBoxLayout()
        for output, checkbox in dicoCheckboxOutput.items():
            layoutHorizontal2.addWidget(checkbox)

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)
        grid.addWidget(self.navi_toolbar, 0, 0)
        grid.addLayout(layoutHorizontal1, 1, 0)
        grid.addLayout(layoutHorizontal2, 2, 0)
        grid.addWidget(self.plot, 3, 0, 3, 5)
        grid.addLayout(layoutHorizontal, 6, 1)
        grid.addWidget(button, 6, 5, 1, 1)
        self.setLayout(grid)
        box = self.plot.axis.get_position()
        self.plot.axis.set_position([box.x0, box.y0, box.width * 0.85, box.height])


'''
Purpose : Menu principal
'''


class Menu(QtGui.QMainWindow):
    def __init__(self):
        super(Menu, self).__init__()
        progress = QtGui.QProgressDialog(u"Lancement du GUI en cours...", "Cancel", 0, 6)
        progress.setCancelButton(None)
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        QtCore.QCoreApplication.processEvents()

        try:
            serverRequest = ServerRequest()
        except:
            serverRequest = None

        self.workerUser = WorkerUser('capteur')
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.data = Data.Data(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.lastWid = 'User'
        self.widPlot = WidgetPlot(qThread=self.workerUser, qData=self.data, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widUser = WidgetUser(qThread=self.workerUser, qData=self.data, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widCompare = WidgetCompare(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widTest = WidgetTest.getWidgetTest(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widIa = WidgetIA(qData=self.data, widUser=self.widUser, widPlot=self.widPlot, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()

        self.initUI()

    def closeEvent(self, event):
        result = QtGui.QMessageBox.question(self,
                                            "Confirm Exit...",
                                            "Etez vous sure de vouloir quitter ?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        event.ignore()

        if result == QtGui.QMessageBox.Yes:
            if self.workerUser.capteur != 'capteur':
                self.workerUser.__del__()

            self.widUser.stop()
            self.widPlot.disconnecting()
            event.accept()
            print 'accepted'
            exit()

    def openFile(self):
        self.switchToBilan()
        self.widCompare.openFile()

    def exportGraph(self):
        if self.widCompare.parent and self.widCompare.isOpen():
            self.widCompare.export()

    def switchToCurve(self):
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widUser.stopUpdate()
        self.widPlot.connecting()
        self.setCentralWidget(self.widPlot)
        self.lastWid = 'Curve'

    def switchToTest(self):
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widPlot.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        if os.stat(DIRREFRESHDATAFILE).st_size != 0:
            try:
                serverRequest = ServerRequest()
            except:
                serverRequest = None
            self.widTest = WidgetTest.getWidgetTest(serverRequest=serverRequest)

        self.setCentralWidget(self.widTest)

    def switchToUser(self):
        self.widUser.startUpdate()
        self.widPlot.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.connecting()
        self.setCentralWidget(self.widUser)
        self.lastWid = 'User'

    def switchToBilan(self):
        self.widPlot.setParent(None)
        self.widUser.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        self.setCentralWidget(self.widCompare)
        self.lastWid = 'Compare'

    def switchToIa(self):
        self.widPlot.setParent(None)
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        self.setCentralWidget(self.widIa)
        self.lastWid = 'IA'

    def initUI(self):
        exitAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.exportAction = QtGui.QAction('Export Graphe to JPG', self)
        self.exportAction.setShortcut('Ctrl+E')
        self.exportAction.setStatusTip('Export Graphe openned to JPG ')
        self.exportAction.triggered.connect(self.exportGraph)

        openAction = QtGui.QAction('Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a file')
        openAction.triggered.connect(self.openFile)

        curveAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Graphe.png'), 'Test', self)
        curveAction.setShortcut('Ctrl+G')
        curveAction.setStatusTip('Start a test')
        curveAction.triggered.connect(self.switchToCurve)

        testAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Score.png'), 'Test', self)
        testAction.setShortcut('Ctrl+G')
        testAction.setStatusTip('Start a test')
        testAction.triggered.connect(self.switchToTest)

        userAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/User.png'), 'User', self)
        userAction.setShortcut('Ctrl+U')
        userAction.setStatusTip('Check position')
        userAction.triggered.connect(self.switchToUser)

        bilanAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Bilan.png'), 'Bilan', self)
        bilanAction.setShortcut('Ctrl+B')
        bilanAction.setStatusTip('Bilan')
        bilanAction.triggered.connect(self.switchToBilan)

        iaAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Ia.png'), 'IA', self)
        iaAction.setStatusTip('IA')
        iaAction.triggered.connect(self.switchToIa)

        toolbar = self.addToolBar('User')
        toolbar.addAction(userAction)
        toolbar.addAction(curveAction)
        toolbar.addAction(bilanAction)
        toolbar.addAction(iaAction)
        toolbar.addAction(testAction)

        toolbar.setIconSize(QtCore.QSize(35, 35))

        self.menubar = self.menuBar()

        self.menu = self.menubar.addMenu('&Bluetooth')

        self.subMenu = self.menu.addMenu("chercher des Koondals ...")

        for item in self.seekBlue():
            entry = self.subMenu.addAction(item)
            entry.setCheckable(True)
            self.connect(entry, QtCore.SIGNAL('triggered()'), lambda item=item: self.doStuff(item))

        self.setWindowTitle('Koondal')
        self.setWindowIcon(QtGui.QIcon(DIRPATH + '/Icon/Koondal2.ico'))
        self.setCentralWidget(self.widUser)
        self.showMaximized()

    def doStuff(self, item):
        print item

    def seekBlue(self):
        ports = list(serial.tools.list_ports.comports())
        menuitems = []
        for p in ports:
            if "COM" in p[0] and 'Bluetooth' in p[1]:
                menuitems.append(p[0])

        return menuitems



def main():
    app = QtGui.QApplication(sys.argv)
    menu = Menu()
    menu.setStyleSheet(styleSheet.getStyle())
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
