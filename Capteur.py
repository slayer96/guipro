#!/usr/bin/python

import bglib, serial, time, signal
import serial.tools.list_ports

uuid_service = [0x28, 0x00] # 0x2800 (uuid of primary services)
uuid_client_characteristic_configuration = [0x29, 0x02] # 0x2902

# service uuid not used
# uuid_mv_service = [0x71, 0x3D, 0, 0, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E]
uuid_mv_rx = [0x71, 0x3D, 0, 0x02, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E]
uuid_mv_tx = [0x71, 0x3D, 0, 0x03, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E]
attr_mv_tx_handle = 0
attr_mv_rx_handle = 0
attr_mv_rx_handle_ccc = 0
connection_handle = 0
cmd_to_send = ""

STATE_STANDBY = 0
STATE_CONNECTING = 1
STATE_FINDING_ATTRIBUTES = 2
STATE_LISTENING_NOTIFICATIONS = 3
STATE_WRITING = 4
state = STATE_STANDBY


ble = 0
ser = 0
line = ''
fullLine = ''
lineAsked = ''
disco = False
lread = False
readDone = False
writeDone = False
movuino = False
rWasRead = False
firstRead = True

# gap_scan_response handler


def my_ble_evt_gap_scan_response(sender, args):
    global ble, ser, state

    # pull all advertised service info from ad packet
    ad_services = []
    local_name = 'unknown'
    this_field = []
    bytes_left = 0
    for b in args['data']:
        if bytes_left == 0:
            bytes_left = b
            this_field = []
        else:
            this_field.append(b)
            bytes_left = bytes_left - 1
            if bytes_left == 0:
                if this_field[0] == 0x02 or this_field[0] == 0x03:  # partial or complete list of 16-bit UUIDs
                    for i in xrange((len(this_field) - 1) / 2):
                        ad_services.append(this_field[-1-i*2:-3-i*2:-1])

                if this_field[0] == 0x04 or this_field[0] == 0x05:  # partial or complete list of 32-bit UUIDs
                    for i in xrange((len(this_field) - 1) / 4):
                        ad_services.append(this_field[-1 - i*4: -5 - i*4:-1])

                if this_field[0] == 0x06 or this_field[0] == 0x07:  # partial or complete list of 128-bit UUIDs
                    for i in xrange((len(this_field) - 1) / 16):
                        ad_services.append(this_field[-1 - i*16: -17 - i*16: -1])

                if this_field[0] == 0x08 or this_field[0] == 0x09:  # shortened or complete local name
                    local_name = "".join(chr(b) for b in this_field[1:])

    print('Device found:' + local_name)
    print(args)

    # connect to this device

    # send "gap_connect_direct" command
    # arguments:
    #  - MAC address
    #  - use detected address type (will work with either public or private addressing)
    #  - 32 = 32*0.625ms = 20ms minimum connection interval
    #  - 48 = 48*0.625ms = 30ms maximum connection interval
    #  - 100 = 100*10ms = 1000ms supervision timeout
    #  - 0 = no slave latency
    ble.send_command(ser, ble.ble_cmd_gap_connect_direct(args['sender'], args['address_type'], 0x20, 0x30, 0x100, 0))
    ble.check_activity(ser, 1)
    state = STATE_CONNECTING


# connection_status handler
def my_ble_evt_connection_status(sender, args):
    global ble, ser, state
    if (args['flags'] & 0x05) == 0x05:
        # connected, now perform service discovery
        print "Connected to %s" % ':'.join(['%02X' % b for b in args['address'][::-1]])
        connection_handle = args['connection']
        # Look for all the characteristics
        ble.send_command(ser, ble.ble_cmd_attclient_find_information(args['connection'], 0x0001, 0xFFFF))
        ble.check_activity(ser, 1)
        state = STATE_FINDING_ATTRIBUTES


def my_ble_evt_attclient_find_information_found(sender, args):
    global ble, ser, attr_mv_rx_handle, attr_mv_tx_handle, attr_mv_rx_handle_ccc

    # check for movuino rx characteristic
    if args['uuid'] == list(reversed(uuid_mv_rx)):
        print "Found attribute RX " + str(map(hex, args['uuid'])) + " handle=%d" % args['chrhandle']
        attr_mv_rx_handle = args['chrhandle']

    # check for movuino tx characteristic
    elif args['uuid'] == list(reversed(uuid_mv_tx)):
        print "Found attribute TX " + str(map(hex, args['uuid'])) + " handle=%d" % args['chrhandle']
        attr_mv_tx_handle = args['chrhandle']

    # check for subsequent client characteristic configuration
    elif args['uuid'] == list(reversed(uuid_client_characteristic_configuration)) and attr_mv_rx_handle > 0 and\
                    attr_mv_rx_handle_ccc == 0:
        print "Found attribute RX CCC " + str(map(hex, args['uuid'])) + " handle=%d" % args['chrhandle']
        attr_mv_rx_handle_ccc = args['chrhandle']


# attclient_procedure_completed handler
def my_ble_evt_attclient_procedure_completed(sender, args):
    global state, ble, ser, connection_handle
    # check if we just finished searching for services
    if state == STATE_FINDING_ATTRIBUTES:
        if attr_mv_rx_handle > 0 and attr_mv_tx_handle > 0 and attr_mv_rx_handle_ccc > 0:
            print "Found 'Movuino' attributes"
            # found the movuino characteristics, so enable notifications
            # (this is done by writing 0x01 to the client characteristic configuration attribute)
            state = STATE_LISTENING_NOTIFICATIONS
            ble.send_command(ser, ble.ble_cmd_attclient_attribute_write(connection_handle,
                                                                        attr_mv_rx_handle_ccc, [0x01, 0x00]))
            ble.check_activity(ser, 1)
            state = STATE_WRITING
        else:
            print "Could not find 'Movuino' attributes"

    if state == STATE_WRITING:
        print "Writting done"
        state = STATE_LISTENING_NOTIFICATIONS


# attclient_attribute_value handler
def my_ble_evt_attclient_attribute_value(sender, args):
    global state, ble, ser, connection_handle, att_handle_measurement, line, lread, fullLine, lineAsked, \
        firstRead, readDone, writeDone, rWasRead
    message = "".join(chr(b) for b in args['value'])
    if cmd_to_send == 'l':
        if 'p' in message and not firstRead:
            fullLine += 'p'+message.split('p')[-1]
            firstRead = True
        elif 'p' in message:
            messagesplit = message.split('p')
            fullLine += messagesplit[0]
            lineAsked = fullLine
            fullLine = 'p' + messagesplit[-1]
            lread = True
        else:
            fullLine += message


def send_cmd():
    global ble, ser, state

    time.sleep(1)
    data_array = map(ord, cmd_to_send + "\n")
    print("Tx:" + cmd_to_send + str(map(hex, data_array)))
    ble.send_command(ser, ble.ble_cmd_attclient_write_command(connection_handle, attr_mv_tx_handle, data_array))
    ble.check_activity(ser, 1)
    state = -1


# handler to notify of an API parser timeout condition
def my_timeout(sender, args):
    # might want to try the following lines to reset, though it probably
    # wouldn't work at this point if it's already timed out:
    print "BGAPI parser timed out. Make sure the BLE device is in a known/idle state."


def dummy_ble_evt_attclient_indicated(sender, args):
    print 'dummy_ble_evt_attclient_indicated'
    print sender
    print args


def dummy_ble_evt_attclient_procedure_completed(sender, args):
    print 'dummy_ble_evt_attclient_procedure_completed'
    print sender
    print args


def dummy_ble_evt_attclient_group_found(sender, args):
    print 'dummy_ble_evt_attclient_group_found'
    print sender
    print args


def dummy_ble_evt_attclient_attribute_found(sender, args):
    print 'dummy_ble_evt_attclient_attribute_found'
    print sender
    print args


def dummy_ble_evt_attclient_find_information_found(sender, args):
    print 'dummy_ble_evt_attclient_find_information_found'
    print sender
    print args


def dummy_ble_evt_attclient_attribute_value(sender, args):
    print 'dummy_ble_evt_attclient_attribute_value'
    print sender
    print args


def dummy_ble_evt_attclient_read_multiple_response(sender, args):
    print 'dummy_ble_evt_attclient_read_multiple_response'
    print sender
    print args


def sendFormat(message):
    line=[]
    line=message.split()

    if len(line) == 30:
        parsedLineIndex = [1, 7, 8, 9, 15, 16, 17, 23, 24, 25]
        timestamp = time.time()
        return [line[i] for i in parsedLineIndex]+[str(timestamp)]
    else:
        return None

        
class Capteur:
    def seekPort(self):
        ports = list(serial.tools.list_ports.comports())
        print ports
        for p in ports:
            if "CDC " in p[1]:
                print 'in capteur port number found :' + p[0]
                return p[0]
       
        print 'Aucun Koondal connecte veuillez rebranchez et relancer l\'application\n'
        raise Exception('NOPORT')
        
    def __init__(self, cmd='r'):
        try:
            port = self.seekPort()
        except Exception:
            raise
        try:
            global ble, ser, cmd_to_send
            cmd_to_send = ""
            ble = bglib.BGLib()
            ble.on_timeout += my_timeout
            
            ble.ble_evt_gap_scan_response += my_ble_evt_gap_scan_response
            ble.ble_evt_connection_status += my_ble_evt_connection_status
            ble.ble_evt_attclient_find_information_found += my_ble_evt_attclient_find_information_found
            ble.ble_evt_attclient_procedure_completed += my_ble_evt_attclient_procedure_completed
            ble.ble_evt_attclient_attribute_value += my_ble_evt_attclient_attribute_value
        
            # create serial port object
            try:
                ser = serial.Serial(port=port, baudrate=115200, timeout=1, writeTimeout=1)
            except serial.SerialException as e:
                print "\n================================================================"
                print "Port error (name='%s', baud='%ld'): %s" % (port, 115200, e)
                print "================================================================"
                exit(2)
        
            # flush buffers
            ser.flushInput()
            ser.flushOutput()
        
            # disconnect if we are connected already
            ble.send_command(ser, ble.ble_cmd_connection_disconnect(0))
            ble.check_activity(ser, 1)
        
            # stop advertising if we are advertising already
            ble.send_command(ser, ble.ble_cmd_gap_set_mode(0, 0))
            ble.check_activity(ser, 1)
        
            # stop scanning if we are scanning already
            ble.send_command(ser, ble.ble_cmd_gap_end_procedure())
            ble.check_activity(ser, 1)
            
            # set scan parameters
            ble.send_command(ser, ble.ble_cmd_gap_set_scan_parameters(0xC8, 0xC8, 1))
            ble.check_activity(ser, 1)
        
            # start scanning now
            print "Scanning for BLE peripherals..."
            ble.send_command(ser, ble.ble_cmd_gap_discover(2))
            ble.check_activity(ser, 1)
                    
        except Exception, e:
            print e
            raise Exception('ERRORWINDOWS')
    
    def reset(self):
        try:
            port = self.seekPort()
        except Exception:
            raise
        while True:
            try:
                ser = serial.Serial(port, 38400, timeout=0.1)
                ser.flush()
                break
            except serial.SerialException:
                raise Exception('ERRORWINDOWS')
                time.sleep(3)
                
    def writeBuffer(self):
        global writeDone, cmd_to_send
        cmd_to_send = 'w'
        send_cmd()
        
        i = 0
        while True:
            i += 1
            ble.check_activity(ser)
            time.sleep(0.01)
            if writeDone:
                writeDone = False
                break

    def readBuffer(self):
        global movuino, readDone, fullLine, cmd_to_send, firstRead, rWasRead
        cmd_to_send = 'r'
        send_cmd()
        while True:
            ble.check_activity(ser)
            time.sleep(0.03)
            if movuino and attr_mv_rx_handle <= 0 and attr_mv_tx_handle <= 0 and attr_mv_rx_handle_ccc <= 0:
                # Mavuino not found
              
                movuino = False
                return []
            elif movuino:
                movuino = False
            if readDone:
                readDone = False
                print '*** Done ***', len(fullLine)
                sendList = [l for l in fullLine if len(l) == 11]
                firstRead = False
                rWasRead = False
                fullLine = []
                return sendList
            else:
                continue

    def askForOneLine(self):
        global cmd_to_send, lread, lineAsked, disco
        while True:
            if disco:
                return None
            ble.check_activity(ser)
            if state == STATE_LISTENING_NOTIFICATIONS:
                cmd_to_send = 'l'
                send_cmd()
            time.sleep(0.01)
            if lread:
                line = sendFormat(lineAsked)
                if line is None:
                    print 'FUUUUUUUUUUUUUUUUUUUUCCCCCCCCCKKKKKKKKKKED'
                    lread = False
                    lineAsked = ''
                    continue
                if len(line) == 11:
                    print '******', line, '********'
                    lread = False
                    lineAsked = ''
                    return line

    def stopLive(self):
        global cmd_to_send
        ble.check_activity(ser)
        if state == STATE_LISTENING_NOTIFICATIONS:
            print 'stop mofo'
            cmd_to_send = 'L'
            send_cmd()

    def close(self):
        global ble, ser
        print ser.isOpen()
        ser.close()
        print ser.isOpen()
        print '************'
    
    def disconnect(self):
        global disco
        disco = True
        time.sleep(0.1)
        ble.send_command(ser, ble.ble_cmd_gap_set_mode(0, 0))
        ble.check_activity(ser, 1)
        ble.send_command(ser, ble.ble_cmd_gap_end_procedure())
        ble.check_activity(ser, 1)
        ble.send_command(ser, ble.ble_cmd_connection_disconnect(0))
        ble.check_activity(ser, 1)


if __name__ == '__main__':
    capteur = Capteur()
    t0 = time.time()
    print 'couco'
    while True:
        l = capteur.askForOneLine()
        print time.time() - t0
        if l is not None:
            t0 = time.time()
