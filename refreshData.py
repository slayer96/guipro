# -*- coding: utf-8 -*-
import time
import os
import csv
import pandas
import numpy as np


from LogParser import getDicoIndicateur

DIRPATH = os.path.dirname(os.path.realpath(__file__))


def parseOutput():
    path = DIRPATH + '/Log/Output/output.csv'
    with open(path, mode='rb') as infile:
        reader = csv.reader(infile, delimiter=';')
        reader.next()
        mydict = {rows[0]: rows[1] for rows in reader}
    return mydict

listOutput = parseOutput().keys()


class Data:
    def initDicoData(self, listCurrentIndicateur):
        dicoData = {}
        self.listIndicatorKey = listCurrentIndicateur
        for key in self.listIndicatorKey:
            dicoData[key] = {'variance': 0, 'mean': 0}
        return dicoData
        
    def __init__(self, listCurrentIndicateur):
        self.data = self.initDicoData(listCurrentIndicateur)
        self.numberOfUpdate = 0
        
    def updateData(self, listValue):
        allIndicateurOk = True 
        listKey = [key for key in self.listIndicatorKey if key != 'timeStamp']
        for i in range(len(listKey)):
            try:
                self.data[listKey[i]]['mean'] = (self.numberOfUpdate * self.data[listKey[i]]['mean'] + listValue[i]) / (self.numberOfUpdate+1)
                self.data[listKey[i]]['variance'] = ((self.numberOfUpdate * self.data[listKey[i]]['variance']) + (listValue[i] - self.data[listKey[i]]['mean'])**2)/(self.numberOfUpdate +1)
                self.numberOfUpdate += 1
            except Exception, e:
                print e
            try:
                if float(float(listValue[i]) - float(self.data[listKey[i]]['mean'])) > float(6 * np.sqrt(self.data[listKey[i]]['variance'])):
                    allIndicateurOk = False
            except:
                print listKey
                print i
                print listValue
                time.sleep(10000)
                
        return allIndicateurOk


def parseTest(filepath):
    with open(filepath, 'rb') as f:
        reader = csv.reader(f)
        dataname = reader.next()[0].split('/Data/')
        return dataname[-1]


def parseTestRecord():
    listTest = []
    for root, dirs, files in os.walk(DIRPATH+'/TestRecord', topdown=False):
        for name in files:
            listTest.append([parseTest(root+'/'+name)][0])
    return listTest


def updateCumul(dicoCumul,line):
        dicoCumul['alt'] += line[0]
        dicoCumul['ax'] += line[1]
        dicoCumul['ay'] += line[2]
        dicoCumul['az'] += line[3]
        dicoCumul['gx'] += line[4]
        dicoCumul['gy'] += line[5]
        dicoCumul['gz'] += line[6]
        dicoCumul['tx'] += line[7]
        dicoCumul['ty'] += line[8]
        dicoCumul['tz'] += line[9]
        dicoCumul['ligne'] += 1
        dicoCumul['assis'] += line[11]
        dicoCumul['debout'] += line[12]
        dicoCumul['affaissement'] += line[13]
        dicoCumul['posture'] += line[14]
        
        return dicoCumul


def refreshData(fileName, dicoAllIndicateur):
    try:
        f = open(fileName, 'rb')
        reader = csv.reader(f)
        listColumnData = reader.next()
        f.close()
    except Exception, e:
        print e

    listAllIndicateur = getDicoIndicateur(path=DIRPATH+'/Dictionnaire/listeIndicateurName.p')

    listIndicKeep = ['Alt', 'Ax', 'Ay', 'Az',
                            'Gx', 'Gy', 'Gz',
                            'Tx', 'Ty', 'Tz',
                            'timeStamp']+[indic for indic in listColumnData[10:] if indic in listAllIndicateur]

    listIndicToAdd = [indic for indic in listAllIndicateur if indic not in listIndicKeep]
    listFunctionByLevel = getFunctionByLevel(listIndicToAdd)[10:]

    dico = {indic: [] for indic in listFunctionByLevel if indic != 'indicateurStd'}

    try:
        f = open(fileName, 'rb')
        reader = csv.reader(f)
        listColumnData = reader.next()

        listHistorique = []
        t0 = 0 
        dicoVariable = {}        
        
        afterOutput = 10
        for name in listColumnData[11:]:
            if name in listOutput:
                afterOutput += 1
            else:
                break
        afterOutput += 1
        
        for row in reader:
            if t0 == 0:
                timeS = 0
            else:
                timeS = float(row[10]) - float(t0)
            dicoLine = { 
                        'alt': row[0],
                        'ax': row[1], 'ay': row[2], 'az': row[3],
                        'gx': row[4], 'gy': row[5], 'gz': row[6],
                        'tx': row[7], 'ty': row[8], 'tz': row[9],
                        'time': timeS,
                        }

            # map en float
            dicoLine = {k: float(v) for k, v in dicoLine.items()}

            if len(listHistorique) > 100:
                listHistorique.pop(0)
            listHistorique.append(dicoLine)

            # REAL
            for function in listFunctionByLevel:
                indicateurResult = dicoAllIndicateur[function](dicoLine, listHistorique, dicoVariable)
                dico[function].append(float(indicateurResult))

    finally:
        f.close()
    dataCsv = pandas.read_csv(fileName, usecols=listIndicKeep, sep=',')

    for key in listFunctionByLevel:
        dataCsv[key] = dico[key]
    dataCsv.to_csv(fileName, sep=',', index_label=False, index=False)
    
    
def getFunctionByLevel(listIndicToAdd):
    listIndicateurByLevel = getDicoIndicateur(path=DIRPATH+'/Dictionnaire/listeIndicateurName.p')
    listIndicateurAddSortedByLevel = []    
    for name in listIndicateurByLevel:
        if name in listIndicToAdd:
            listIndicateurAddSortedByLevel.append(name)
    return listIndicateurAddSortedByLevel


def refreshAllData():
    listDataName = parseTestRecord()
    dirname = DIRPATH + '/Log/Data/'
    for root, dirs, files in os.walk(dirname, topdown=False):
        for name in files:
            if name in listDataName:
                refreshData(dirname+name, [], {})
          

def main():
    print 'blabla'
    dicoAllIndicateur = getDicoIndicateur(path=DIRPATH+'/Dictionnaire/indicateur.p')
    listDataName = parseTestRecord()
    dirname = DIRPATH + '/Log/Data/'
    __tmp = 0
    i = 0
    for root, dirs, files in os.walk(dirname, topdown=False):

        for name in files:
            if name in listDataName:
                t = time.time()
                print str(i) + '/' + str(len(listDataName))
                i += 1

                refreshData(dirname + name, dicoAllIndicateur)
                print '----> ' + str(time.time()-t) + ' sec'
                if __tmp < 1:
                    __tmp += 1
                else:
                    break

if __name__ == '__main__':
    main()
