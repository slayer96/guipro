# -*- coding: utf-8 -*-
import os
from Constant import *
from refreshData import refreshData
from sklearn import linear_model
from sklearn.decomposition import PCA
from sklearn import cross_validation
import csv
import numpy as np
import pandas
import itertools
from LogParser import getDicoIndicateur
import styleSheet
from PyQt4 import QtCore, QtGui


def parseOutput():
    path = DIRPATH + '/Log/Output/output.csv'
    with open(path, mode='rb') as infile:
        reader = csv.reader(infile, delimiter=';')
        reader.next()
        mydict = {rows[0]: rows[1] for rows in reader}
    return mydict
    
listOutput = parseOutput()


def parseTest(filepath, dicoOutput, listOutputTest, subsetUser):
    dico = dicoOutput
    dicoTmp = {}
    update = False
    memberIn = False
    
    with open(filepath, 'rb') as f:
        reader = f.readlines()
        dataname = reader[0].split('/Log')[-1]
        dataname = str(dataname).replace('\r', '')
        dataname = str(dataname).replace('\n', '')
        dataname = DIRPATH + '/Log' + dataname
        
        listOutputFormat = [output+':' for output in listOutputTest+subsetUser]
        for row in reader[1:]:
            row = row.replace('\r', '')
            if row.split(' ')[0] in listOutputFormat:
                outputName = row.split(':')[0]
                outputTest = float(row.split(' ')[-1])
                dicoTmp[outputName] = outputTest
                if outputName not in listOutputTest and outputName in subsetUser and outputTest == 1.0:
                    print outputName
                    memberIn = True
                if outputName in listOutputTest:
                    update = True

        if update and memberIn:
            for output in listOutputTest:
                dico[output].append((dataname, dicoTmp[output]))
        return dico


def parseTestRecordRandom(listOutputTest, subsetUser):
    dicoOutput = {output: []for output in listOutputTest}
    for root, dirs, files in os.walk(DIRPATH+'/TestRecord', topdown=False):
        for name in files:
            dicoOutput = parseTest(root+'/'+name, dicoOutput, listOutputTest, subsetUser)
  
    return dicoOutput


def addNoise(outputName,outputTest):
    listOutput = parseOutput()
    outputNoise = outputTest
    bruit = abs(np.random.randn()/5)
    
    if len(listOutput[outputName]) <= len('[-1,1]'):
        if outputTest == -1:
            outputNoise = (outputTest - bruit)
        elif outputTest == 1:
            outputNoise = (outputTest + bruit)
    else:
        if outputTest == 0:
            outputNoise = (outputTest - bruit)
        elif outputTest == 3:
            outputNoise = (outputTest + bruit)
    return outputNoise
    
'''
Purpose: Creation d'un set de test et leur réponse associé selon les test enregistré dans le fichier test et data
'''


def _getTestSet(listIndicateur, output, subsetUser):
    dicoOutput = parseTestRecordRandom([output], subsetUser)
    dicoTest = {output:
                    {'X': [[] for i in range(len(listIndicateur))], 'Y': [], 'Real': []}for output in dicoOutput.keys()}
    print 'extraction'
    for outputName,listDuo in dicoOutput.items():
        for duo in listDuo:
            nameFile = duo[0]
            outputAssocie = duo[1]
            print 'open : ' + nameFile
            dataCsv = pandas.read_csv(nameFile)
            i = 0
            for indicateurName in listIndicateur:
                try:
                    dicoTest[outputName]['X'][i] += map(float, dataCsv[indicateurName])
                except Exception, e:
                    print e
                    continue
                i += 1
            for i in range(len(dataCsv['Ax'])):
                dicoTest[outputName]['Y'].append(addNoise(outputName, outputAssocie))
                dicoTest[outputName]['Real'].append(outputAssocie)
            dicoTest[outputName]['NbTest'] = len(dicoOutput[outputName])
                
        #combinaison des colonnes
        print 'combinaison"'
        dicoTest[outputName]['X'] = [[t[i] for t in dicoTest[outputName]['X']] for i in range(len(dicoTest[outputName]['X'][0]))]
    
    return dicoTest


def updateLocalData():
    with open(DIRREFRESHDATAFILE, 'rb') as f:
        listUpload = f.readlines()
        
    numberUpdate = len(listUpload)
        
    if numberUpdate != 0:
        progress = QtGui.QProgressDialog(u"Mise à jour des fichier tests...", "Cancel", 0, numberUpdate)
        progress.setCancelButton(None)            
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        dicoAllIndicateur = getDicoIndicateur(path=DIRALLINDICATEUR)

        listFileData = map(lambda name:name.replace('\n', ''), listUpload)
        listFileData = map(lambda name:name.replace('\r', ''), listFileData)

        for fileName in listFileData:
            print '****' + fileName + '*****'
            refreshData(fileName, dicoAllIndicateur)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
        progress.close()


def requestRegression(output, listIndicateur, subsetUser, pca, typeRegression, progress=None):
        dicoTest = _getTestSet(listIndicateur, output, subsetUser)
        with open(DIRREFRESHDATAFILE, 'wb') as f:
            f.seek(0)
            f.truncate()  
            
        print 'Requete serveur en cours'
        #partie la plus longue, acquisition d'une array
        progress.setValue(progress.value()+2)
        QtCore.QCoreApplication.processEvents()
        print 'arratDictX done'
        
        if len(dicoTest[output]['X']) == 0:
            #requete null
            return {'fonction': None,
                'pca': None,
                'scoreIn': None,
                'scoreOut': None,
                'nbTest': None,
                'advice': u'Conseil: Assurer de téléverser vos tests (Onglet de Test)',
                'error': u'Aucun test trouvé pour ces personnes', }
        arrayX = dicoTest[output]['X']
        progress.setValue(progress.value()+1)
        print 'arrayX done'
        QtCore.QCoreApplication.processEvents()
        arrayY = dicoTest[output]['Real']
        print 'arrayY done'
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        arrayYNoise = dicoTest[output]['Y']
        print 'arrayY done'
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        middle = len(arrayX)/2
        
        #equilibrage et meleange
        
        ratio = None
        if output != 'Repartitionmouvement_X-Y-Z':
            listIndexTrue = []
            listIndexFalse = []
            for i in range(len(arrayY)):
                if arrayY[i] == 1:
                    listIndexTrue += [i]
                else:
                    listIndexFalse += [i]
            print 'separete True from FalseDone'
            nFalse = len(listIndexFalse)
            nTrue = len(listIndexTrue)
            if nFalse == 0 or nTrue == 0:
                #pas de test
                return {'fonction': None,
                'pca': None,
                'scoreIn': None,
                'scoreOut': None,
                'nbTest': None,
                'advice': u'Conseil: Assurer de téléverser vos tests (Onglet de Test) et vérifier si vous avez bien fait des tests Vrai ET Faux',
                'error': 'Pas assez de test Vrai ou de test Faux', }
            print 'shuffle matrice'
            np.random.shuffle(listIndexFalse)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            np.random.shuffle(listIndexTrue)
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            print 'shuffle both matrice done'

            minSize = min(nFalse, nTrue)
            iters = [iter(listIndexFalse[:minSize]), iter(listIndexTrue[:minSize])]
            listAllIndexes = list(it.next() for it in itertools.cycle(iters))
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            if max(nFalse, nTrue)/minSize > 1.3:
                ratio = float(max(nFalse, nTrue))/float(minSize)
                biggestResponse = 'True' if nTrue >= nFalse else 'False'
                lowestResponse = 'False' if nTrue >= nFalse else 'True'
            
            print 'Two list merge done'
            arrayX = [arrayX[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            
            arrayY = [arrayY[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            
            arrayYNoise = [arrayYNoise[i] for i in listAllIndexes]
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
        
            print 'three Array re ordonned done'

        middle = len(arrayX)/2
        #découpage en training et testingSet
        trainingX = arrayX[:middle]
        trainingY = arrayY[:middle]
        trainingYNoise = arrayYNoise[:middle]
        testingX = arrayX[middle:]
        testingY = arrayY[middle:]
        
        dico = regression(trainingX, trainingY, trainingYNoise, typeRegression, testingX=testingX, testingY=testingY,
                          pcaComponent=pca, output=output, subsetUser=subsetUser, indicateurNames=listIndicateur)
        progress.setValue(progress.value()+1)
        QtCore.QCoreApplication.processEvents()
        
        dico['nbTest'] = dicoTest[output]['NbTest']
        dico['error'] = None
        if ratio is not None:
            ratio = str("%.2f ") % ratio
            dico['advice'] =u'Warning: La quantité de test(' + output + ') ' + biggestResponse + ' est ' + ratio + \
                            ' fois plus grand que ' + lowestResponse
        else:
            dico['advice'] = None
        
        return dico


def regression(trainingX, trainingY, trainingYNoise, typeRegression, testingX=[], testingY=[], pcaComponent=0,
               output='Assis', subsetUser=['Mourougan','Tarik','Paul'],
               indicateurNames=['alt','ax','ay','az','gx','gy','gz','tx','ty','tz']):
    if pcaComponent == 0:
        pca = None
    else:
        pca = PCA(pcaComponent)
        trainingX = pca.fit_transform(trainingX)
        testingX = pca.fit_transform(testingX)

    print 'print wait for fitting'

    if typeRegression == 'Ridge':
        #On utilise une array Y avec du bruit
        regressionFonction = linear_model.Ridge(normalize=True)
        regressionFonction.fit(trainingX, trainingYNoise)
    elif typeRegression == 'Logistiquecv':
        regressionFonction = linear_model.LogisticRegressionCV(penalty='l2', tol=0.0001)
        regressionFonction.fit(trainingX, trainingY)
    elif typeRegression == 'Logistique':
        regressionFonction = linear_model.LogisticRegression(penalty='l2', tol=0.0001)
        regressionFonction.fit(trainingX, trainingY)
    else:
        regressionFonction = linear_model.LogisticRegression(penalty='l2', tol=0.0001)
        regressionFonction.fit(trainingX, trainingY)

    if typeRegression == 'Logistique' or typeRegression == 'LogistiqueCV':
        scoreIn = regressionFonction.score(trainingX, trainingY)
    else:
        scoreIn = regressionFonction.score(trainingX, trainingY)
        
    scores = cross_validation.cross_val_score(regressionFonction, testingX, testingY, cv=5)
    print scores   
    print ' std  ' + str(scores.std())
    scoreOut = "%0.2f" % (scores.mean()-(scores.std() * 2))
    scoreOut = float(scoreOut)
    print 'scoreOUt ======   ' + str(scoreOut)
    dicoFonction = {output: regressionFonction}
    
    dico = {'fonction': dicoFonction,
            'pca': pca,
            'scoreIn': scoreIn,
            'scoreOut': scoreOut, }
    return dico
