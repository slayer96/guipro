# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 12:52:06 2015

@author: Mourougan
"""
import LogParser
import os
import numpy as np
DIRPATH = os.path.dirname(os.path.realpath(__file__))

'''
LISTE DES FONCTION INDICATEUR
'''


def ARR(fonctionName, inputName, arrondi, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = dicoLine[inputName]/arrondi
    return dicoVariable[fonctionName]['CURRENT_VAL']


def MME(fonctionName, inputName, alpha, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['CURRENT_VAL'] = dicoLine[inputName]
        dicoVariable[fonctionName]['ALPHA'] = alpha
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    dicoVariable[fonctionName]['CURRENT_VAL'] = dicoVariable[fonctionName]['CURRENT_VAL']*(1-alpha) +\
                                                (dicoLine[inputName] * alpha)
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEAL2(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.sqrt(dicoLine['ax']**2 + dicoLine['ay']**2 + dicoLine['az']**2)
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEGL2(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.sqrt(dicoLine['gx']**2 + dicoLine['gy']**2 + dicoLine['gz']**2)
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEML2(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.sqrt(dicoLine['tx']**2 + dicoLine['ty']**2 + dicoLine['tz']**2)
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEAL1(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.abs(dicoLine['ax']) + np.abs(dicoLine['ay']) + np.abs(dicoLine['az']) 
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEGL1(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.abs(dicoLine['gx']) + np.abs(dicoLine['gy']) + np.abs(dicoLine['gz']) 
    return dicoVariable[fonctionName]['CURRENT_VAL']


def NORMEML1(fonctionName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.abs(dicoLine['tx']) + np.abs(dicoLine['ty']) + np.abs(dicoLine['tz']) 
    return dicoVariable[fonctionName]['CURRENT_VAL']
        
    
def MM(fonctionName, inputName, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['NBUPDATE'] = 1
        dicoVariable[fonctionName]['CURRENT_VAL'] = dicoLine[inputName]
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    n = dicoVariable[fonctionName]['NBUPDATE'] if dicoVariable[fonctionName]['NBUPDATE'] < 100 else 100

    dicoVariable[fonctionName]['CURRENT_VAL'] = ((dicoVariable[fonctionName]['CURRENT_VAL']*n) +
                                                 (dicoLine[inputName]-lastLine[0][inputName]))/(n+1)
    dicoVariable[fonctionName]['NBUPDATE'] = n+1 if n < 100 else n
    return dicoVariable[fonctionName]['CURRENT_VAL']
    

def VAR(fonctionName, inputName, rangeVAR, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['MEAN'] = dicoLine[inputName]
        dicoVariable[fonctionName]['NBUPDATE'] = 1
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    n = dicoVariable[fonctionName]['NBUPDATE']
    dicoVariable[fonctionName]['MEAN'] = (dicoVariable[fonctionName]['MEAN']*n + dicoLine[inputName]) / (n+1)
    mean = dicoVariable[fonctionName]['MEAN']
    currentValue = dicoVariable[fonctionName]['CURRENT_VAL']
    dicoVariable[fonctionName]['CURRENT_VAL'] = (currentValue*n + (dicoLine[inputName]-mean)**2)/(1+n)
    dicoVariable[fonctionName]['NBUPDATE'] = n+1 if n < rangeVAR else rangeVAR
    return dicoVariable[fonctionName]['CURRENT_VAL']
 

def CMP(fonctionName, inputSource, inputCible, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
    dicoVariable[fonctionName]['CURRENT_VAL'] = 1 if dicoLine[inputSource] >= dicoLine[inputCible] else 0
    return dicoVariable[fonctionName]['CURRENT_VAL']


def COR(fonctionName, inputSource, inputCible, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
        return dicoVariable[fonctionName]['CURRENT_VAL']
    lastLineSrc = [line[inputSource] for line in lastLine[-50:]]
    lastLineCible =[line[inputCible] for line in lastLine[-50:]]
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.corrcoef(lastLineSrc, lastLineCible)[0][1]
    return dicoVariable[fonctionName]['CURRENT_VAL']


def AUTOCOR(fonctionName, inputSrc, pas, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
    if len(lastLine) <= 51+pas:
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    lastLineSrc = [line[inputSrc] for line in lastLine[-50:]]
    lastLineSrcK = [line[inputSrc] for line in lastLine[-50-pas-1:-pas-1]]
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.corrcoef(lastLineSrc, lastLineSrcK)[0][1]
    if np.isnan(dicoVariable[fonctionName]['CURRENT_VAL']):
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0.5
    return dicoVariable[fonctionName]['CURRENT_VAL']


def AUTOCORARR(fonctionName, inputSrc, arrondi, pas, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
    if len(lastLine) <= 51+pas:
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    lastLineSrc = [line[inputSrc]/arrondi for line in lastLine[-50:]]
    lastLineSrcK = [line[inputSrc]/arrondi for line in lastLine[-50-pas-1:-pas-1]]
    dicoVariable[fonctionName]['CURRENT_VAL'] = np.corrcoef(lastLineSrc, lastLineSrcK)[0][1]
    if np.isnan(dicoVariable[fonctionName]['CURRENT_VAL']):
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0.5
    return dicoVariable[fonctionName]['CURRENT_VAL']    

'''
FONCTION QUI S'APPLIQUE SUR TOUTE LES AUTRES NIVEAU 1
'''


def MINLOCAL(fonctionName, inputName, rangeLocal, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
            dicoVariable[fonctionName]['CURRENT_VAL'] = dicoLine[inputName]
        else:
            dicoVariable[fonctionName][inputName] = []
            dicoVariable[fonctionName][inputName] += [dicoVariable[inputName]['CURRENT_VAL']]
            dicoVariable[fonctionName]['CURRENT_VAL'] = min(dicoVariable[fonctionName][inputName])

    else:
        if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
            dicoVariable[fonctionName]['CURRENT_VAL'] = min([cell[inputName] for cell in lastLine[-rangeLocal:]])
        else:
            if len(dicoVariable[fonctionName][inputName])>rangeLocal:
                dicoVariable[fonctionName][inputName].pop(0)
            dicoVariable[fonctionName][inputName]+=[dicoVariable[inputName]['CURRENT_VAL']]
            dicoVariable[fonctionName]['CURRENT_VAL'] = min(dicoVariable[fonctionName][inputName])
            
    return dicoVariable[fonctionName]['CURRENT_VAL']
    
def MAXLOCAL(fonctionName,inputName,rangeLocal,dicoLine,lastLine,dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
            dicoVariable[fonctionName]['CURRENT_VAL'] = dicoLine[inputName]
        else:
            dicoVariable[fonctionName][inputName] = []
            dicoVariable[fonctionName][inputName] += [dicoVariable[inputName]['CURRENT_VAL']]
            dicoVariable[fonctionName]['CURRENT_VAL'] = max(dicoVariable[fonctionName][inputName])

    else:
        if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
            dicoVariable[fonctionName]['CURRENT_VAL'] = max([cell[inputName] for cell in lastLine[-rangeLocal:]])
        else:
            if len(dicoVariable[fonctionName][inputName]) > rangeLocal:
                dicoVariable[fonctionName][inputName].pop(0)
            dicoVariable[fonctionName][inputName] += [dicoVariable[inputName]['CURRENT_VAL']]
            dicoVariable[fonctionName]['CURRENT_VAL'] = max(dicoVariable[fonctionName][inputName])
            
    return dicoVariable[fonctionName]['CURRENT_VAL']


def ECCART(fonctionName, inputName, eccartTolerable, dicoLine, lastLine, dicoVariable):
    if fonctionName not in dicoVariable:
        dicoVariable[fonctionName] = {}
        if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
            dicoVariable[fonctionName]['MEAN'] = dicoLine[inputName]
        else:
            dicoVariable[fonctionName]['MEAN'] = dicoVariable[inputName]['CURRENT_VAL']
        
        dicoVariable[fonctionName]['VAR'] = 0
        dicoVariable[fonctionName]['ECCART'] = 0
        dicoVariable[fonctionName]['NBUPDATE'] = 1
        dicoVariable[fonctionName]['CURRENT_VAL'] = 0
        return dicoVariable[fonctionName]['CURRENT_VAL']
    
    n = dicoVariable[fonctionName]['NBUPDATE']
    
    if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
        currentVal = dicoLine[inputName]
        mean = (dicoVariable[fonctionName]['MEAN']*n + currentVal) / (n+1)
    
    else:
        currentVal = dicoVariable[inputName]['CURRENT_VAL']
        mean = (dicoVariable[fonctionName]['MEAN']*n + currentVal) / (n+1)

    lastVar = dicoVariable[fonctionName]['VAR']
    dicoVariable[fonctionName]['VAR'] = (lastVar*n+ (currentVal-mean)**2)/(1+n)
    dicoVariable[fonctionName]['MEAN'] = mean
    lastEccart = dicoVariable[fonctionName]['ECCART']
    currentVariance = dicoVariable[fonctionName]['VAR']
    dicoVariable[fonctionName]['ECCART'] = (lastEccart*n + np.sqrt(currentVariance))/(1+n)
    dicoVariable[fonctionName]['NBUPDATE'] = n+1 if n < 10 else 10
         
    dicoVariable[fonctionName]['CURRENT_VAL'] = 0 if currentVal < (mean+(dicoVariable[fonctionName]['ECCART'] * eccartTolerable)) else 1
    return dicoVariable[fonctionName]['CURRENT_VAL']
'''
FONCTION QUI S'APPLIQUE SUR TOUTE LES AUTRES NIVEAU 2
'''


def EXP(fonctionName, inputName, dicoLine, lastLine, dicoVariable):
    if inputName in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
        return np.exp(dicoLine[inputName])
    else:
        return np.exp(dicoVariable[inputName]['CURRENT_VAL'])


def LOG(fonctionName,inputName,dicoLine,lastLine,dicoVariable):
    if inputName in ['ax', 'ay', 'az', 'gx', 'gy' ,'gz', 'tx', 'ty', 'tz', 'alt']:
        if dicoLine[inputName] < 0:
            return -999999
        return np.log(dicoLine[inputName])
    else:
        if dicoVariable[inputName]['CURRENT_VAL'] <0:
            return -999999
        return np.log(dicoVariable[inputName]['CURRENT_VAL'])        
        
'''
LISTE DES FONCTION D'AJOUT d'INDICATEUR
'''


def addFonctionMME(fonctionName, inputName, alpha, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputName=inputName,
                                            alpha=alpha: MME(fonctionName, inputName, alpha, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionMM(fonctionName, inputName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName,
                                            inputName=inputName: MM(fonctionName, inputName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionVAR(fonctionName, inputName, rangeVAR, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar,rangeVAR=rangeVAR, fonctionName=fonctionName,
                                            inputName=inputName: VAR(fonctionName, inputName, rangeVAR, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionARR(fonctionName, inputName, arrondi, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputName=inputName,
                                            arrondi=arrondi: ARR(fonctionName, inputName, arrondi, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEAL2(fonctionName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar,
                                            fonctionName=fonctionName: NORMEAL2(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEGL2(fonctionName,indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar,
                                          fonctionName=fonctionName: NORMEGL2(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEML2(fonctionName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar,
                                            fonctionName=fonctionName: NORMEML2(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEAL1(fonctionName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName = fonctionName: NORMEAL1(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEGL1(fonctionName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName: NORMEGL1(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionNORMEML1(fonctionName, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName: NORMEML1(fonctionName, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionCMP(fonctionName, inputSource, inputCible, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputSource=inputSource,
                                            inputCible=inputCible: CMP(fonctionName, inputSource, inputCible, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionCOR(fonctionName, inputSource, inputCible, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputSource=inputSource,
                                            inputCible=inputCible: COR(fonctionName, inputSource, inputCible, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionAUTOCOR(fonctionName, inputSource, pas, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputSource=inputSource,
                                            pas=pas: AUTOCOR(fonctionName, inputSource, pas, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionAUTOCORARR(fonctionName, inputSource, arrondi, pas, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName, inputSource=inputSource,
                                            arrondi=arrondi, pas=pas: AUTOCORARR(fonctionName, inputSource, arrondi, pas, dicoLine, lastLine, dicoVar))
    return indicateurTotal
    
def addFonctionMINLOCAL(fonctionName,inputSource,rangeLocal,indicateurTotal):
    if rangeLocal<100:
        indicateurTotal[fonctionName]=(lambda dicoLine,lastLine,dicoVar,fonctionName=fonctionName,inputSource=inputSource,rangeLocal=rangeLocal:MINLOCAL(fonctionName,inputSource,rangeLocal,dicoLine,lastLine,dicoVar))
        return indicateurTotal


def addFonctionMAXLOCAL(fonctionName, inputSource, rangeLocal, indicateurTotal):
    if rangeLocal < 100:
        indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName,
                                                inputSource=inputSource, rangeLocal=rangeLocal: MAXLOCAL(fonctionName, inputSource,rangeLocal, dicoLine, lastLine, dicoVar))
        return indicateurTotal 


def addFonctionECCART(fonctionName,inputSource,eccartTolerable,indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName,
                                            inputSource=inputSource,
                                            eccartTolerable=eccartTolerable: ECCART(fonctionName, inputSource, eccartTolerable, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionEXP(fonctionName, inputSource, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine ,lastLine, dicoVar, fonctionName=fonctionName,
                                            inputSource=inputSource: EXP(fonctionName, inputSource, dicoLine, lastLine, dicoVar))
    return indicateurTotal


def addFonctionLOG(fonctionName, inputSource, indicateurTotal):
    indicateurTotal[fonctionName] = (lambda dicoLine, lastLine, dicoVar, fonctionName=fonctionName,
                                            inputSource=inputSource: LOG(fonctionName, inputSource, dicoLine, lastLine, dicoVar))
    return indicateurTotal
    
filePath = DIRPATH + '\\Dictionnaire\\indicateur.p'
indicateurTotal = {}

indicBrut = ['ax', 'ay', 'az', 'tx', 'ty', 'tz', 'gx', 'gy', 'gz', 'alt']
dicoIndicateurLvl = {brut: 'None' for brut in indicBrut}
i = 1

'''
INDICATEUR DE NIVEAU 0
'''
for indic in indicBrut:
    print indic
    for i in range(8):
        nb = str(20+(i*10))
        indicateurTotal = addFonctionMME('MME_0'+nb+'_'+indic, indic, float(nb)/100, indicateurTotal)
        dicoIndicateurLvl['MME_0'+nb+'_'+indic] = 'None'
        
    indicateurTotal = addFonctionVAR('VAR_10_'+indic, indic, 10, indicateurTotal)
    dicoIndicateurLvl['VAR_10_'+indic] = 'None'
    for j in range(50):
        if j < 10:
            indicateurTotal =addFonctionAUTOCOR('AUTOCOR_'+indic+'_0'+str(j), indic, j, indicateurTotal)
            dicoIndicateurLvl['AUTOCOR_'+indic+'_0'+str(j)] = 'None'
        else:
            indicateurTotal = addFonctionAUTOCOR('AUTOCOR_'+indic+'_'+str(j), indic, j, indicateurTotal)
            dicoIndicateurLvl['AUTOCOR_'+indic+'_'+str(j)] = 'None'
    if indic == 'alt':
            break
    for subIndic in indicBrut[i:]:
        indicateurTotal = addFonctionCOR('COR_'+indic+'-'+subIndic, indic, subIndic, indicateurTotal)
        dicoIndicateurLvl['COR_'+indic+'-'+subIndic]='None' 
    i += 1

indicateurTotal = addFonctionARR('ARRAX', 'ax', 140, indicateurTotal)
indicateurTotal = addFonctionARR('ARRAY', 'ay', 120, indicateurTotal)
indicateurTotal = addFonctionARR('ARRAZ', 'az', 130, indicateurTotal)
indicateurTotal = addFonctionARR('ARRGX', 'gx', 50, indicateurTotal)
indicateurTotal = addFonctionARR('ARRGY', 'gy', 55, indicateurTotal)
indicateurTotal = addFonctionARR('ARRGZ', 'gz', 55, indicateurTotal)
indicateurTotal = addFonctionARR('ARRTX', 'tx', 2, indicateurTotal)
indicateurTotal = addFonctionARR('ARRTY', 'ty', 2, indicateurTotal)
indicateurTotal = addFonctionARR('ARRTZ', 'tz', 2, indicateurTotal)
dicoIndicateurLvl['ARRAX'] = 'None'
dicoIndicateurLvl['ARRAY'] = 'None'
dicoIndicateurLvl['ARRAZ'] = 'None'
dicoIndicateurLvl['ARRGX'] = 'None'
dicoIndicateurLvl['ARRGY'] = 'None'
dicoIndicateurLvl['ARRGZ'] = 'None'
dicoIndicateurLvl['ARRTX'] = 'None'
dicoIndicateurLvl['ARRTY'] = 'None'
dicoIndicateurLvl['ARRTZ'] = 'None'

for i in [5, 10, 45]:
    if i == 5:
        indicateurTotal =addFonctionAUTOCORARR('AUTOCORARR_ax_0'+str(i), 'ax', 140, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ax_0'+str(i)] = 'None'
        indicateurTotal =addFonctionAUTOCORARR('AUTOCORARR_ay_0'+str(i), 'ay', 120, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ay_0'+str(i)] = 'None'

        indicateurTotal =addFonctionAUTOCORARR('AUTOCORARR_az_0'+str(i), 'az', 130, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_az_0'+str(i)] = 'None'

        indicateurTotal =addFonctionAUTOCORARR('AUTOCORARR_gx_0'+str(i), 'gx', 50, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gx_0'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_gy_0'+str(i), 'gy', 55, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gy_0'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_gz_0'+str(i), 'gz', 55, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gz_0'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_tx_0'+str(i), 'tx', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_tx_0'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_ty_0'+str(i), 'ty', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ty_0'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_tz_0'+str(i), 'tz', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_tz_0'+str(i)] = 'None'
        
    else:
        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_ax_'+str(i), 'ax', 140, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ax_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_ay_'+str(i), 'ay', 120, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ay_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_az_'+str(i), 'az', 130, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_az_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_gx_'+str(i), 'gx', 50, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gx_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_gy_'+str(i), 'gy', 55, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gy_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_gz_'+str(i), 'gz', 55, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_gz_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_tx_'+str(i), 'tx', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_tx_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_ty_'+str(i), 'ty', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_ty_'+str(i)] = 'None'

        indicateurTotal = addFonctionAUTOCORARR('AUTOCORARR_tz_'+str(i), 'tz', 2, i, indicateurTotal)
        dicoIndicateurLvl['AUTOCORARR_tz_'+str(i)] = 'None'


indicateurTotal = addFonctionNORMEAL2('NORME_L2_ACCELERATION', indicateurTotal)
indicateurTotal = addFonctionNORMEGL2('NORME_L2_ROTATION', indicateurTotal)
indicateurTotal = addFonctionNORMEML2('NORME_L2_MAGNETOMETRE', indicateurTotal)
indicateurTotal = addFonctionNORMEAL1('NORME_L1_ACCELERATION', indicateurTotal)
indicateurTotal = addFonctionNORMEGL1('NORME_L1_ROTATION', indicateurTotal)
indicateurTotal = addFonctionNORMEML1('NORME_L1_MAGNETOMETRE', indicateurTotal)

indicateurTotal = addFonctionCMP('AX>AY', 'ax', 'ay', indicateurTotal)
indicateurTotal = addFonctionCMP('AX>AZ', 'ax', 'az', indicateurTotal)
indicateurTotal = addFonctionCMP('AY>AZ', 'ay', 'az', indicateurTotal)
indicateurTotal = addFonctionCMP('GX>GY', 'gx', 'gy', indicateurTotal)
indicateurTotal = addFonctionCMP('GX>GZ', 'gx', 'gz', indicateurTotal)
indicateurTotal = addFonctionCMP('GY>GZ', 'gy', 'gz', indicateurTotal)
indicateurTotal = addFonctionCMP('TX>TY', 'tx', 'ty', indicateurTotal)
indicateurTotal = addFonctionCMP('TX>TZ', 'tx', 'tz', indicateurTotal)
indicateurTotal = addFonctionCMP('TY>TZ', 'ty', 'tz', indicateurTotal)

dicoIndicateurLvl['NORME_L2_ACCELERATION'] = 'None'
dicoIndicateurLvl['NORME_L2_ROTATION'] = 'None'
dicoIndicateurLvl['NORME_L2_MAGNETOMETRE'] = 'None'
dicoIndicateurLvl['NORME_L1_ACCELERATION'] = 'None'
dicoIndicateurLvl['NORME_L1_ROTATION'] = 'None'
dicoIndicateurLvl['NORME_L1_MAGNETOMETRE'] = 'None'

dicoIndicateurLvl['AX>AY'] = 'None'
dicoIndicateurLvl['AX>AZ'] = 'None'
dicoIndicateurLvl['AY>AZ'] = 'None'
dicoIndicateurLvl['GX>GY'] = 'None'
dicoIndicateurLvl['GX>GZ'] = 'None'
dicoIndicateurLvl['GY>GZ'] = 'None'
dicoIndicateurLvl['TX>TY'] = 'None'
dicoIndicateurLvl['TX>TZ'] = 'None'
dicoIndicateurLvl['TY>TZ'] = 'None'


print 'indicateur lvl 0 done'
'''
INDICATEUR DE NIVEAU 1
'''
#Premiere etape retrouver les indicateur utilisé precedement
indicateurLvl0 = []
for indic in indicBrut:
    
    for i in range(8):
        nb = str(20+(i*10))
        indicateurLvl0 += ['MME_0'+nb+'_'+indic]

    for j in range(50):
        if j < 10:
            indicateurLvl0 += ['AUTOCOR_'+indic+'_0'+str(j)]
        else:
            indicateurLvl0 += ['AUTOCOR_'+indic+'_'+str(j)]
    if indic == 'alt':
            break
    for subIndic in indicBrut[i:]:
        indicateurLvl0 += ['COR_'+indic+'-'+subIndic]
    i += 1

indicateurLvl0 += ['NORME_L2_ACCELERATION']
indicateurLvl0 += ['NORME_L2_ROTATION']
indicateurLvl0 += ['NORME_L2_MAGNETOMETRE']
indicateurLvl0 += ['NORME_L1_ACCELERATION']
indicateurLvl0 += ['NORME_L1_ROTATION']
indicateurLvl0 += ['NORME_L1_MAGNETOMETRE']

indicateurLvl0 += ['AX>AY']
indicateurLvl0 += ['AX>AZ']
indicateurLvl0 += ['AY>AZ']
indicateurLvl0 += ['GX>GY']
indicateurLvl0 += ['GX>GZ']
indicateurLvl0 += ['GY>GZ']
indicateurLvl0 += ['TX>TY']
indicateurLvl0 += ['TX>TZ']
indicateurLvl0 += ['TY>TZ']


#Deuxieme etape on ajoute les fonctions de niveau 1
indicateurLvl1 = []
for fonctionName in indicBrut+indicateurLvl0:
    
    indicateurTotal = addFonctionMINLOCAL('MINLOCAL('+fonctionName+')', fonctionName, 50, indicateurTotal)
    indicateurTotal = addFonctionMAXLOCAL('MAXLOCAL('+fonctionName+')', fonctionName, 50, indicateurTotal)
    indicateurTotal = addFonctionECCART('ECCART_3('+fonctionName+')', fonctionName, 3, indicateurTotal)

    indicateurLvl1 += ['MINLOCAL('+fonctionName+')']
    indicateurLvl1 += ['MAXLOCAL('+fonctionName+')']
    indicateurLvl1 += ['ECCART_3('+fonctionName+')']
    
    dicoIndicateurLvl['MINLOCAL('+fonctionName+')'] = fonctionName
    dicoIndicateurLvl['MAXLOCAL('+fonctionName+')'] = fonctionName
    dicoIndicateurLvl['ECCART_3('+fonctionName+')'] = fonctionName

print u'indicateur LVL I inséré'
indicateurLvl2 = []
error2 = []
'''
INDICATEUR DE NIVEAU II
'''

LogParser.dumpDicoIndicateur(indicateurTotal, path=filePath)
filePath = DIRPATH + '\\Dictionnaire\\listeIndicateurName.p'
LogParser.dumpDicoIndicateur(indicBrut+indicateurLvl0+indicateurLvl1, path=filePath)
filePath = DIRPATH + '\\Dictionnaire\\dicoIndicateurLvl.p'
LogParser.dumpDicoIndicateur(dicoIndicateurLvl, path=filePath)
