# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from Constant import DIRTESTRECORD, DIRDATACSV, DIRREFRESHDATAFILE, DIRALLINDICATEUR, DIRPATH
import os
import styleSheet
from ServerRequest import ServerRequest
from LogParser import getDicoIndicateur
from refreshData import refreshData

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


def getAllCheckedItem(item):
    if item.checkState(0) == QtCore.Qt.Checked and item.childCount() == 0:
        return [str(item.data(0, QtCore.Qt.UserRole).toPyObject())]
    else:
        l = []
        for i in range(item.childCount()):
            elem = getAllCheckedItem(item.child(i))
            if elem != []:
                l += elem
        return l


def getAllItem(item):
    if (item.checkState(0) == QtCore.Qt.Checked or item.checkState(0) == QtCore.Qt.Unchecked) and item.childCount() == 0:
        return [str(item.data(0, QtCore.Qt.UserRole).toPyObject())]
    else:
        l = []
        for i in range(item.childCount()):
            elem = getAllItem(item.child(i))
            if elem != []:
                l += elem
        return l


def getAllUnCheckedItem(item):
    if item.checkState(0) == QtCore.Qt.Unchecked and item.childCount() == 0:
        return [str(item.data(0, QtCore.Qt.UserRole).toPyObject())]
    else:
        l = []
        for i in range(item.childCount()):
            elem = getAllUnCheckedItem(item.child(i))
            if elem:
                l += elem
        return l


def getAllCheckedItemSource(item):
    if item.checkState(0) == QtCore.Qt.Checked and item.childCount() == 0:
        return [item]
    else:
        l = []
        for i in range(item.childCount()):
            elem = getAllCheckedItemSource(item.child(i))
            if elem:
                l += elem
        return l


def confirmDelete(widget, numberTestFile):
    confirmTest = "Etez vous sur de vouloir supprimer  %d fichier test?" % (numberTestFile)
    result = QtGui.QMessageBox.question(widget,
                                        "Confirmation de Suppression...",
                                        confirmTest,
                                        QtGui.QMessageBox.Yes| QtGui.QMessageBox.No)
    if result == QtGui.QMessageBox.Yes:
        return True
    return False 


def confirmUpload(widget, numberTestFile, serverRequest, listUpload):
    confirmTest = u"Etez vous sur de vouloir téléverser  %d fichier test?" % numberTestFile
    result = QtGui.QMessageBox.question(widget,
                                        "Confirmation de téléversement...",
                                        confirmTest,
                                        QtGui.QMessageBox.Yes| QtGui.QMessageBox.No)
        
    if result == QtGui.QMessageBox.Yes:
        numberUpdate = len(listUpload)
        if numberUpdate != 0:
            progress = QtGui.QProgressDialog(u"Mise à jour des fichier tests...", "Cancel", 0, numberUpdate)
            progress.setCancelButton(None)            
            progress.setStyleSheet(styleSheet.getStyle())
            progress.show()
            progress.setValue(progress.value()+1)
            QtCore.QCoreApplication.processEvents()
            dicoAllIndicateur = getDicoIndicateur(path=DIRALLINDICATEUR)

            listFileData = map(lambda name: name.replace('\n', ''), listUpload)
            listFileData = map(lambda name: name.replace('\r', ''), listFileData)

            for testFile in listFileData:
                try:
                    with open(testFile, 'rb')as f:
                        fileName = f.readline()
                        fileName = fileName.replace('\n', '')
                        fileName = fileName.replace('    r', '')
                        fileName = DIRPATH + '/Log/Data/' + fileName.split('/')[-1]
                        print '****' + fileName + '*****'
                        refreshData(fileName, dicoAllIndicateur)
                        progress.setValue(progress.value()+1)
                        QtCore.QCoreApplication.processEvents()
                except:
                    progress.setValue(progress.value()+1)
                    QtCore.QCoreApplication.processEvents()
                    continue
            progress.close()
        
        serverRequest.uploadTest(listUpload)
        return True
    return False


def deleteSelectedFilesLocal(widget, treeWidget):
    root = treeWidget.invisibleRootItem()
    listDelete = getAllCheckedItem(root)
    listSourceDelete = getAllCheckedItemSource(root)
    if confirmDelete(widget, len(listDelete)):
        for fileTestName in listDelete:
            try:
                with open(fileTestName, 'rb') as f:
                    fileDataName = f.readline()
                    fileDataName = fileDataName.replace('\n', '')
                    fileDataName = fileDataName.replace('\r', '')
                    fileDataName = fileDataName.split('/')[-1]
                    fileDataName = DIRDATACSV+fileDataName
            except Exception:
                continue
            try:
                os.remove(fileTestName)
                os.remove(fileDataName)
            except Exception, e:
                print e
                continue
        for item in listSourceDelete:
            (item.parent() or root).removeChild(item)


def uploadSelectedFilesLocal(widget, treeWidget, treeWidgetBDD, serverRequest):
    root = treeWidget.invisibleRootItem()
    listUpload = getAllItem(root)
    print len(listUpload)
    print listUpload[:10]
    listUpload = removeRedundant(listUpload, treeWidgetBDD)
    print len(listUpload)
    print listUpload[:10]
    result = confirmUpload(widget, len(listUpload), serverRequest, listUpload)
    
    if result:
        for fileTestName in listUpload:
            if fileTestName == 'Directory':
                continue
            root = treeWidgetBDD.invisibleRootItem()
            firstItem = root.child(0)
            with open(fileTestName, 'rb') as f:
                reader = f.readlines()
                for row in reader[1:]:
                    row = row.replace('\r', '')
                    if row.split(' ')[0] == 'TestId:':
                        testId = str(row.split(' ')[-1])[:-1]
                        item = QtGui.QTreeWidgetItem(firstItem, ['Test_'+str(testId)])
                        item.setData(0, QtCore.Qt.UserRole, str(testId))
                        item.setCheckState(0, QtCore.Qt.Unchecked)
        print 'all file uploaded'


def removeRedundant(listUpload, treeWidgetBDD):
    root = treeWidgetBDD.invisibleRootItem()
    listIdBDD = getAllUnCheckedItem(root)
    listIdUpload = []
    for fileTestName in listUpload:
        if fileTestName == 'Directory':
                continue
        with open(fileTestName, 'rb') as f:
            reader = f.readlines()
            for row in reader[1:]:
                row = row.replace('\r', '')
                if row.split(' ')[0] == 'TestId:':
                    testId = str(row.split(' ')[-1])[:-1]
                    if testId not in listIdBDD:
                        listIdUpload.append(fileTestName)
                    
    return listIdUpload


class Ui_widgetTest(object):
    def addParent(self, parent, column, title):
        item = QtGui.QTreeWidgetItem(parent, [title])
        item.setData(column, QtCore.Qt.UserRole, "Directory")
        item.setChildIndicatorPolicy(QtGui.QTreeWidgetItem.ShowIndicator)
        item.setExpanded (False)
        return item

    def addChild(self, parent, column, title, data):
        item = QtGui.QTreeWidgetItem(parent, [title])
        item.setData(column, QtCore.Qt.UserRole, data)
        item.setCheckState(column, QtCore.Qt.Unchecked)
        return item
        
    def addItems(self, parent):
        column = 0
        clients_item = self.addParent(parent, column, 'Clients')
        vendors_item = self.addParent(parent, column, 'Vendors')
        clients_item2 = self.addParent(clients_item, column, 'Clients2')
        time_period_item = self.addParent(parent, column, 'Time Period')
        
        self.addChild(clients_item, column, 'Type A', 'data Type A')
        self.addChild(clients_item, column, 'Type B', 'data Type B')
        self.addChild(clients_item2, column, 'Type Z', 'data Type Z')
        self.addChild(clients_item2, column, 'Type E', 'data Type E')

        self.addChild(vendors_item, column, 'Mary', 'data Mary')
        self.addChild(vendors_item, column, 'Arnold', 'data Arnold')

        self.addChild(time_period_item, column, 'Init', 'data Init')
        self.addChild(time_period_item, column, 'End', 'data End')

    def addTestFile(self, parent, rootName):
        for root, subdirs, files in os.walk(rootName):
            if root == rootName:
                dirName = root.split('/')[-1]
                dirItem = self.addParent(parent, 0, dirName)
            if root == rootName:
                for subdir in subdirs:
                    self.addTestFile(dirItem, rootName+'/'+subdir)
                
            if root == rootName:
                for fileName in files:
                    self.addChild(dirItem, 0, fileName, rootName+'/'+fileName)
                
    def addTestFiles(self, parent):
        self.addTestFile(parent, DIRTESTRECORD)
    
    def addTestFilesBDD(self, parent):
        itemBDD = self.addParent(parent, 0, u'Test de la base de donnée')  
        if self.online:
            listId = self.serverRequest.getAllTestId()
        else:
            return
        for testId in listId:
            self.addChild(itemBDD, 0, 'Test_'+str(testId), testId)
            
    def setupUi(self, widgetTest, serverRequest):
        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False

        widgetTest.setObjectName(_fromUtf8("widgetTest"))
        self.horizontalLayout = QtGui.QHBoxLayout(widgetTest)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.widget = QtGui.QWidget(widgetTest)
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout = QtGui.QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(-1, -1, -1, 9)
        self.gridLayout.setHorizontalSpacing(10)
        self.gridLayout.setVerticalSpacing(15)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))

        self.groupBoxTreeBDD = QtGui.QGroupBox(self.widget)
        self.groupBoxTreeBDD.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBoxTreeBDD.setObjectName(_fromUtf8("groupBoxTreeBDD"))
        self.gridLayout_BDD = QtGui.QGridLayout(self.groupBoxTreeBDD)
        self.gridLayout_BDD.setObjectName(_fromUtf8("gridLayout_BDD"))
        self.frame_2 = QtGui.QFrame(self.groupBoxTreeBDD)
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.frame_2)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.buttonBDDDelete = QtGui.QPushButton(self.frame_2)
        self.buttonBDDDelete.setObjectName(_fromUtf8("buttonBDDDelete"))
        self.horizontalLayout_4.addWidget(self.buttonBDDDelete)
        self.buttonBDDDownload = QtGui.QPushButton(self.frame_2)
        self.buttonBDDDownload.setObjectName(_fromUtf8("buttonBDDDownload"))
        self.buttonBDDDownload.setEnabled(False)
        self.buttonBDDDelete.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.buttonBDDDownload)
        self.gridLayout_BDD.addWidget(self.frame_2, 1, 0, 1, 1)
        self.treeWidgetBDD = QtGui.QTreeWidget(self.groupBoxTreeBDD)
        self.treeWidgetBDD.setEditTriggers(QtGui.QAbstractItemView.DoubleClicked |
                                           QtGui.QAbstractItemView.EditKeyPressed|QtGui.QAbstractItemView.SelectedClicked)
        self.treeWidgetBDD.setDragDropOverwriteMode(False)
        self.treeWidgetBDD.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
        self.treeWidgetBDD.setObjectName(_fromUtf8("treeWidgetBDD"))
        self.addTestFilesBDD(self.treeWidgetBDD.invisibleRootItem())

        self.gridLayout_BDD.addWidget(self.treeWidgetBDD, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBoxTreeBDD, 1, 1, 1, 1)

        self.groupBoxTreeLocal = QtGui.QGroupBox(self.widget)
        self.groupBoxTreeLocal.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBoxTreeLocal.setObjectName(_fromUtf8("groupBoxTreeLocal"))
        self.gridLayout_2 = QtGui.QGridLayout(self.groupBoxTreeLocal)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.frame = QtGui.QFrame(self.groupBoxTreeLocal)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.frame)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.buttonLocalDelete = QtGui.QPushButton(self.frame)
        self.buttonLocalDelete.setObjectName(_fromUtf8("buttonLocalDelete"))
        self.horizontalLayout_2.addWidget(self.buttonLocalDelete)
        self.buttonLocalUpload = QtGui.QPushButton(self.frame)
        self.buttonLocalUpload.setObjectName(_fromUtf8("buttonLocalUpload"))
        self.horizontalLayout_2.addWidget(self.buttonLocalUpload)
        self.gridLayout_2.addWidget(self.frame, 1, 0, 1, 1)
        self.treeWidgetLocal = QtGui.QTreeWidget(self.groupBoxTreeLocal)
        self.treeWidgetLocal.setEditTriggers(QtGui.QAbstractItemView.DoubleClicked|QtGui.QAbstractItemView.EditKeyPressed|QtGui.QAbstractItemView.SelectedClicked)
        self.treeWidgetLocal.setDragDropOverwriteMode(False)
        self.treeWidgetLocal.setDragDropMode(QtGui.QAbstractItemView.DropOnly)
        self.treeWidgetLocal.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.treeWidgetLocal.setObjectName(_fromUtf8("treeWidgetLocal"))
        self.addTestFiles(self.treeWidgetLocal.invisibleRootItem())
       
        self.gridLayout_2.addWidget(self.treeWidgetLocal, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBoxTreeLocal, 2, 1, 1, 1)

        self.groupBoxDescriptionBDD = QtGui.QGroupBox(self.widget)
        self.groupBoxDescriptionBDD.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBoxDescriptionBDD.setObjectName(_fromUtf8("groupBoxDescriptionBDD"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout(self.groupBoxDescriptionBDD)
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.textBrowserBDD = QtGui.QTextBrowser(self.groupBoxDescriptionBDD)
        self.textBrowserBDD.setObjectName(_fromUtf8("textBrowserBDD"))
        self.horizontalLayout_5.addWidget(self.textBrowserBDD)
        self.gridLayout.addWidget(self.groupBoxDescriptionBDD, 1, 2, 1, 1)

        self.groupBoxDescriptionLocal = QtGui.QGroupBox(self.widget)
        self.groupBoxDescriptionLocal.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBoxDescriptionLocal.setObjectName(_fromUtf8("groupBoxDescriptionLocal"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.groupBoxDescriptionLocal)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.textBrowserLocal = QtGui.QTextBrowser(self.groupBoxDescriptionLocal)
        self.textBrowserLocal.setObjectName(_fromUtf8("textBrowserLocal"))
        self.horizontalLayout_6.addWidget(self.textBrowserLocal)
        self.gridLayout.addWidget(self.groupBoxDescriptionLocal, 2, 2, 1, 1)

        self.horizontalLayout.addWidget(self.widget)
        self.retranslateUi(widgetTest)
        QtCore.QMetaObject.connectSlotsByName(widgetTest)
        QtCore.QObject.connect(self.treeWidgetLocal, QtCore.SIGNAL(_fromUtf8("itemSelectionChanged()")),
                               self.updateDescriptionLocal)
        QtCore.QObject.connect(self.treeWidgetBDD, QtCore.SIGNAL(_fromUtf8("itemSelectionChanged()")),
                               self.updateDescriptionBDD)
        self.buttonLocalDelete.clicked[bool].connect(lambda:deleteSelectedFilesLocal(self.widget, self.treeWidgetLocal))
        self.buttonBDDDelete.clicked[bool].connect(lambda:deleteSelectedFilesLocal(self.widget, self.treeWidgetBDD))
        self.buttonLocalUpload.clicked[bool].connect(lambda:uploadSelectedFilesLocal(self.widget, self.treeWidgetLocal,
                                                                                     self.treeWidgetBDD,
                                                                                     self.serverRequest))

    def updateDescriptionBDD(self):
        try:
            dataItem = self.treeWidgetBDD.selectedItems()[0].data(0, QtCore.Qt.UserRole).toPyObject()
            if dataItem != 'Directory':
                self.textBrowserBDD.clear()
                if self.online:
                    string = self.serverRequest.getTestDescription(dataItem)
                else:
                    return
                self.textBrowserBDD.setText(string)  
        except IndexError:
            pass

    def updateDescriptionLocal(self):
        try:
            dataItem = self.treeWidgetLocal.selectedItems()[0].data(0, QtCore.Qt.UserRole).toPyObject()
            if dataItem != 'Directory':
                self.textBrowserLocal.clear()
                with open(dataItem)as f:
                    string = ''.join(f.readlines())
                self.textBrowserLocal.setText(unicode(string, 'utf-8'))
        except IndexError:
            pass

    def retranslateUi(self, widgetTest):
        widgetTest.setWindowTitle(_translate("widgetTest", "Form", None))
        self.groupBoxTreeBDD.setTitle(_translate("widgetTest", u"Test de la base donnée", None))
        self.treeWidgetBDD.headerItem().setText(0, _translate("widgetTest", "Fichier test", None))
        __sortingEnabled = self.treeWidgetBDD.isSortingEnabled()
        self.treeWidgetBDD.setSortingEnabled(False)

        self.treeWidgetBDD.setSortingEnabled(__sortingEnabled)
        self.buttonBDDDelete.setText(_translate("widgetTest", "Supprimer", None))
        self.buttonBDDDownload.setText(_translate("widgetTest", u"Télécharger", None))
        self.groupBoxTreeBDD.setTitle(_translate("widgetTest", u"Test de la base de donnée", None))
        self.groupBoxTreeLocal.setTitle(_translate("widgetTest", u"Test local en attente de téléversement", None))
        self.buttonLocalDelete.setText(_translate("widgetTest", "Supprimer", None))
        self.buttonLocalUpload.setText(_translate("widgetTest", u"Téléverser", None))
        self.treeWidgetLocal.headerItem().setText(0, _translate("widgetTest", "Ficher test", None))
        __sortingEnabled = self.treeWidgetLocal.isSortingEnabled()
        self.treeWidgetLocal.setSortingEnabled(False)

        self.treeWidgetLocal.setSortingEnabled(__sortingEnabled)
        self.groupBoxDescriptionBDD.setTitle(_translate("widgetTest", u"Description du fichier test de la base de donnée", None))
        self.groupBoxDescriptionLocal.setTitle(_translate("widgetTest", "Description du fichier local", None))
                
        self.textBrowserBDD.setHtml(_translate("widgetTest", "", None))
        self.textBrowserLocal.setHtml(_translate("widgetTest", "", None))

   
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    widgetTest = QtGui.QWidget()
    ui = Ui_widgetTest()
    ui.setupUi(widgetTest)
    widgetTest.show()
    sys.exit(app.exec_())


def getWidgetTest(serverRequest=None):
    widgetTest = QtGui.QWidget()
    ui = Ui_widgetTest()
    ui.setupUi(widgetTest, serverRequest)
    return widgetTest
    

