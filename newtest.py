#!/usr/bin/python
import os
import stat
import subprocess
import serial
import serial.tools.list_ports
import time
    
    
class Capteur:
    
    def seekPort(self):
        ports = list(serial.tools.list_ports.comports())
        po = []
        for p in ports:
            if p[2] != 'n/a':
                po.append(p[0])
        
        if len(po) != 0:
            print po
            return po
        return po
        print 'Aucun Koondal connecte veuillez rebranchez et relancer l\'application\n'
        raise Exception('NOPORT')
        
    def __init__(self):
        try:
            po = self.seekPort()
        except Exception, e:
            print e
            exit()
        for port in po:
            while True:
                try:
                    print port
                    self.capteur = serial.Serial(port, 38400, timeout=0.1)
                    break
                except Exception, e:
                    print e
                    raise Exception('ERRORWINDOWS')

    def reset(self):
        try:
            port = self.seekPort()
        except Exception:
            raise
        while True:
            try:
                self.capteur = serial.Serial(port, 38400, timeout=0.1)
                self.capteur.flush()
                break
            except serial.SerialException:
                raise Exception('ERRORWINDOWS')
                time.sleep(3)
                
    def writeBuffer(self):
        Capteur = self.capteur
        try:
                Capteur.write('w')
        except Exception, e:

            print e
            raise Exception('TIMEOUT')
            return
            
    def readBuffer(self):
        Capteur = self.capteur
        try:
            Capteur.write('r')
        except Exception:
            raise Exception('write TIMEOUT')
            return
        print 'ok'
        time.sleep(0.06)
        lines = []
        
        while True:
            try:
                lineData = Capteur.readline()
            except:
                raise Exception('INACTIF')
            if lineData == '':
                break
            else:
                line = lineData.split()
                if len(line) == 11:
                    line = line[1:]
                    timestamp = time.time()
                    line.append(str(timestamp))
                    lines.append(line)
            
        return lines

    def askForOneLine(self):
        Capteur = self.capteur
        inactif = 0
        while True:
            if inactif >= 30:
                raise Exception('INACTIF')
                return
            try:
                Capteur.write('l')
            except Exception:
                raise Exception('TIMEOUT')
                return
            time.sleep(0.06)
            lineData = Capteur.readline()
            line = []
            line = lineData.split()
            if len(line) == 0:
                inactif += 1 if inactif >= 0 else inactif
                continue
            if 'l' in line[-1]:
                inactif -= 1 if inactif > 0 else inactif
                continue
            if len(line) == 11:
                inactif -= 1 if inactif > 0 else inactif
                break
            print lineData
            if lineData == '?' and inactif >= 10:
                inactif -= 1 if inactif > 0 else inactif
                raise Exception('BADPORT')
                return
            print 'ligne de capteur mal formate'
        try:
            timestamp = time.time()
            line = line[1:]
            line.append(str(timestamp))
        except:
            print 'error Capteur.py : askForOneLine'
            Capteur.close()
        return line
     
    def close(self):
        self.Capteur.close()


def main():
    c = Capteur()

main()