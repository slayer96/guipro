# -*- coding: utf-8 -*-

import os
import pickle
import csv
import pandas
from cloud.serialization.cloudpickle import dump
from cloud.serialization.cloudpickle import dump as Cdump
import time
import shutil

DIRPATH = os.path.dirname(os.path.realpath(__file__))


def getScore(label): 
    if not os.path.exists(DIRPATH+'/Log/Score/'+label+'.csv'):
        return ('0', '0', '0')
    else:
        dataCsv = pandas.read_csv(DIRPATH+'/Log/Score/'+label+'.csv')
        if len(list(dataCsv['NombreDeTest'])) < 1:
            return ('0','0','0')
        if float(list(dataCsv['NombreDeTest'])[-1]) >= 1:
            return (list(dataCsv['ScoreIn'])[-1], list(dataCsv['ScoreOut'])[-1], list(dataCsv['NombreDeTest'])[-1])
        else:
            return ('0', '0', '0')


def createFileScore(label):
    filepath = DIRPATH + '/Log/Score/' + label + '.csv'
    if not os.path.exists(filepath):
        f = open(filepath, 'w')
        f.write('ScoreIn,ScoreOut,dicoIndicateurPickle,NombreDeTest,regressionPickle,pcaPickle\n')
        f.close()
        return True
    else:
        return False  


def copyanything(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copyanything(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)


def parseOutput(checkScore =True):
    path = DIRPATH+'/Log/Output/output.csv'
    with open(path, mode='rb') as infile:
        reader = csv.reader(infile, delimiter=';')
        reader.next()
        mydict = {rows[0]: rows[1] for rows in reader}
    
    if checkScore:
        for output in mydict.keys():
            # si aucun test fait encore
            if getScore(output)[2] == '0':
                del(mydict[output])
    return mydict


def getFunctionByLevel(listIndicToAdd):
    listIndicateurByLevel = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz'] + \
                            getDicoIndicateur(path=DIRPATH+'/Dictionnaire/listeIndicateurName.p')[10:]
    listIndicateurAddSortedByLevel = []

    for name in listIndicateurByLevel:
        if name in listIndicToAdd:
            listIndicateurAddSortedByLevel.append(name)
    return listIndicateurAddSortedByLevel


def dumpDicoRegression(dico, path=DIRPATH+"/Log/Pickle/donnee.p"):
    f = open( path, "wb")
    pickle.dump(dico, f)
    f.close()


def getDicoRegression(path=DIRPATH+"/Log/Pickle/donnee.p"):
    f = open( path, "rb")
    dicoPickle = pickle.load(f)
    f.close()
    return dicoPickle


#ecrit le pca dans un fichier pickle
def writePCA(pca, path=DIRPATH+"/Log/Pickle/pca.p"):
    f = open(path, "wb")
    pickle.dump(pca, f)
    f.close()   


# retourne le pca
def getPCA(path=DIRPATH+"/Log/Pickle/pca.p" ):
    f = open(path, "rb" )
    dicoPickle = pickle.load(f)
    f.close()
    return dicoPickle

    
def getDicoIndicateur(path=DIRPATH+"/Log/Pickle/indicateur.p"):
    f = open(path, "rb")
    dicoPickle = pickle.load(f)
    f.close()
    return dicoPickle


def dumpDicoIndicateur(dico, path=DIRPATH+"/Log/Pickle/indicateur.p"):
    f = open(path, "wb")
    dump(dico, f)
    f.close()


def dumpListMember(listMember, path=DIRPATH+"/Log/Pickle/member.p"):
    f = open(path, "wb")
    dump(listMember, f)
    f.close()


def getListMember(path=DIRPATH+"/Log/Pickle/member.p"):
    f = open( path, "rb")
    listMember = pickle.load(f)
    f.close()
    return listMember


def saveRegression(label, filepath, regression):
    directory = DIRPATH + '/Log/Regression/' + label
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(filepath, "wb")
    pickle.dump(regression, f)
    f.close() 


def saveDicoIndicator(label, filepath):
    directory = DIRPATH + '/Log/Indicateur/' + label
    dicoIndicateur = getDicoIndicateur()
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(filepath, "wb")
    Cdump(dicoIndicateur, f)
    f.close()


def savePca(label, filepath, pca):
    directory = DIRPATH + '/Log/Pca/' + label
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(filepath, "wb")
    pickle.dump(pca, f)
    f.close()


def addScore(outputName, scoreIn, scoreOut, NbTest, regression,pca):
    indicateurFilePath = DIRPATH + '/Log/Indicateur/' + outputName + '/' + outputName + str(time.time())+ '.p'
    regressionFilePath = DIRPATH + '/Log/Regression/' + outputName + '/' + outputName+str(time.time()) + '.p'
    pcaFilePath = DIRPATH + '/Log/Pca/' + outputName + '/' + outputName + str(time.time()) + '.p'

    line = str(scoreIn) + ',' + str(scoreOut) + ',' + indicateurFilePath + ',' + str(NbTest) + ',' + \
           regressionFilePath + ',' + pcaFilePath + '\n'
    with open(DIRPATH+'/Log/Score/'+outputName+'.csv', 'ab') as f:
        f.write(line)


def saveScore(label, scoreIn, scoreOut, NbTest, regression, pca):
    createFileScore(label)
    addScore(label, scoreIn, scoreOut, NbTest, regression, pca)


def saveFunctionConfiguration(user, dico, dicoIndicateur, outputName='Assis'):
    if dico['fonction'] == None:
        return
    saveScore(outputName, dico['scoreIn'], dico['scoreOut'], dico['nbTest'], dico['fonction'], dico['pca'])
    dumpDicoRegression(dico['fonction'], path=DIRPATH + '/Compte/' + user + '/' + outputName + '/donnee.p')
    dumpDicoIndicateur(dicoIndicateur, path=DIRPATH + '/Compte/' + user + '/' + outputName + '/indicateur.p')
    writePCA(dico['pca'], path=DIRPATH + '/Compte/' + user + '/' + outputName + '/pca.p')