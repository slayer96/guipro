# -*- coding: utf-8 -*-
import Capteur
import time

from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QThread
from functionality import cmdToSend


class WorkerUser(QThread):
    def __init__(self, capteur, parent=None):

        QThread.__init__(self, parent)
        self.exiting = False
        self.capteur = capteur

    def __del__(self):

        self.exiting = True
        if self.capteur != 'capteur':
            self.capteur.disconnect()
            self.terminate()

    def run(self):
        if not self.exiting:
            try:
                self.capteur = Capteur.Capteur()
            except Exception, e:
                if str(e) == 'NOPORT':
                    self.emit(SIGNAL("output(QString)"), str(e))
                else:
                    self.emit(SIGNAL("output(QString)"), 'ERRORWINDOWS')
                return
            self.exiting = True
        lastLine = ''
        cpt = 0
        t = time.time()
        while True:
            cpt += 1
            try:
                if cmdToSend == "L":
                    self.capteur.stopLive()
                    time.sleep(0.01)
                    continue
                line = self.capteur.askForOneLine()
                time.sleep(0.01)
                if line is None:
                    continue

                if len(line) != 11:
                    continue

            except Exception, e:
                print e
                self.emit(SIGNAL("output(QString)"), str(e))
                if str(e) == 'TIMEOUT' or str(e) == 'INACTIF':
                    return
            map(str, line)
            string = ':'.join(line)
            if lastLine != string:
                print time.time() - t
                t = time.time()
                self.emit(SIGNAL("output(QString)"), string)
                lastLine = string
