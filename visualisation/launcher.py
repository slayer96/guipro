# -*- coding: utf-8 -*-
import sys
import styleSheet

from PyQt4 import QtGui
from menu import Menu


def main():
    app = QtGui.QApplication(sys.argv)
    menu = Menu()
    menu.setStyleSheet(styleSheet.getStyle())
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()