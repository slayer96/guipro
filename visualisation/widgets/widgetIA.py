# -*- coding: utf-8 -*-
import pandas
import shutil
import time
import OfflineRequest
import styleSheet

from Constant import *
from PyQt4 import QtGui, QtCore
from LogParser import parseOutput, createFileScore, getDicoIndicateur, dumpDicoIndicateur, \
    getListMember, saveFunctionConfiguration, getFunctionByLevel
from choix_widget import WidgetChoixIndicateur, WidgetChoixMember
from visualisation.functionality import DIRPATH, createCheckbox, teamKoondal, orderPrincipalOutput, createComboBox


class WidgetIA(QtGui.QWidget):
    def __init__(self, parent=None, qData='jojo', widUser='user', widPlot='plot', serverRequest=None):
        super(WidgetIA, self).__init__(parent)
        self.data = qData
        self.widgetIndicateur = WidgetChoixIndicateur()
        self.widgetIndicateur.hideIT()
        self.widgetMember = WidgetChoixMember()
        self.widgetMember.hideIT()
        self.widU = widUser
        self.widP = widPlot

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.initUI()

    def checkAll(self):
        if self.check:
            for output, checkbox in self.checkOutput.items():
                self.checkOutput[output].setChecked(False)
        else:
            for output, checkbox in self.checkOutput.items():
                self.checkOutput[output].setChecked(True)
        self.check = not self.check

    def initUI(self):
        gridIntern = QtGui.QGridLayout()
        gridIntern.setSpacing(1)

        if self.online:
            listOutputScore = self.serverRequest.getOutput(exist='False')
        else:
            listOutputScore = parseOutput(checkScore=False).keys()
        listCheckLabel = sorted([outputi for outputi in listOutputScore + ['USER'] if outputi not in teamKoondal])

        self.checkOutput = {output: createCheckbox(self, output, None, check=0) for output in listCheckLabel}
        self.scoreOutput = {output: self.getScore(output) for output in listOutputScore}
        self.check = False
        buttonCheck = QtGui.QPushButton('Check/Uncheck OUTPUT', self)
        buttonCheck.clicked[bool].connect(self.checkAll)

        self.outputLineEdit = QtGui.QLineEdit()
        button = QtGui.QPushButton('ajouter', self)
        button.clicked[bool].connect(self.addOutput)
        buttonR = QtGui.QPushButton('supprimer', self)
        buttonR.clicked[bool].connect(self.removeOutput)
        self.buttonRegressionPartiel = QtGui.QPushButton('Lancer Regression', self)
        self.buttonRegressionPartiel.clicked[bool].connect(self.regressionPartiel)

        lenPcaMax = len(getDicoIndicateur())
        self.comboboxPca = createComboBox(['Pas de PCA'] + [str(i + 1) for i in range(lenPcaMax + 10)])
        self.comboboxRegression = createComboBox(
            ['Regression Logistique', 'Regression LogistiqueCV', 'Regression Ridge'])
        self.labelIA = QtGui.QLabel('En attente de regression ... ')
        self.labelAdviceIA = QtGui.QLabel('')
        if self.online:
            onlineMember = ['Guest'] + self.serverRequest.getTeamKoondal()
        else:
            onlineMember = ['Guest', 'Mourougan', 'Tarik', 'Paul']
        self.comboboxUser = createComboBox(onlineMember)

        buttonIndicateur = QtGui.QPushButton('Choix des indicateurs', self)
        buttonIndicateur.clicked[bool].connect(self.chooseIndicateur)
        buttonMember = QtGui.QPushButton('  Sources des tests  ', self)
        buttonMember.clicked[bool].connect(self.chooseMember)

        hAjout = QtGui.QHBoxLayout()
        hPca = QtGui.QHBoxLayout()

        if self.online:
            listOutput = self.serverRequest.getOutput(exist='False')
        else:
            listOutput = parseOutput(checkScore=False)
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        listCheckLabel = sorted([outputi for outputi in listOutputScore + ['USER'] if outputi not in teamKoondal])
        self.labelScoreIn = {output: QtGui.QLabel(str("%.2f %% " % (float(self.scoreOutput[output][0]) * 100))) for
                             output in listOutputScore}
        self.labelScoreOut = {output: QtGui.QLabel(str("%.2f %% " % (float(self.scoreOutput[output][1]) * 100))) for
                              output in listOutputScore}
        self.labelNbTest = {output: QtGui.QLabel(str(self.scoreOutput[output][2])) for output in listOutputScore}
        self.hLine = {output: QtGui.QHBoxLayout() for output in listCheckLabel if output not in teamKoondal}

        for output in listCheckLabel:
            if output == 'USER':
                nbTest = 512
                self.hLine[output].addWidget(self.checkOutput[output])
                self.hLine[output].addWidget(self.labelScoreIn['Mourougan'])
                self.hLine[output].addWidget(self.labelScoreOut['Mourougan'])
                self.hLine[output].addWidget(QtGui.QLabel(str(nbTest)))
            else:
                self.hLine[output].addWidget(self.checkOutput[output])
                self.hLine[output].addWidget(self.labelScoreIn[output])
                self.hLine[output].addWidget(self.labelScoreOut[output])
                self.hLine[output].addWidget(self.labelNbTest[output])

        v = QtGui.QVBoxLayout()
        for output in listCheckLabel:
            v.addLayout(self.hLine[output])

        h0 = QtGui.QHBoxLayout()
        h0.addWidget(buttonCheck)
        h0.addWidget(QtGui.QLabel('SCORE IN'))
        h0.addWidget(QtGui.QLabel('SCORE OUT'))
        h0.addWidget(QtGui.QLabel('NOMBRE DE TEST'))

        hAjout.addWidget(self.outputLineEdit)
        hAjout.addWidget(button)
        hAjout.setSpacing(0)
        hPca.addWidget(QtGui.QLabel('PCA:'))
        hPca.addWidget(self.comboboxPca)
        hPca.setSpacing(0)
        h0.setSpacing(0)

        gridIntern.addLayout(h0, 0, 0, 1, 4)
        gridIntern.addLayout(v, 1, 0, 4, 4)

        gridIntern.addWidget(buttonR, 5, 0, 1, 1)
        gridIntern.addLayout(hAjout, 5, 1, 1, 1)
        gridIntern.addLayout(hPca, 6, 0, 1, 1)
        gridIntern.addWidget(buttonIndicateur, 6, 1, 1, 1)
        gridIntern.addWidget(buttonMember, 6, 2, 1, 1)
        gridIntern.addWidget(self.comboboxRegression, 6, 3, 1, 1)

        h = QtGui.QHBoxLayout()
        h.addWidget(QtGui.QLabel('Utilisateur actuel: '))
        h.addWidget(self.comboboxUser)
        gridIntern.addLayout(h, 6, 4, 1, 1)
        gridIntern.addWidget(self.buttonRegressionPartiel, 6, 5, 1, 1)
        h = QtGui.QVBoxLayout()
        h.addWidget(self.labelIA)
        h.addWidget(self.labelAdviceIA)
        gridIntern.addLayout(h, 7, 3, 1, 2)

        self.setLayout(gridIntern)
        return gridIntern

    def chooseIndicateur(self):
        self.widgetIndicateur.showIT()

    def chooseMember(self):
        self.widgetMember.showIT()

    def addOutput(self):
        filepath = DIRPATH + '/Log/Output/output.csv'
        dataCsv = pandas.read_csv(filepath, sep=';')
        outputName = str(unicode(self.outputLineEdit.text()).encode("utf-8")).title()
        try:
            self.serverRequest.ServerAddOutput(outputName)
            print 'ajouter'
        except:
            pass
        if outputName in list(dataCsv['OutputName']):
            print 'wtf'
            return
        with open(filepath, 'ab') as f:
            if str(self.outputLineEdit.text()) != '':
                f.write(outputName + ';[-1 1]\n')
                createFileScore(outputName)
                directory = DIRPATH + '/Log/Pickle/' + outputName
                if not os.path.exists(directory):
                    os.makedirs(directory)
                directory = DIRPATH + 'Log/Pickle/BEST/' + outputName
                if not os.path.exists(directory):
                    os.makedirs(directory)

                for member in teamKoondal + ['Guest']:
                    directory = DIRPATH + '/Compte/' + member + '/' + outputName
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                    directory = DIRPATH + '/Compte/' + member + '/BEST/' + outputName
                    if not os.path.exists(directory):
                        os.makedirs(directory)

        currentUser = self.comboboxUser.currentText()
        with open(DIRPATH + '/Log/Output/update.txt', 'wb')as fU:
            fU.write('output: ' + outputName + '\n')
            fU.write('user: ' + currentUser + '\n')
        self.refreshUi()

    def regressionPartiel(self):
        self.buttonRegressionPartiel.setEnabled(False)
        self.labelIA.repaint()
        self.labelIA.setText(' ')
        self.labelAdviceIA.repaint()
        self.labelAdviceIA.setText('')
        if not self.online:
            self.labelIA.setText(u'La connexion au serveur à échoué')
            self.buttonRegressionPartiel.setEnabled(True)

        listCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        listCurrentIndicateur = getFunctionByLevel(listCurrentIndicateur)

        currentUser = self.comboboxUser.currentText()
        outputs = [output for output in self.checkOutput.keys() if
                   self.checkOutput[output].isChecked() and output != 'USER']
        if self.checkOutput['USER'].isChecked():
            outputs += teamKoondal

        listUser = getListMember()
        if listUser == []:
            self.labelIA.repaint()
            self.labelIA.setText(u'Aucune source d\'utilisateur selectionné')
            self.buttonRegressionPartiel.setEnabled(True)
            return
        if outputs == []:
            self.labelIA.repaint()
            self.labelIA.setText(u'Aucune fonction output selectionné')
            self.buttonRegressionPartiel.setEnabled(True)
            return
        pca = int(str(self.comboboxPca.currentText())) if self.comboboxPca.currentIndex() != 0 else 0
        t0 = time.time()
        for output in outputs:
            if output in teamKoondal:
                continue
            self.labelIA.repaint()
            self.labelIA.setText('Mise a jour de ' + str(output) + ' en cours ...')
            QtCore.QCoreApplication.processEvents()
            typeRegression = str(self.comboboxRegression.currentText()).split()[-1]

            if self.online:
                progress = QtGui.QProgressDialog(u"Regression en cours ...", "Cancel", 0, 12)
                progress.setCancelButton(None)
                progress.setStyleSheet(styleSheet.getStyle())
                progress.show()
                QtCore.QCoreApplication.processEvents()
                dico = self.serverRequest.requestRegression(output, listCurrentIndicateur, listUser, pca,
                                                            typeRegression, progress=progress)
            else:
                OfflineRequest.updateLocalData()
                progress = QtGui.QProgressDialog(u"Regression en cours ...", "Cancel", 0, 12)
                progress.setCancelButton(None)
                progress.setStyleSheet(styleSheet.getStyle())
                progress.show()
                QtCore.QCoreApplication.processEvents()
                dico = OfflineRequest.requestRegression(output, listCurrentIndicateur, listUser, pca,
                                                        typeRegression, progress=progress)

            if dico['error'] == None:
                saveFunctionConfiguration(currentUser, dico, listCurrentIndicateur, outputName=output)
            else:
                self.labelIA.repaint()
                self.labelIA.setText(dico['error'])
                self.buttonRegressionPartiel.setEnabled(True)
                if output == outputs[-1]:
                    return
                else:
                    continue
            if dico['advice']:
                self.labelAdviceIA.repaint()
                self.labelAdviceIA.setText(dico['advice'])

            QtCore.QCoreApplication.processEvents()
            self.labelIA.repaint()
            self.labelIA.setText('Mise a jour de ' + str(output) + ' OK')

        self.labelIA.repaint()
        timeRegression = str("%.1f" % (time.time() - t0))
        self.labelIA.setText('Temps regression(' + timeRegression + ')sec')

        self.updateScore()
        self.data.initialize(user=currentUser)
        progress.setValue(progress.value() + 2)
        QtCore.QCoreApplication.processEvents()
        dumpDicoIndicateur(listCurrentIndicateur, path=DIRPATH + '/Log/Pickle/lastIndicateur.p')
        QtCore.QCoreApplication.processEvents()
        self.buttonRegressionPartiel.setEnabled(True)

        self.widU.stop()
        QtCore.QCoreApplication.processEvents()
        QtGui.QWidget().setLayout(self.widU.layout())
        self.widU.setLayout(self.widU.initUI(user=currentUser))
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()

    def refreshUi(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI())
        with open(DIRPATH + '/Log/Output/update.txt', 'rb')as f:
            str(f.readline()).split()[-1]
            user = str(f.readline()).split()[-1]
        QtGui.QWidget().setLayout(self.widU.layout())
        self.widU.setLayout(self.widU.initUI(user=user))
        self.widU.stop()
        self.widP.disconnecting()
        self.widP.thread.terminate()
        QtGui.QWidget().setLayout(self.widP.layout())
        self.widP.setLayout(self.widP.initUI())
        self.data.initialize(user=user)

    def getScore(self, label):
        if not os.path.exists(DIRPATH + '/Log/Score/' + label + '.csv'):
            return ('0', '0', '0')
        else:
            dataCsv = pandas.read_csv(DIRPATH + '/Log/Score/' + label + '.csv')
            if len(list(dataCsv['ScoreIn'])) >= 1:
                return (list(dataCsv['ScoreIn'])[-1], list(dataCsv['ScoreOut'])[-1], list(dataCsv['NombreDeTest'])[-1])
            else:
                return ('0', '0', '0')

    def updateScore(self):
        if self.online:
            listOutputScore = self.serverRequest.getOutput(exist='True')
        else:
            listOutputScore = parseOutput(checkScore='True')
        for output in listOutputScore:
            self.scoreOutput[output] = self.getScore(output)

        for output in self.labelScoreIn.keys():
            self.labelScoreIn[output].repaint()
            self.labelScoreOut[output].repaint()
            self.labelNbTest[output].repaint()
            self.labelScoreIn[output].setText(str("%.2f %%" % (float(self.scoreOutput[output][0]) * 100)))
            self.labelScoreOut[output].setText(str("%.2f %%" % (float(self.scoreOutput[output][1]) * 100)))
            self.labelNbTest[output].setText(str(self.scoreOutput[output][2]))

    def removeOutput(self):
        result = QtGui.QMessageBox.question(self,
                                            "Confirm Suppression...",
                                            "Are you sure you want to remove those output ?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        filepath = DIRPATH + '/Log/Output/output.csv'
        if result == QtGui.QMessageBox.Yes:
            listWithoutTeam = [output for output in self.listOutput if output not in teamKoondal]
            listOutputKeep = [output for output in listWithoutTeam if (not self.checkOutput[output].isChecked()) or
                              (output in orderPrincipalOutput + ['User'] + teamKoondal)]
            dataCsv = pandas.read_csv(filepath, sep=';')
            dataCsv = dataCsv[dataCsv['OutputName'].isin(listOutputKeep + teamKoondal)]
            dataCsv.to_csv(filepath, sep=';', index_label=False, index=False)
            for output in [output for output in self.checkOutput.keys() if output not in listOutputKeep]:
                if output == 'User':
                    continue
                try:
                    self.serverRequest.ServerRemoveOutput(output)
                except:
                    pass
                directory = DIRPATH + '/Log/Pickle/' + output
                if os.path.exists(directory):
                    shutil.rmtree(directory)
                    line = '0' + ',' + '0' + ',' + '0' + ',' + '0' + ',' + 'NONE' + ',' + 'NONE' + '\n'
                    with open(DIRPATH + '/Log/Score/' + output + '.csv', 'ab') as f:
                        f.write(line)
                for member in teamKoondal + ['Guest']:
                    directory = DIRPATH + '/Compte/' + member + '/' + output
                    if os.path.exists(directory):
                        shutil.rmtree(directory)

            self.refreshUi()
