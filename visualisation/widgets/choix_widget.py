import styleSheet

from PyQt4 import QtGui
from LogParser import getDicoIndicateur, dumpDicoIndicateur, dumpListMember
from visualisation.functionality import DIRPATH, createCheckbox, teamKoondal


class WidgetChoixIndicateur(QtGui.QWidget):
    def __init__(self, parent=None):
        super(WidgetChoixIndicateur, self).__init__(parent)
        self.initUI()
        self.setMinimumSize(600, 400)
        self.hideOnce = False

    def initUI(self, first=True):
        grid = QtGui.QGridLayout()
        grid.setSpacing(1)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        groupFonction.setFixedWidth(300)
        self.widgetFonction = QtGui.QWidget()
        self.verticalFonction = QtGui.QVBoxLayout()
        self.loadAllIndicateur()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)

        buttonAddAll = QtGui.QPushButton('Tout ajouter', self)
        buttonAddAll.clicked[bool].connect(self.addAll)

        buttonAdd = QtGui.QPushButton('Ajouter la selection', self)
        buttonAdd.clicked[bool].connect(self.add)

        buttonReset = QtGui.QPushButton('Reset', self)
        buttonReset.clicked[bool].connect(self.reset)

        buttonOK = QtGui.QPushButton('OK', self)
        buttonOK.clicked[bool].connect(self.ok)

        selectionFonction = QtGui.QScrollArea()
        selectionFonction.setWidgetResizable(True)
        selectionFonction.setFixedWidth(300)
        widgetSelection = QtGui.QWidget()
        self.verticalSelectionFonction = QtGui.QVBoxLayout()
        self.loadAllCurrentIndicateur(first)
        widgetSelection.setLayout(self.verticalSelectionFonction)
        selectionFonction.setWidget(widgetSelection)

        grid.setRowMinimumHeight(0, 10)
        grid.addWidget(groupFonction, 1, 0, 5, 4)
        grid.setColumnMinimumWidth(4, 10)
        grid.setColumnMinimumWidth(6, 10)
        grid.addWidget(buttonAdd, 2, 5, 1, 1)
        grid.addWidget(buttonAddAll, 3, 5, 1, 1)
        grid.addWidget(buttonReset, 4, 5, 1, 1)
        grid.addWidget(selectionFonction, 1, 7, 5, 4)
        grid.addWidget(buttonOK, 8, 8, 1, 1)

        self.setLayout(grid)
        self.setStyleSheet(styleSheet.getStyle())

        return grid

    def loadAllIndicateur(self):
        dicoIndicateur = getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p')
        dicoCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        self.dicoCheckBox = {}
        brut = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']
        for output in brut + sorted(dicoIndicateur.keys()):
            if output in dicoCurrentIndicateur:
                checkbox = createCheckbox(self, output, None, check=1)
            else:
                checkbox = createCheckbox(self, output, None, check=0)
            self.dicoCheckBox[output] = checkbox
            self.verticalFonction.addWidget(checkbox)

    def loadAllCurrentIndicateur(self, first):
        dicoCurrentIndicateur = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/indicateur.p')
        self.dicoLabelBox = {}
        for output in sorted(self.dicoCheckBox.keys()):
            if first:
                if output in dicoCurrentIndicateur:
                    label = QtGui.QLabel(output)
                    self.dicoLabelBox[output] = label
                    self.verticalSelectionFonction.addWidget(label)

    def ok(self):
        self.hideIT()
        brut = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']
        listIndicateurBrut = [mesure for mesure in brut if mesure in self.dicoLabelBox.keys()]
        listIndicateurNonBrut = [indicateur for indicateur in self.dicoLabelBox.keys() if indicateur not in brut]
        dumpDicoIndicateur(listIndicateurBrut + listIndicateurNonBrut, path=DIRPATH + "/Log/Pickle/indicateur.p")
        return self.dicoLabelBox

    def addAll(self):
        for output in self.dicoCheckBox.keys():
            if output not in self.dicoLabelBox.keys():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def add(self):
        for output, checkbox in self.dicoCheckBox.items():
            if output not in self.dicoLabelBox.keys() and checkbox.isChecked():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def reset(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI(first=False))
        self.dicoLabelBox = {}
        for output, checkbox in self.dicoCheckBox.items():
            checkbox.setChecked(False)

    def showIT(self):
        self.show()

    def hideIT(self):
        self.hideOnce = True
        self.hide()


class WidgetChoixMember(QtGui.QWidget):
    def __init__(self, parent=None):
        super(WidgetChoixMember, self).__init__(parent)
        self.initUI()
        self.setMinimumSize(600, 400)

    def initUI(self, first=True):
        grid = QtGui.QGridLayout()
        grid.setSpacing(1)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        self.widgetFonction = QtGui.QWidget()
        self.verticalFonction = QtGui.QVBoxLayout()
        self.loadAllMember()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)

        buttonAddAll = QtGui.QPushButton('Tout ajouter', self)
        buttonAddAll.clicked[bool].connect(self.addAll)

        buttonAdd = QtGui.QPushButton('Ajouter la selection', self)
        buttonAdd.clicked[bool].connect(self.add)

        buttonReset = QtGui.QPushButton('Reset', self)
        buttonReset.clicked[bool].connect(self.reset)

        buttonOK = QtGui.QPushButton('OK', self)
        buttonOK.clicked[bool].connect(self.ok)

        selectionFonction = QtGui.QScrollArea()
        selectionFonction.setWidgetResizable(True)
        widgetSelection = QtGui.QWidget()
        self.verticalSelectionFonction = QtGui.QVBoxLayout()
        self.loadAllCurrentMember(first)
        widgetSelection.setLayout(self.verticalSelectionFonction)
        selectionFonction.setWidget(widgetSelection)

        grid.setRowMinimumHeight(0, 10)
        grid.addWidget(groupFonction, 1, 0, 5, 4)
        grid.setColumnMinimumWidth(4, 10)
        grid.setColumnMinimumWidth(6, 10)
        grid.addWidget(buttonAdd, 2, 5, 1, 1)
        grid.addWidget(buttonAddAll, 3, 5, 1, 1)
        grid.addWidget(buttonReset, 4, 5, 1, 1)
        grid.addWidget(selectionFonction, 1, 7, 5, 4)
        grid.addWidget(buttonOK, 8, 8, 1, 1)

        self.setLayout(grid)
        self.setStyleSheet(styleSheet.getStyle())
        return grid

    def loadAllMember(self):
        self.dicoCheckBox = {}
        listCurrentMember = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/member.p')
        for member in sorted(teamKoondal + ['Guest']):
            if member in listCurrentMember:
                checkbox = createCheckbox(self, member, None, check=1)
            else:
                checkbox = createCheckbox(self, member, None, check=0)
            self.dicoCheckBox[member] = checkbox
            self.verticalFonction.addWidget(checkbox)

    def loadAllCurrentMember(self, first):
        listCurrentMember = getDicoIndicateur(path=DIRPATH + '/Log/Pickle/member.p')
        self.dicoLabelBox = {}
        for output in sorted(self.dicoCheckBox.keys()):
            if first:
                if output in listCurrentMember:
                    label = QtGui.QLabel(output)
                    self.dicoLabelBox[output] = label
                    self.verticalSelectionFonction.addWidget(label)

    def ok(self):
        self.hideIT()
        listMember = [member for member in self.dicoLabelBox.keys()]
        dumpListMember(listMember)

    def addAll(self):
        for output in self.dicoCheckBox.keys():
            if output not in self.dicoLabelBox.keys():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def add(self):
        for output, checkbox in self.dicoCheckBox.items():
            if output not in self.dicoLabelBox.keys() and checkbox.isChecked():
                label = QtGui.QLabel(output)
                self.dicoLabelBox[output] = label
                self.verticalSelectionFonction.addWidget(label)

    def reset(self):
        QtGui.QWidget().setLayout(self.layout())
        self.setLayout(self.initUI(first=False))
        self.dicoLabelBox = {}
        for output, checkbox in self.dicoCheckBox.items():
            checkbox.setChecked(False)

    def showIT(self):
        self.show()

    def hideIT(self):
        self.hide()

