# -*- coding: utf-8 -*-

import numpy as np
import random
import time

from datetime import datetime

from PyQt4 import QtGui
from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QApplication
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from math_plot_lib import MatplotlibWidget
from Constant import *
from LogParser import parseOutput, getDicoIndicateur
from visualisation.functionality import DIRPATH, createCheckbox, teamKoondal, orderPrincipalOutput, createComboBox, updateMinuteur,\
    writeAllLineInFile


class WidgetPlot(QtGui.QWidget):
    def __init__(self, parent=None, qThread=None, qData='fake', serverRequest=None):
        super(WidgetPlot, self).__init__()
        self.dicoAllIndicateur = getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p')
        self.t0 = time.time()
        self.cpt = 0
        self.firstPoint = True
        self.thread = qThread
        self.connected = False
        self.currentPointNumber = 0
        self.MaxPoint = 100
        self.recordBool = False
        self.llAx = [0] * self.MaxPoint
        self.llAy = [0] * self.MaxPoint
        self.llAz = [0] * self.MaxPoint
        self.dicoF = {}
        self.fonctionAdded = False
        self.data = qData

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.initUI()

    def connecting(self):
        self.connect(self.thread, SIGNAL("output(QString)"), self.updateGraphe)

    def disconnecting(self):
        self.disconnect(self.thread, SIGNAL("output(QString)"), self.updateGraphe)

    def raiseError(self, errorType, errorMessage):
        result = QtGui.QMessageBox.question(self, errorType + "...", errorMessage,
                                            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        if result == QtGui.QMessageBox.Ok:
            exit()

    '''
    Purpose : sauvegarde d'un fichier test pour description , ainsi que la ligne de test dans test.csv
    '''

    def saveFile(self, path=None):
        try:
            if path == None:
                filename, extension = QtGui.QFileDialog.getSaveFileNameAndFilter(self, 'Save File',
                                                                                 DIRPATH + '/TestRecord',
                                                                                 filter=QApplication.translate(
                                                                                     'etmEditor', 'txt File (*.txt)'))
            else:
                filename = path
        except:
            print 'error saveFile'
        else:
            try:
                f = open(filename, 'w')
                f.write(self.dataFileName + '\n')
                f.write('Durée: ')
                seconds = float(time.time()) - float(self.lastT0)
                m, s = divmod(seconds, 60)
                h, m = divmod(m, 60)
                f.write("%d:%02d:%02d\n" % (h, m, s))
                f.write('Description:\n')
                f.write(unicode(self.description.toPlainText()).encode("utf-8") + '\n')
                assis = '1' if str(self.dicoComboxOutput['Assis'].currentText()) == 'Oui' else '-1'
                repartitionMouvement = str(self.comboboxRepartitionMouvement.currentIndex() - 1)
                if str(self.dicoComboxOutput['Assis'].currentText()) != 'Pas test':
                    f.write('Assis:  ')
                    f.write(assis + '\n')
                if str(self.comboboxRepartitionMouvement.currentText()) != 'Pas test':
                    f.write('Repartitionmouvement_X-Y-Z:  ')
                    f.write(str(repartitionMouvement) + '\n')
                if str(self.comboboxFlexionExtension.currentText()) != 'Pas test':
                    f.write('Flexion-Extension:  ')
                    value = str(self.comboboxFlexionExtension.currentIndex() - 1)
                    value = '1' if value == '1' else '-1'
                    f.write(str(value) + '\n')
                if str(self.comboboxInclinaisonLateral.currentText()) != 'Pas test':
                    f.write('Inclinaisonlaterale_Gauche-Droite:  ')
                    value = str(self.comboboxInclinaisonLateral.currentIndex() - 1)
                    value = '1' if value == '1' else '-1'
                    f.write(str(value) + '\n')
                for output, combox in self.dicoComboxOutput.items():
                    if output != 'Assis' and str(combox.currentText()) != 'Pas test':
                        f.write(output + ':  ')
                        f.write('1\n' if str(combox.currentText()) == 'Oui' else '-1\n')
                # user part
                '''
                with open(DIRPATH+'/Compte/CurrentUser.txt','rb')as fUser:
                    currentUser = str(fUser.readline())
                '''
                currentUser = 'Guest' if self.userCombox.currentIndex() == 0 else self.userCombox.currentText()
                f.write(currentUser + ':  1\n')
                for user in teamKoondal + ['Guest']:
                    if user != currentUser:
                        f.write(user + ':  -1\n')
                f.write('TestId:  ' + str(int(time.time())) + '\n')
                f.close()
            except Exception, e:
                print e
                print 'first file fail'
            else:
                f.close()
                with open(DIRPATH + '/Log/RefreshData/refreshData.txt', 'ab') as f:
                    f.write(self.dataFileName + '\n')
                return filename

    def record(self):
        if self.recordBool == True:
            self.recordBool = not self.recordBool
            self.button.repaint()
            self.button.setText("Start test")
            updateMinuteur(self.minuteur, 0)
            self.createRecord()
        else:
            self.createTestDirectory()
            self.button.repaint()
            self.button.setText("Stop")
            self.lastT0 = time.time()
            self.t0 = time.time()
            self.numberOfFileTest = 0
            self.lines = []
            self.recordBool = not self.recordBool

    def createTestDirectory(self):
        d = datetime.now()
        self.nameDir = DIRTESTRECORD + '/' + str(d).replace(' ', '_').replace(':', '-')[:-7]
        os.makedirs(self.nameDir)

    def createRecord(self):
        print 'create part ' + str(self.numberOfFileTest)
        self.createFileData()
        self.saveFile(path=self.nameDir + '/Part_' + str(self.numberOfFileTest) + '.txt')
        self.numberOfFileTest += 1
        self.lastT0 = time.time()

    def createFileData(self):
        listParam = ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz', 'timeStamp'] + \
                    self.data.getListCurrentOutput()
        self.dataFileName = DIRPATH + '/Log/Data/Data' + str(time.time()) + '.csv'
        lines = self.lines
        writeAllLineInFile(lines, self.dataFileName, title=listParam)

    def initUI(self):
        self.dataVariable = {}
        self.dicoF = {}
        self.plot1 = MatplotlibWidget(self, yName='Acceleration(m./s2)', ymin=-45000, ymax=45000)
        self.navi_toolbar1 = NavigationToolbar(self.plot1.canvas, self)
        l = self.navi_toolbar1.actions()
        for i in l:
            if i.text() == 'Save':
                customizeAction = i
            if i.text() == 'Subplots':
                subplotAction = i
        self.navi_toolbar1.removeAction(customizeAction)
        self.navi_toolbar1.removeAction(subplotAction)
        self.lastMaxPoint = []
        self.lastMinPoint = []
        box = self.plot1.axis.get_position()
        self.plot1.axis.set_position([box.x0, box.y0, box.width * 0.85, box.height])

        self.currentCourbe = 'Accelerometre'
        self.minuteur = QtGui.QLabel("00:00:00")
        self.button = QtGui.QPushButton('Start Test', self)
        self.button.clicked[bool].connect(self.record)
        self.button.setFixedWidth(100)

        grid = QtGui.QGridLayout()
        grid.setSpacing(1)
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput().keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        legendLabel1 = QtGui.QLabel('                 ')
        if self.online:
            listOutputNoCheck = self.serverRequest.getOutput(exist='False')
        else:
            listOutputNoCheck = parseOutput(checkScore=False).keys()
        listAllIndicateur = sorted(getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p').keys())
        self.comboxCourbe1 = createComboBox(['Giroscope', 'Accelerometre', 'Altimetre', 'Magnetometre'] +
                                            self.listOutput + listAllIndicateur)
        self.buttonCourbe1 = QtGui.QPushButton('ajouter', self)
        # self.buttonCourbe1.clicked[bool].connect(self.switchCourbe1)
        self.buttonCourbe1.clicked[bool].connect(self.addFonction)
        self.comboxCourbe1.setCurrentIndex(1)

        buttonHideShow = QtGui.QPushButton('HIDE/SHOW test', self)
        buttonHideShow.clicked[bool].connect(self.hideShow)
        buttonPauseStart = QtGui.QPushButton('PAUSE/START test', self)
        buttonPauseStart.clicked[bool].connect(self.pauseStart)

        groupFonction = QtGui.QScrollArea()
        groupFonction.setWidgetResizable(True)
        self.widgetFonction = QtGui.QWidget()

        self.verticalFonction = QtGui.QVBoxLayout()
        self.widgetFonction.setLayout(self.verticalFonction)
        groupFonction.setWidget(self.widgetFonction)
        groupFonction.setMaximumWidth(200)

        self.dicoComboxOutput = {output: createComboBox(['Pas test', 'Non', 'Oui']) for output in listOutputNoCheck if
                                 output not in ['Flexion-Extension', 'Inclinaisonlaterale_Gauche-Droite',
                                                'Repartitionmouvement_X-Y-Z'] + teamKoondal}
        self.userCombox = createComboBox([u'utilisateur invité'] + teamKoondal)
        self.levelRepartitionMouvement = ['Pas test', 'Frontale', 'Sagitale', 'Transversal']
        self.comboboxRepartitionMouvement = createComboBox(self.levelRepartitionMouvement)
        repartionMouvementLabel = QtGui.QLabel('Repartitionmouvement_X-Y-Z:')
        self.comboboxInclinaisonLateral = createComboBox(['Pas test', 'Gauche', 'Droite'])
        inclinaisonLateralLabel = QtGui.QLabel('Inclinaisonlaterale_Gauche-Droite:')
        self.comboboxFlexionExtension = createComboBox(['Pas test', 'Flexion', 'Extension'])
        flexionExtensionLabel = QtGui.QLabel('Flexion-Extension:')

        descriptionLabel = QtGui.QLabel('Description:')
        self.description = QtGui.QTextEdit()

        groupOutput = QtGui.QGroupBox("Output test")
        scrollArea = QtGui.QScrollArea()
        scrollArea.setWidgetResizable(True)
        h = QtGui.QHBoxLayout()
        v = QtGui.QVBoxLayout()
        i = 0
        for output, combox in sorted(self.dicoComboxOutput.items()):
            ligne = QtGui.QHBoxLayout()
            labelCombobox = QtGui.QLabel(output + ':')
            labelCombobox.setObjectName('comboboxLabel')
            ligne.addWidget(labelCombobox)
            ligne.addWidget(combox)
            v.addLayout(ligne)
            i += 1
            if i % 2 == 0:
                h.addLayout(v)
                v = QtGui.QVBoxLayout()

        if i % 2 != 0:
            ho = QtGui.QHBoxLayout()
            ho.addWidget(repartionMouvementLabel)
            ho.addWidget(self.comboboxRepartitionMouvement)
            v.addLayout(ho)
            h.addLayout(v)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(inclinaisonLateralLabel)
            ho.addWidget(self.comboboxInclinaisonLateral)
            va = QtGui.QVBoxLayout()
            va.addLayout(ho)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(flexionExtensionLabel)
            ho.addWidget(self.comboboxFlexionExtension)
            va.addLayout(ho)
            h.addLayout(va)
        else:
            ho = QtGui.QHBoxLayout()
            ho.addWidget(inclinaisonLateralLabel)
            ho.addWidget(self.comboboxInclinaisonLateral)
            va = QtGui.QVBoxLayout()
            va.addLayout(ho)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(flexionExtensionLabel)
            ho.addWidget(self.comboboxFlexionExtension)
            va.addLayout(ho)
            h.addLayout(va)
            ho = QtGui.QHBoxLayout()
            ho.addWidget(repartionMouvementLabel)
            ho.addWidget(self.comboboxRepartitionMouvement)
            h.addLayout(ho)

        groupOutput.setLayout(h)
        scrollArea.setWidget(groupOutput)

        v2 = QtGui.QVBoxLayout()
        h2 = QtGui.QHBoxLayout()
        h2.addWidget(self.button)
        h2.addWidget(self.minuteur)
        h2.setSpacing(0)
        v2.addLayout(h2)

        h3 = QtGui.QHBoxLayout()
        h3.addWidget(self.navi_toolbar1)
        h3.addWidget(legendLabel1)
        h3.addWidget(self.comboxCourbe1)
        h3.addWidget(self.buttonCourbe1)
        grid.addLayout(h3, 1, 0, 2, 11)
        grid.addWidget(self.plot1, 3, 0, 15, 11)

        grid.addWidget(groupFonction, 1, 11, 7, 1)

        grid.addWidget(buttonPauseStart, 16, 11)
        grid.addWidget(buttonHideShow, 17, 11)

        h = QtGui.QHBoxLayout()
        h.addWidget(descriptionLabel)
        h.addWidget(self.description)

        horizontalBas = QtGui.QHBoxLayout()
        horizontalBas.addWidget(scrollArea)
        horizontalBas.addWidget(self.userCombox)
        horizontalBas.addLayout(h)
        horizontalBas.addLayout(v2)
        horizontalBas.setSpacing(15)
        s = QtGui.QWidget()
        s.setLayout(horizontalBas)
        self.dock = QtGui.QDockWidget('Test')
        self.dock.setFeatures(QtGui.QDockWidget.DockWidgetMovable)
        self.dock.setWidget(s)
        self.dock.hide()
        grid.addWidget(self.dock, 19, 0, 2, 12)
        self.setLayout(grid)
        self.addFonction()
        return grid

    def resetFonction(self):
        self.dicoF = {}
        QtGui.QWidget().setLayout(self.verticalFonction)
        # self.setLayout(self.initUI())
        self.verticalFonction = QtGui.QV1BoxLayout()

    def hideShow(self):
        if self.dock.isHidden():
            self.dock.show()
        else:
            self.dock.hide()

    def pauseStart(self):
        if self.connected:
            self.disconnecting()
            self.connected = False
        else:
            self.connecting()
            self.thread.start()

    def changeMode(self):
        self.labelSld.repaint()
        if 'BUFFER' in str(self.labelSld.text()):
            self.labelSld.setText('Mode CLASSIC : ')
        else:
            self.labelSld.setText('Mode BUFFER : ')

    def addFonction(self):
        indicateur = str(self.comboxCourbe1.currentText())
        if indicateur not in self.dicoF:
            if indicateur == 'Accelerometre':
                if 'ax' in self.dicoF:
                    return

                listIndicateurAdd = ['ax', 'ay', 'az']
            elif indicateur == 'Giroscope':
                if 'gx' in self.dicoF:
                    return

                listIndicateurAdd = ['gx', 'gy', 'gz']
            elif indicateur == 'Magnetometre':
                if 'tx' in self.dicoF:
                    return

                listIndicateurAdd = ['tx', 'ty', 'tz']
            elif indicateur == 'Altimetre':
                if 'alt' in self.dicoF:
                    return

                listIndicateurAdd = ['alt']
            elif indicateur in self.listOutput:
                if indicateur + 'BEST' in self.dicoF:
                    return

                listIndicateurAdd = [indicateur + 'BEST', indicateur + 'NEW', indicateur + 'SERVER']
            else:
                listIndicateurAdd = [indicateur]
            for indicateurtoAdd in listIndicateurAdd:
                self.dicoF[indicateurtoAdd] = {}
                self.dicoF[indicateurtoAdd]['ll'] = [0] * self.MaxPoint
                r = lambda: random.randint(0, 255)
                randomColor = '#%02X%02X%02X' % (r(), r(), r())
                self.dicoF[indicateurtoAdd]['line'], = self.plot1.axis.plot(self.dicoF[indicateurtoAdd]['ll'],
                                                                            color=randomColor, label=indicateurtoAdd)
                self.dicoF[indicateurtoAdd]['checkbox'] = createCheckbox(self.widgetFonction, indicateurtoAdd,
                                                                         self.updateCourbeFonction, check=1)
                self.switch(self.plot1)
                self.verticalFonction.addWidget(self.dicoF[indicateurtoAdd]['checkbox'])

    def updateCourbeFonction(self):
        self.switch(self.plot1)

    def switch(self, plot):
        lineFonction = []
        outputFonction = []
        for output, dicoLine in self.dicoF.items():
            if dicoLine['checkbox'].isChecked():
                lineFonction.append(dicoLine['line'])
                outputFonction.append(output)

        plot.axis.legend(lineFonction, outputFonction, loc=2, borderaxespad=0., bbox_to_anchor=(1.05, 1))
        plot.canvas.draw()

    def addPoint(self, dicoLine, dicoReponse):
        if self.currentPointNumber < self.MaxPoint:
            self.addPlot(dicoLine, dicoReponse)
        else:
            self.graphePop()
            self.appendPlot(dicoLine, dicoReponse)
        self.currentPointNumber += 1

    def addPlot(self, dicoLine, dicoReponse):
        currentIndex = self.currentPointNumber
        first = True
        for indicateur, dicoLineF in self.dicoF.items():
            if self.dicoF[indicateur]['checkbox'].isChecked():
                self.dicoF[indicateur]['line'].set_visible(True)
                if indicateur in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
                    point = dicoLine[indicateur]
                elif indicateur in [output + 'BEST' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-4]][0]
                elif indicateur in [output + 'NEW' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-3]][1]
                elif indicateur in [output + 'SERVER' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-6]][2]
                else:
                    point = float(
                        self.dicoAllIndicateur[indicateur](dicoLine, self.data.getLastLine(), self.dataVariable))
                if isinstance(point, np.ndarray):
                    point = point[0]
                elif isinstance(point, list):
                    point = point[-1]
                if first:
                    minPoint = point
                    maxPoint = point
                    first = False
                else:
                    if point < minPoint:
                        minPoint = point
                    if point > maxPoint:
                        maxPoint = point

                dicoLineF['ll'][currentIndex] = point
            else:
                self.dicoF[indicateur]['line'].set_visible(False)

        if not first:
            if len(self.lastMaxPoint) > 100:
                self.lastMaxPoint.pop(0)
            if len(self.lastMinPoint) > 100:
                self.lastMinPoint.pop(0)
            self.lastMaxPoint.append(maxPoint)
            self.lastMinPoint.append(minPoint)

    def appendPlot(self, dicoLine, dicoReponse):

        first = True
        for indicateur, dicoLineF in self.dicoF.items():
            if self.dicoF[indicateur]['checkbox'].isChecked():
                self.dicoF[indicateur]['line'].set_visible(True)
                if indicateur in ['ax', 'ay', 'az', 'gx', 'gy', 'gz', 'tx', 'ty', 'tz', 'alt']:
                    point = dicoLine[indicateur]
                elif indicateur in [output + 'BEST' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-4]][0]
                elif indicateur in [output + 'NEW' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-3]][1]
                elif indicateur in [output + 'SERVER' for output in self.listOutput]:
                    point = dicoReponse[indicateur[:-6]][2]
                else:
                    point = float(
                        self.dicoAllIndicateur[indicateur](dicoLine, self.data.getLastLine(), self.dataVariable))

                if isinstance(point, np.ndarray):
                    point = float(point[0])
                elif isinstance(point, list):
                    point = float(point[-1])

                if first:
                    minPoint = point
                    maxPoint = point
                    first = False
                else:
                    if point < minPoint:
                        minPoint = point
                    if point > maxPoint:
                        maxPoint = point

                dicoLineF['ll'].append(point)
            else:
                self.dicoF[indicateur]['line'].set_visible(False)

        if not first:
            if len(self.lastMaxPoint) > 100:
                self.lastMaxPoint.pop(0)
            if len(self.lastMinPoint) > 100:
                self.lastMinPoint.pop(0)
            self.lastMaxPoint.append(maxPoint)
            self.lastMinPoint.append(minPoint)

    def graphePop(self):
        for output, dicoLine in self.dicoF.items():
            if self.dicoF[output]['checkbox'].isChecked():
                dicoLine['ll'].pop(0)

    def updateGraphe(self, line):
        if len(self.dicoF.keys()) == 0:
            return

        if line == 'NOPORT':
            self.raiseError('NOPORT', 'Aucun dongle koondal trouver, veuillez redemarrer')
        elif line == 'ERRORWINDOWS':
            self.raiseError('Erreur koondal', (
            'Error de port windows, veuillez redemarer l\'application \n et changer le dongle de port').encode("utf-8"))
        elif line == 'TIMEOUT':
            self.raiseError('DECONNEXION', 'Dongle deconnecte')
        elif line == 'INACTIF':
            self.raiseError('DECONNEXION',
                            'Koondal inacitf trop longtemps, raison :\n\t-plus de batterie\n\t-distance trop longue pendant une longue period\nVeuillez redemarer')
        elif line == 'BADPORT':
            self.raiseError('MAUVAIS PORT', 'Erreur de port de la carte, veuillez redemarrer')
        else:
            self.connected = True
        ll = line.split(':')
        ll2 = map(float, ll)

        dicoReponse = self.data.updateDataAndReturnAnswer(ll)

        dicoLine = {'alt': ll2[0],
                    'ax': ll2[1], 'ay': ll2[2], 'az': ll2[3],
                    'gx': ll2[4], 'gy': ll2[5], 'gz': ll2[6],
                    'tx': ll2[7], 'ty': ll2[8], 'tz': ll2[9],
                    'time': ll2[10]}

        self.addPoint(dicoLine, dicoReponse)
        # redraw line

        self.plot1.axis.clear()
        self.plot1.axis.draw_artist(self.plot1.axis.patch)
        if self.lastMaxPoint:
            self.cpt += 1
            newMax = max(self.lastMaxPoint)
            newMin = min(self.lastMinPoint)
            if self.firstPoint:
                self.lastMax = newMax
                self.lastMin = newMin
            diff = (newMax - newMin) / 5
            if newMax > self.lastMax + diff or newMin < self.lastMin - diff or \
                    self.firstPoint or newMin > self.lastMin + diff or newMax < self.lastMax - diff:
                self.lastMax = newMax
                self.lastMin = newMin
                self.plot1.axis.set_ylim([newMin - diff - 0.1, newMax + diff + 0.1])
                self.switch(self.plot1)
                self.cpt = 0
                self.firstPoint = False

        # draw indicateur
        for indicateur, dico in self.dicoF.items():
            if dico['checkbox'].isChecked():
                dico['line'].set_ydata(dico['ll'])
                self.plot1.axis.draw_artist(dico['line'])

        self.plot1.canvas.update()

        if self.recordBool:
            if len(self.lines) >= 300:
                self.createRecord()
                self.lines = []
            self.lines += [ll]
            seconds = float(ll[10]) - float(self.t0)
            updateMinuteur(self.minuteur, seconds)

