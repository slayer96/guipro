# -*- coding: utf-8 -*-
from PyQt4 import QtGui

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg


class MatplotlibWidget(QtGui.QWidget):
    def __init__(self, parent=None, yName=' ', xName='', ymin=-10, ymax=10):
        super(MatplotlibWidget, self).__init__(parent)

        self.figure = Figure()
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.axis = self.figure.add_subplot(111, xlabel='Time(sec)', ylabel=yName)

        self.figure.subplots_adjust(left=0.08, bottom=0.12, right=0.90, top=0.98)
        self.axis.minorticks_on()
        self.axis.grid(True, which='both')
        self.layoutVertical = QtGui.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.canvas)
        self.setMinimumHeight(400)

    def savefig(self, filePath):
        originalInch = self.figure.get_size_inches()
        self.figure.set_size_inches(18.5, 10.5)
        self.figure.savefig(filePath)
        self.figure.set_size_inches(originalInch)