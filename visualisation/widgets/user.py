# -*- coding: utf-8 -*-
import time
import styleSheet

from datetime import datetime
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL
from LogParser import parseOutput, copyanything
from visualisation.functionality import DIRPATH, teamKoondal, orderPrincipalOutput, createComboBox


class WidgetUser(QtGui.QWidget):
    def __init__(self, parent=None, qThread=None, qData='fake', serverRequest=None):
        super(WidgetUser, self).__init__()
        self.thread = qThread
        self.data = qData

        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False
        self.boolUpdate = True
        self.initUI()

    def raiseError(self, errorType, errorMessage):
        result = QtGui.QMessageBox.question(errorType + "...", errorMessage,
                                            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)

        if result == QtGui.QMessageBox.Ok:
            exit()

    def initUI(self, user='Guest'):
        self.lastUpdate = time.time()
        self.button = QtGui.QPushButton('CONNECTION KOONDAL', self)
        self.button.clicked[bool].connect(self.capture)
        self.buttonStop = QtGui.QPushButton('DECONNECTION KOONDAL', self)
        self.buttonStop.clicked[bool].connect(self.stop)
        self.buttonStop.setEnabled(False)
        self.comboboxUser = createComboBox(['Guest'] + sorted([member for member in teamKoondal if member != 'Guest']))
        self.buttonConnection = QtGui.QPushButton('CONNECTION USER', self)
        self.buttonConnection.clicked[bool].connect(self.changeUser)
        self.buttonSaveFunction = QtGui.QPushButton('Save functions', self)
        self.buttonSaveFunction.clicked[bool].connect(self.saveFunction)
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput(checkScore=True).keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        listOutput = self.outputPrimaire + teamKoondal
        listOutput2 = [output for output in listOutput if output not in teamKoondal]
        self.labelOutput = {output: QtGui.QLabel(output) for output in listOutput2}
        self.reponseCouleurOutput = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseCouleurOutputNEW = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseCouleurOutputSERVER = {output: QtGui.QLabel('XXX') for output in listOutput2}
        self.reponseChiffreOutput = {output: QtGui.QLabel('0') for output in listOutput2}
        self.reponseChiffreOutputNEW = {output: QtGui.QLabel('0') for output in listOutput2}
        self.reponseChiffreOutputSERVER = {output: QtGui.QLabel('0') for output in listOutput2}

        for output in self.labelOutput.keys():
            self.labelOutput[output].setObjectName('labelPageOne')
            self.reponseChiffreOutput[output].setObjectName('labelPageOne')
            self.reponseChiffreOutputNEW[output].setObjectName('labelPageOne')
            self.reponseCouleurOutput[output].setObjectName('reponsePageOne')
            self.reponseCouleurOutputNEW[output].setObjectName('reponsePageOne')
            self.reponseCouleurOutputSERVER[output].setObjectName('reponsePageOne')
            self.reponseChiffreOutputSERVER[output].setObjectName('labelPageOne')

            self.reponseCouleurOutput[output].setMinimumWidth(self.labelOutput[output].width() / 5)
            self.reponseCouleurOutputNEW[output].setMinimumWidth(self.labelOutput[output].width() / 5)
            self.reponseCouleurOutputSERVER[output].setMinimumWidth(self.labelOutput[output].width() / 5)

        gridIntern = QtGui.QGridLayout()
        gridIntern.setSpacing(0)

        v0 = QtGui.QHBoxLayout()
        h0 = QtGui.QHBoxLayout()
        v1 = QtGui.QVBoxLayout()
        v2 = QtGui.QVBoxLayout()
        v3 = QtGui.QVBoxLayout()
        v4 = QtGui.QVBoxLayout()
        v5 = QtGui.QVBoxLayout()
        v6 = QtGui.QVBoxLayout()

        v0.setSpacing(0)
        v1.setSpacing(0)
        v2.setSpacing(0)
        v3.setSpacing(0)
        v4.setSpacing(0)
        v5.setSpacing(0)
        for output in listOutput2:
            h = QtGui.QHBoxLayout()
            h.addWidget(self.reponseCouleurOutput[output])
            h.addWidget(self.reponseCouleurOutputNEW[output])
            h.addWidget(self.reponseCouleurOutputSERVER[output])
            v1.addWidget(self.labelOutput[output])
            v2.addLayout(h)
            v3.addWidget(self.reponseChiffreOutput[output])
            v4.addWidget(self.reponseChiffreOutputNEW[output])
            v5.addWidget(self.reponseChiffreOutputSERVER[output])

        v0.addWidget(self.comboboxUser)
        v0.addWidget(self.buttonConnection)
        self.labelConnected = QtGui.QLabel('Guest')
        self.labelConnected.setObjectName('user')
        self.initUserGuest()
        h0.addWidget(QtGui.QLabel(u'Connecté en tant que:   '))
        h0.addWidget(self.labelConnected)

        hmodeLabel = QtGui.QHBoxLayout()
        hmodeLabel.addWidget(QtGui.QLabel(u'Serveur:'))
        modelabel = QtGui.QLabel('Online') if self.online else QtGui.QLabel('Offline')
        modelabel.setStyleSheet("color:green") if self.online else modelabel.setStyleSheet("color:red")
        hmodeLabel.addWidget(modelabel)
        labelUser = QtGui.QLabel(u'Utilisateur détecté')
        self.currentUserNAME = QtGui.QLabel('XXX')
        self.currentUserCHIFFRE = QtGui.QLabel('0')
        self.currentUserCHIFFRENEW = QtGui.QLabel('0')
        self.currentUserCHIFFRESERVER = QtGui.QLabel('0')
        self.currentUserNAME.setObjectName('reponsePageOne')
        self.currentUserCHIFFRE.setObjectName('reponsePageOne')
        self.currentUserCHIFFRENEW.setObjectName('reponsePageOne')
        self.currentUserCHIFFRESERVER.setObjectName('reponsePageOne')
        labelUser.setObjectName('reponsePageOne')
        v1.addWidget(labelUser)
        v2.addWidget(self.currentUserNAME)
        v3.addWidget(self.currentUserCHIFFRE)
        v4.addWidget(self.currentUserCHIFFRENEW)
        v5.addWidget(self.currentUserCHIFFRESERVER)

        v6.addWidget(self.button)
        v6.addWidget(self.buttonStop)

        groupOutput = QtGui.QGroupBox('OUTPUT')
        groupOutput.setLayout(v1)
        groupReponseIA = QtGui.QGroupBox('REPONSE IA(Interpretation)')
        groupReponseIA.setLayout(v2)
        groupReponseIAC = QtGui.QGroupBox('REPONSE IA BEST')
        groupReponseIAC.setLayout(v3)
        groupReponseIACNEW = QtGui.QGroupBox('REPONSE IA NORMAL')
        groupReponseIACNEW.setLayout(v4)
        groupReponseIACSERVER = QtGui.QGroupBox('REPONSE IA SERVER')
        groupReponseIACSERVER.setLayout(v5)
        gridIntern.setRowMinimumHeight(1, 30)
        gridIntern.addLayout(v0, 0, 8, 1, 1)
        gridIntern.addLayout(hmodeLabel, 0, 0, 1, 1)
        gridIntern.addLayout(h0, 1, 8, 1, 1)
        # gridIntern.addWidget(self.buttonConnection,0,6,1,1)
        gridIntern.addWidget(groupOutput, 2, 0, 10, 1)
        gridIntern.addWidget(groupReponseIA, 2, 2, 10, 1)
        gridIntern.addWidget(groupReponseIAC, 2, 4, 10, 1)
        gridIntern.addWidget(groupReponseIACNEW, 2, 6, 10, 1)
        gridIntern.addWidget(groupReponseIACSERVER, 2, 8, 10, 1)
        gridIntern.addWidget(self.buttonSaveFunction, 12, 6, 1, 1)
        gridIntern.addLayout(v6, 13, 8, 1, 1)

        self.setLayout(gridIntern)
        return gridIntern

    def connecting(self):
        self.connect(self.thread, SIGNAL("output(QString)"), self.updateUi)

    def disconnecting(self):
        self.disconnect(self.thread, SIGNAL("output(QString)"), self.updateUi)

    def saveFunction(self):
        user = str(self.labelConnected.text())
        d = datetime.now()
        for output in self.outputPrimaire:
            nameSrc = DIRPATH + '/Compte/' + user + '/' + output
            nameDst = DIRPATH + '/Backup/' + str(d).replace(' ', '_').replace(':', '-')[:-7] + '/' + output
            copyanything(nameSrc, nameDst)

    def changeUser(self):
        self.stop()
        progress = QtGui.QProgressDialog(u"Téléchargement des nouvelles information ...", "Cancel", 0, 0)
        progress.setCancelButton(None)
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        QtCore.QCoreApplication.processEvents()
        self.labelConnected.setText(self.comboboxUser.currentText())
        self.data.initialize(user=str(self.comboboxUser.currentText()))
        self.button.setEnabled(True)
        self.buttonStop.setEnabled(False)
        progress.close()

    def initUserGuest(self):
        '''
        with open(DIRPATH+'/Compte/CurrentUser.txt','wb') as f:
            f.write('Guest')
        '''

    def stopUpdate(self):
        self.boolUpdate = False

    def startUpdate(self):
        self.boolUpdate = True

    def updateUi(self, line):
        if not self.boolUpdate:
            return
        if line == 'NOPORT':
            self.raiseError('NOPORT', 'Aucun dongle koondal trouver, veuillez redemarrer')
        elif line == 'ERRORWINDOWS':
            self.raiseError('Erreur koondal',
                            (
                            'Error de port windows, veuillez redemarer l\'application \n et changer le dongle de port').encode(
                                "utf-8"))
        elif line == 'TIMEOUT':
            self.raiseError('DECONNEXION', 'Dongle deconnecte')
            self.thread.sleep()
        elif line == 'INACTIF':
            self.raiseError('DECONNEXION',
                            'Koondal inacitf trop longtemps, raison :\n\t-plus de batterie\n\t-distance trop longue pendant une longue period\nVeuillez redemarer')
            self.thread.sleep()
        elif line == 'BADPORT':
            self.raiseError('MAUVAIS PORT', 'Erreur de port de la carte, veuillez redemarrer')
        ll = line.split(':')

        dicoReponse = self.data.updateDataAndReturnAnswer(ll)
        self.lastUpdate = time.time()
        state = self.data.getState()
        for output in self.outputPrimaire:
            outputInServer = len(state[output]) == 3
            if dicoReponse[output] == None or output in teamKoondal:
                self.reponseCouleurOutput[output].repaint()
                self.reponseChiffreOutput[output].repaint()
                self.reponseChiffreOutputNEW[output].repaint()
                self.reponseCouleurOutput[output].setText('XXX')
                self.reponseCouleurOutputNEW[output].setText('XXX')
                self.reponseCouleurOutputSERVER[output].setText('XXX')
                self.reponseChiffreOutput[output].setText('XXX')
                self.reponseChiffreOutputNEW[output].setText('XXX')
                self.reponseChiffreOutputSERVER[output].setText('XXX')
                self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                continue

            self.reponseCouleurOutput[output].repaint()
            self.reponseCouleurOutputNEW[output].repaint()
            self.reponseChiffreOutput[output].repaint()
            self.reponseChiffreOutputNEW[output].repaint()
            self.reponseChiffreOutputSERVER[output].repaint()
            if output == 'Inclinaisonlaterale_Gauche-Droite':
                if state[output][0] == ' ... ':
                    self.reponseCouleurOutput[output].setText('Gauche')
                else:
                    self.reponseCouleurOutput[output].setText('Droite')
                if state[output][1] == ' ... ':
                    self.reponseCouleurOutputNEW[output].setText('Gauche')
                else:
                    self.reponseCouleurOutputNEW[output].setText('Droite')

            elif output == 'Flexion-Extension':
                if state[output][0] == ' ... ':
                    self.reponseCouleurOutput[output].setText('Flexion')
                else:
                    self.reponseCouleurOutput[output].setText('Extension')
                if state[output][1] == ' ... ':
                    self.reponseCouleurOutputNEW[output].setText('Flexion')
                else:
                    self.reponseCouleurOutputNEW[output].setText('Extension')
            else:
                if output != 'Assis':
                    if state[output][0] == ' ... ':
                        self.reponseCouleurOutput[output].setText('Non')
                    else:
                        self.reponseCouleurOutput[output].setText('Oui')
                    if state[output][1] == ' ... ':
                        self.reponseCouleurOutputNEW[output].setText('Non')
                    else:
                        self.reponseCouleurOutputNEW[output].setText('Oui')
                else:
                    self.reponseCouleurOutput[output].setText(state[output][0])
                    self.reponseCouleurOutputNEW[output].setText(state[output][1])
            if outputInServer:
                # self.reponseCouleurOutputSERVER[output].repaint()
                # self.reponseCouleurOutputSERVER[output].setText(state[output][2])
                if output == 'Inclinaisonlaterale_Gauche-Droite':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Gauche')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Droite')

                elif output == 'Flexion-Extension':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Flexion')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Extension')
                elif output != 'Assis':
                    if state[output][2] == ' ... ':
                        self.reponseCouleurOutputSERVER[output].setText('Non')
                    else:
                        self.reponseCouleurOutputSERVER[output].setText('Oui')
                else:
                    self.reponseCouleurOutputSERVER[output].setText(state[output][2])

            self.reponseChiffreOutput[output].setText(str(dicoReponse[output][0]))
            self.reponseChiffreOutputNEW[output].setText(str(dicoReponse[output][1]))
            self.reponseChiffreOutputSERVER[output].setText(str(dicoReponse[output][2]))

            for i in range(len(state[output])):
                if output == 'Repartitionmouvement_X-Y-Z':
                    if state[output][i] == '0':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:green")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:green")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:green")
                    elif state[output][i] == '1':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:yellow")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:yellow")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:yellow")

                    elif state[output][i] == '2':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:orange")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:orange")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:orange")
                    elif state[output][i] == '3':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:red")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:red")
                else:
                    if state[output][i] != ' ... ':
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:green")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:green")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:green")
                    else:
                        if i == 0:
                            self.reponseCouleurOutput[output].setStyleSheet("background-color:red")
                        elif i == 1:
                            self.reponseCouleurOutputNEW[output].setStyleSheet("background-color:red")
                        elif i == 2 and outputInServer:
                            self.reponseCouleurOutputSERVER[output].setStyleSheet("background-color:red")

        self.currentUserNAME.repaint()
        self.currentUserCHIFFRE.repaint()
        self.currentUserCHIFFRENEW.repaint()
        self.currentUserCHIFFRESERVER.repaint()
        teamKoondalNoGuest = [member for member in teamKoondal if member not in ['Guest', 'Maxime']]
        maxUserChiffre = max([dicoReponse[team][0] for team in teamKoondalNoGuest])
        maxUserChiffreNew = max([dicoReponse[team][1][1] for team in teamKoondalNoGuest])
        for key, duo in dicoReponse.items():
            if key not in teamKoondalNoGuest:
                continue
            if duo[0] == maxUserChiffre:
                name = key
        teamReponse = {dicoReponse[team][1][1]: team for team in teamKoondalNoGuest}
        self.currentUserCHIFFRE.setText(str(maxUserChiffre))
        self.currentUserCHIFFRENEW.setText(str(teamReponse[maxUserChiffreNew]))
        self.currentUserCHIFFRESERVER.setText(str(teamReponse[maxUserChiffreNew]))
        self.currentUserNAME.setText(name)

    def stop(self):
        global cmdToSend
        cmdToSend = "L"
        self.button.setEnabled(True)
        self.buttonStop.setEnabled(False)
        self.disconnecting()
        # self.stopped = True
        # self.thread.wait()

    def capture(self):
        global cmdToSend
        self.button.setEnabled(False)
        self.buttonStop.setEnabled(True)
        self.connecting()
        cmdToSend = "l"
        self.thread.start()
        self.t0 = time.time()


