from PyQt4 import QtGui
from PyQt4.QtGui import QApplication

from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from LogParser import parseOutput
from math_plot_lib import MatplotlibWidget

from visualisation.functionality import DIRPATH, createCheckbox, teamKoondal, orderPrincipalOutput, parseFile


class WidgetCompare(QtGui.QWidget):
    def __init__(self, parent=None, serverRequest=None):
        super(WidgetCompare, self).__init__(parent)
        self.currentOpen = 0
        self.Listdico = [{}, {}, {}]
        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
        else:
            self.online = False

        self.initUI()

    def getCanvas(self):
        return self.plot.canvas

    def openFile(self):
        try:
            filename, extension = QtGui.QFileDialog.getOpenFileNameAndFilter(self, 'Open File', DIRPATH + '/TestRecord',
                                                                             filter=QApplication.translate('etmEditor',
                                                                                                           'txt File (*.txt)'))
            with open(filename, 'rb') as f:
                dataName = f.readline()
                path_split = dataName.split("/")
                dataName = DIRPATH + '/Log/Data/' + path_split[-1][:-2]
                dicoList = parseFile(dataName)
                if self.currentOpen == 3:
                    self.currentOpen = 0

                t0 = float(dicoList['timeStamp'][0])
                dicoList['timeStamp'] = [float(t) - t0 for t in dicoList['timeStamp']]
                self.Listdico[self.currentOpen] = dicoList
                self.dicoCheckboxCourbe['Courbe' + str(self.currentOpen)].setChecked(True)
                self.currentOpen += 1
                self.updatePlot()
        except Exception, e:
            print 'Error in openFile (Widget Compare) : ' + str(e)

    def isOpen(self):
        return self.currentOpen > 0

    def updatePlot(self):
        self.plot.axis.clear()
        listLine2D = []
        listNameLine = []
        for name, checkbox in self.dicoCheckbox.items():
            if checkbox.isChecked():
                for i in range(self.currentOpen):
                    if self.dicoCheckboxCourbe['Courbe' + str(i)].isChecked():
                        line = self.plot.axis.plot(self.Listdico[i]['timeStamp'], self.Listdico[i][str(name)],
                                                   label=str(name))
                        listLine2D.append(line)
                        nameCourbe = 'Courbe' + str(i) + ' ' + str(name)
                        listNameLine.append(nameCourbe)
        self.plot.axis.legend(listLine2D, listNameLine, loc=2, borderaxespad=0., bbox_to_anchor=(1.05, 1))
        self.plot.canvas.draw()

    def initUI(self):
        if self.online:
            listOutput = self.serverRequest.getOutput(exist='True')
        else:
            listOutput = parseOutput(checkScore=True).keys()
        self.outputPrimaire = orderPrincipalOutput + sorted(
            [output for output in listOutput if output not in teamKoondal + orderPrincipalOutput])
        self.listOutput = self.outputPrimaire + teamKoondal
        button = QtGui.QPushButton('open graphe', self)
        button.clicked[bool].connect(self.openFile)
        self.plot = MatplotlibWidget(self, yName='Acceleration(m./sec2) - Rotation(rad/sec)')
        self.navi_toolbar = NavigationToolbar(self.plot.canvas, self)
        dicoCheckboxInput = {inputIndic: createCheckbox(self, inputIndic, self.updatePlot, check=0)
                             for inputIndic in ['Alt', 'Ax', 'Ay', 'Az', 'Gx', 'Gy', 'Gz', 'Tx', 'Ty', 'Tz']}

        dicoCheckboxOutput = {output: createCheckbox(self, output, self.updatePlot, check=0) for output in
                              self.listOutput}

        self.dicoCheckbox = {}
        self.dicoCheckbox.update(dicoCheckboxInput)
        self.dicoCheckbox.update(dicoCheckboxOutput)
        self.dicoCheckboxCourbe = {'Courbe' + str(i): createCheckbox(self, 'Courbe' + str(i), self.updatePlot, check=0)
                                   for i in range(len(self.Listdico))}
        layoutHorizontal1 = QtGui.QHBoxLayout()

        for inputIndic, checkbox in dicoCheckboxInput.items():
            layoutHorizontal1.addWidget(checkbox)
        layoutHorizontal = QtGui.QHBoxLayout()
        for inputIndic, checkbox in self.dicoCheckboxCourbe.items():
            layoutHorizontal.addWidget(checkbox)
        layoutHorizontal2 = QtGui.QHBoxLayout()
        for output, checkbox in dicoCheckboxOutput.items():
            layoutHorizontal2.addWidget(checkbox)

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)
        grid.addWidget(self.navi_toolbar, 0, 0)
        grid.addLayout(layoutHorizontal1, 1, 0)
        grid.addLayout(layoutHorizontal2, 2, 0)
        grid.addWidget(self.plot, 3, 0, 3, 5)
        grid.addLayout(layoutHorizontal, 6, 1)
        grid.addWidget(button, 6, 5, 1, 1)
        self.setLayout(grid)
        box = self.plot.axis.get_position()
        self.plot.axis.set_position([box.x0, box.y0, box.width * 0.85, box.height])

