# -*- coding: utf-8 -*-
import csv
import pandas


from Constant import *

from PyQt4.QtCore import SIGNAL

from PyQt4.QtGui import QCheckBox
from PyQt4.QtGui import QComboBox
from PyQt4.QtGui import QFont

cmdToSend = ""
# DIRPATH =os.path.dirname(os.path.realpath(__file__))

with open(DIRPATH + '/Compte/ListUser.txt', 'rb') as f:
    teamKoondal = f.readlines()
teamKoondal = map(lambda name: name.replace('\n', ''), teamKoondal)
teamKoondal = map(lambda name: name.replace('\r', ''), teamKoondal)

orderPrincipalOutput = ['Assis', 'Inclinaisonlaterale_Gauche-Droite', 'Flexion-Extension', 'Fluiditemouvement_Oui-Non',
                        'Stabilitebassin_Oui-Non', 'Repartitionmouvement_X-Y-Z']

try:
    print 'start'
except:
    print 'error connection Serveur'
    pass


def writeAllLineInFile(lines, pathFile, title=[]):
    if len(lines):
        try:
            f = open(pathFile, 'wb')
            writer = csv.writer(f)
            if title:
                writer.writerow(title)
            for line in lines:
                writer.writerow(line)
        finally:
            f.close()


def parseFile(filename):
    with open(filename) as f:
        listVariable = f.readline()[:-1].split(',')
    try:
        fileCsv = pandas.read_csv(filename)
        dicoList = {}
        for indicateurBrut in listVariable:
            dicoList[indicateurBrut] = fileCsv[indicateurBrut]

        return dicoList
    except Exception, e:
        print 'Error in parseFile : ' + e


def updateMinuteur(label, seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    label.repaint()
    label.setText("%d:%02d:%02d" % (h, m, s))


def createCheckbox(classObject, label, connectFonction, check=1):
    checkBox = QCheckBox(label)
    checkBox.setChecked(check)
    checkBox.setFont(QFont("Arial", pointSize=12, weight=QFont.Bold))
    if connectFonction != None:
        classObject.connect(checkBox, SIGNAL("clicked()"), connectFonction)
    return checkBox


def createComboBox(listItem):
    comboBox = QComboBox(parent=None)
    for item in listItem:
        comboBox.addItem(item, userData=None)
    return comboBox

