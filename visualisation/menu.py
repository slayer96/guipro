# -*- coding: utf-8 -*-
import Data
import styleSheet
import WidgetTest
import os
import serial

from PyQt4 import QtCore
from PyQt4 import QtGui

from ServerRequest import ServerRequest
from worker_user import WorkerUser
from widgets.plot import WidgetPlot
from widgets.user import WidgetUser
from widgets.compare_widget import WidgetCompare
from widgets.widgetIA import WidgetIA
from functionality import DIRPATH, DIRREFRESHDATAFILE


class Menu(QtGui.QMainWindow):
    def __init__(self):
        super(Menu, self).__init__()
        progress = QtGui.QProgressDialog(u"Lancement du GUI en cours...", "Cancel", 0, 6)
        progress.setCancelButton(None)
        progress.setStyleSheet(styleSheet.getStyle())
        progress.show()
        QtCore.QCoreApplication.processEvents()

        try:
            serverRequest = ServerRequest()
        except:
            serverRequest = None

        self.workerUser = WorkerUser('capteur')
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.data = Data.Data(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.lastWid = 'User'
        self.widPlot = WidgetPlot(qThread=self.workerUser, qData=self.data, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widUser = WidgetUser(qThread=self.workerUser, qData=self.data, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widCompare = WidgetCompare(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widTest = WidgetTest.getWidgetTest(serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()
        self.widIa = WidgetIA(qData=self.data, widUser=self.widUser, widPlot=self.widPlot, serverRequest=serverRequest)
        progress.setValue(progress.value() + 1)
        QtCore.QCoreApplication.processEvents()

        self.initUI()

    def closeEvent(self, event):
        result = QtGui.QMessageBox.question(self,
                                            "Confirm Exit...",
                                            "Etez vous sure de vouloir quitter ?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        event.ignore()

        if result == QtGui.QMessageBox.Yes:
            if self.workerUser.capteur != 'capteur':
                self.workerUser.__del__()

            self.widUser.stop()
            self.widPlot.disconnecting()
            event.accept()
            print 'accepted'
            exit()

    def openFile(self):
        self.switchToBilan()
        self.widCompare.openFile()

    def exportGraph(self):
        if self.widCompare.parent and self.widCompare.isOpen():
            self.widCompare.export()

    def switchToCurve(self):
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widUser.stopUpdate()
        self.widPlot.connecting()
        self.setCentralWidget(self.widPlot)
        self.lastWid = 'Curve'

    def switchToTest(self):
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widPlot.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        if os.stat(DIRREFRESHDATAFILE).st_size != 0:
            try:
                serverRequest = ServerRequest()
            except:
                serverRequest = None
            self.widTest = WidgetTest.getWidgetTest(serverRequest=serverRequest)

        self.setCentralWidget(self.widTest)

    def switchToUser(self):
        self.widUser.startUpdate()
        self.widPlot.setParent(None)
        self.widCompare.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.connecting()
        self.setCentralWidget(self.widUser)
        self.lastWid = 'User'

    def switchToBilan(self):
        self.widPlot.setParent(None)
        self.widUser.setParent(None)
        self.widIa.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        self.setCentralWidget(self.widCompare)
        self.lastWid = 'Compare'

    def switchToIa(self):
        self.widPlot.setParent(None)
        self.widUser.setParent(None)
        self.widCompare.setParent(None)
        self.widTest.setParent(None)
        self.widPlot.disconnecting()
        self.widUser.disconnecting()
        self.setCentralWidget(self.widIa)
        self.lastWid = 'IA'

    def initUI(self):
        exitAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.exportAction = QtGui.QAction('Export Graphe to JPG', self)
        self.exportAction.setShortcut('Ctrl+E')
        self.exportAction.setStatusTip('Export Graphe openned to JPG ')
        self.exportAction.triggered.connect(self.exportGraph)

        openAction = QtGui.QAction('Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a file')
        openAction.triggered.connect(self.openFile)

        curveAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Graphe.png'), 'Test', self)
        curveAction.setShortcut('Ctrl+G')
        curveAction.setStatusTip('Start a test')
        curveAction.triggered.connect(self.switchToCurve)

        testAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Score.png'), 'Test', self)
        testAction.setShortcut('Ctrl+G')
        testAction.setStatusTip('Start a test')
        testAction.triggered.connect(self.switchToTest)

        userAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/User.png'), 'User', self)
        userAction.setShortcut('Ctrl+U')
        userAction.setStatusTip('Check position')
        userAction.triggered.connect(self.switchToUser)

        bilanAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Bilan.png'), 'Bilan', self)
        bilanAction.setShortcut('Ctrl+B')
        bilanAction.setStatusTip('Bilan')
        bilanAction.triggered.connect(self.switchToBilan)

        iaAction = QtGui.QAction(QtGui.QIcon(DIRPATH + '/Icon/Ia.png'), 'IA', self)
        iaAction.setStatusTip('IA')
        iaAction.triggered.connect(self.switchToIa)

        toolbar = self.addToolBar('User')
        toolbar.addAction(userAction)
        toolbar.addAction(curveAction)
        toolbar.addAction(bilanAction)
        toolbar.addAction(iaAction)
        toolbar.addAction(testAction)

        toolbar.setIconSize(QtCore.QSize(35, 35))

        self.menubar = self.menuBar()

        self.menu = self.menubar.addMenu('&Bluetooth')

        self.subMenu = self.menu.addMenu("chercher des Koondals ...")

        for item in self.seekBlue():
            entry = self.subMenu.addAction(item)
            entry.setCheckable(True)
            self.connect(entry, QtCore.SIGNAL('triggered()'), lambda item=item: self.doStuff(item))

        self.setWindowTitle('Koondal')
        self.setWindowIcon(QtGui.QIcon(DIRPATH + '/Icon/Koondal2.ico'))
        self.setCentralWidget(self.widUser)
        self.showMaximized()

    def doStuff(self, item):
        print item

    def seekBlue(self):
        ports = list(serial.tools.list_ports.comports())
        menuitems = []
        for p in ports:
            if "COM" in p[0] and 'Bluetooth' in p[1]:
                menuitems.append(p[0])

        return menuitems

