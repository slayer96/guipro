# -*- coding: utf-8 -*-
import numpy as np
import os
import pickle

from LogParser import getPCA, getDicoRegression, getDicoIndicateur, parseOutput
import sklearn
from Constant import *
from ServerRequest import ServerRequest

teamKoondal = ['Mourougan', 'Tarik', 'Paul', 'Agathe', 'Rouis', 'Maxime']

orderPrincipalOutput = ['Assis', 'Inclinaisonlaterale_Gauche-Droite', 'Flexion-Extension', 'Fluiditemouvement_Oui-Non',
                        'Stabilitebassin_Oui-Non', 'Repartitionmouvement_X-Y-Z']

DIRPATH = os.path.dirname(os.path.realpath(__file__))

'''
Purpose : convertie la reponse chiffre en reponse assis ou debout, en verifiant quil faut 10 assis avant de detre assis
'''


def convertReponseAssis(dico, dicoReponse, etat, label, nbAssis=0, i=0):
    tupleEtat = []
    tupleAssis = [0, 0, 0]

    nb = nbAssis[i]

    value = float(dicoReponse['Assis'][i])
    if etat == 'inconnu':
        return ['Debout'], [nb]

    if value > 50:
        tupleEtat += ['Assis']
    else:
        tupleEtat += ['Debout']

    return tupleEtat, tupleAssis


def convertReponseBinaire(dico, dicoReponse, etat, label, i=0):
    tupleEtat = []

    if dicoReponse[label] is None:
        tupleEtat += [None]
    value = float(dicoReponse[label][i])

    if etat == 'inconnu':
        tupleEtat += [label]
    elif value > 50:  # (float(mean) + diff):
        # Assis, au dessus de la moyenne
        tupleEtat += [label]

    elif value < 50:  # (float(mean) - diff):
        # Debout, en dessous de la moyenne - diff
        tupleEtat += [' ... ']
    else:
        # etat precedent, inconnu entre les deux barriere
        tupleEtat += [etat]
    return tupleEtat


def convertReponseMultiple(dico, dicoReponse, etat, label, i=0):
    tupleEtat = []

    reponse = float(dicoReponse[label][i])
    if reponse > 50:
        tupleEtat += ['2']
    else:
        tupleEtat += ['1']

    return tupleEtat


def convertLogistic(dico, dicoReponse, etat, label, nbAssis=0, i=0):
    dicoExtracted = {}
    dicoExtracted[label] = [list(tupleReponse)[-1] if not isinstance(tupleReponse, int) else tupleReponse for
                            tupleReponse in dicoReponse[label]]
    if label == 'Assis':
        return convertReponseAssis(dico, dicoExtracted, etat, label, nbAssis=nbAssis, i=i)
    elif label == 'Repartitionmouvement_X-Y-Z':
        return convertReponseMultiple(dico, dicoExtracted, etat, label, i=i)
    else:
        return convertReponseBinaire(dico, dicoExtracted, etat, label, i=i)


class Data:
    def __init__(self, serverRequest=None):
        if serverRequest is not None:
            self.serverRequest = serverRequest
            self.online = True
            self.listOutput = self.serverRequest.getOutput(exist='True')
        else:
            self.online = False
            self.listOutput = parseOutput(checkScore=False).keys()

        self.data = self.initDicoData()
        # dictionnaire contenant toute les fonction regression et pca pour chaque output
        self.dicoFonction = self.initDicoFonction()
        # dictionnaire associant a chaque output chiffre une methode de conversion en string
        self.dicoConvert = self.initDicoConvert()
        # list des 100 dernieres ligne lu (les donnes sont stocker dans un dico)
        self.lastLine = []
        # list des 100 derniere reponse des outputs
        self.lastReponse = []
        self.lastReponse.append({output: 0 for output in self.listOutput})
        # nombre de fois que Data a ete update
        self.numberOfUpdate = 0
        # detection du premier passage
        self.first = True
        self.dicoIndicateur = getDicoIndicateur(path=DIRPATH + '/Dictionnaire/indicateur.p')

    def initialize(self, user='Guest'):
        if self.online:
            self.listOutput = self.serverRequest.getOutput(exist='True')
        else:
            self.listOutput = parseOutput(checkScore=False).keys()

        if user == 'Guest':
            self.dicoFonction = self.initDicoFonction()
        else:
            self.dicoFonction = self.initDicoFonction(user=user)
        self.data = self.initDicoData()
        self.dicoConvert = self.initDicoConvert()
        self.lastLine = []
        self.lastReponse = []

        self.lastReponse.append({output: 0 for output in self.listOutput})
        self.numberOfUpdate = 0
        self.first = True

    def initDicoData(self):
        dicoData = {}
        return dicoData

    def saveFunctionServer(self, output, regression, listIndicateur, pca):
        if regression is None:
            return
        if not os.path.exists(DIRSERVER + output):
            os.makedirs(DIRSERVER + output)
        with open(DIRSERVER + output + '/regression.p', "wb") as f:
            pickle.dump(regression, f)
        with open(DIRSERVER + output + '/listIndicateur.p', "wb") as f:
            pickle.dump(listIndicateur, f)
        with open(DIRSERVER + output + '/pca.p', "wb") as f:
            pickle.dump(pca, f)

    def initDicoFonction(self, user='Guest'):
        dicoFonction = {output: {} for output in self.listOutput}
        for key, value in dicoFonction.items():
            # SERVER ONE
            if self.online:
                try:
                    print 'download : ' + key
                    value['regressionSERVER'] = self.serverRequest.downloadFonction(str(key), user)
                    value['pcaSERVER'] = self.serverRequest.downloadPCA(key, user)
                    value['indicateurSERVER'] = self.serverRequest.downloadIndicateur(key, user)
                    value['dataVariableSERVER'] = {}
                    if key not in self.serverRequest.getTeamKoondal():
                        self.saveFunctionServer(key, value['regressionSERVER'], value['indicateurSERVER'],
                                                value['pcaSERVER'])
                except Exception, e:
                    print e
                    print 'Exception init Data server : regression, pca, indicateur'
                    exit()
            else:
                if os.path.exists(DIRSERVER + key):
                    with open(DIRSERVER + key + '/regression.p', 'rb') as f:
                        value['regressionSERVER'] = pickle.load(f)
                    with open(DIRSERVER + key + '/pca.p', 'rb') as f:
                        value['pcaSERVER'] = pickle.load(f)
                    with open(DIRSERVER + key + '/listIndicateur.p', 'rb') as f:
                        value['indicateurSERVER'] = pickle.load(f)
                else:
                    value['regressionSERVER'] = None
                    value['pcaSERVER'] = None
                    value['indicateurSERVER'] = None
                value['dataVariableSERVER'] = {}
            # NEW ONE
            try:
                value['regression'] = getDicoRegression(
                    path=DIRPATH + "/Compte/" + user + "/" + key + "/donnee.p").itervalues().next()
                value['pca'] = getPCA(path=DIRPATH + "/Compte/" + user + "/" + key + "/pca.p")
                value['indicateur'] = getDicoIndicateur(path=DIRPATH + "/Compte/" + user + "/" + key + "/indicateur.p")
                value['dataVariable'] = {}
            except:
                # cette personne n'a pas cette fonction
                value['regression'] = None
                value['pca'] = None
                value['indicateur'] = None
                value['regressionBEST'] = None
                value['pcaBEST'] = None
                value['indicateurBEST'] = None
                value['dataVariableBEST'] = {}
                value['dataVariable'] = {}
                continue
            # BEST ONE
            try:
                value['regressionBEST'] = getDicoRegression(
                    path=DIRPATH + "/Compte/" + user + "/BEST/" + key + "/donnee.p").itervalues().next()
                value['pcaBEST'] = getPCA(path=DIRPATH + "/Compte/" + user + "/BEST/" + key + "/pca.p")
                value['indicateurBEST'] = getDicoIndicateur(
                    path=DIRPATH + "/Compte/" + user + "/BEST/" + key + "/indicateur.p")
                value['dataVariableBEST'] = {}
            except:
                value['regressionBEST'] = getDicoRegression(
                    path=DIRPATH + "/Compte/" + user + "/" + key + "/donnee.p").itervalues().next()
                value['pcaBEST'] = getPCA(path=DIRPATH + "/Compte/" + user + "/" + key + "/pca.p")
                value['indicateurBEST'] = getDicoIndicateur(
                    path=DIRPATH + "/Compte/" + user + "/" + key + "/indicateur.p")
                value['dataVariableBEST'] = {}

        return dicoFonction

    def initStatutAndMinMax(self, dicoReponse):
        self.dicoMinMax = {output: {} for output in self.listOutput}
        for key, value in self.dicoMinMax.items():
            valueTriple = []
            for i in range(3):
                regressionExist = True
                if i == 0 and self.dicoFonction[key]['regressionBEST'] is None:
                    valueTriple += [None]
                    regressionExist = False
                elif i == 1 and self.dicoFonction[key]['regression'] is None:
                    valueTriple += [None]
                    regressionExist = False
                elif i == 2 and self.dicoFonction[key]['regressionSERVER'] is None:
                    valueTriple += [None]
                    regressionExist = False
                if regressionExist:
                    if isinstance(dicoReponse[key][i], list):
                        valueTriple += [float(dicoReponse[key][i][1])]
                    else:
                        valueTriple += [float(dicoReponse[key][i])]
            value['min'] = valueTriple
            value['max'] = valueTriple
            value['numberUpdateMin'] = [1, 1, 1]
            value['numberUpdateMax'] = [1, 1, 1]
        self.state = {
            'Repartitionmouvement_X-Y-Z': ['2', '2', '2'],
        }
        dicoStateTemp = {output: ['inconnu', 'inconnu', 'inconnu'] for output in self.listOutput if
                         output not in ['Repartitionmouvement_X-Y-Z']}
        self.state.update(dicoStateTemp)
        self.nbAssis = [0, 0, 0]
        self.listCurrentOutput = [output for output, value in dicoReponse.items() if value != None]

    def initDicoConvert(self):
        dicoConvert = {output: [None for i in range(3)] for output in self.listOutput}
        for output in self.listOutput:
            # FONCTION BEST
            if self.dicoFonction[output]['regressionBEST'] is None:
                pass
            elif isinstance(self.dicoFonction[output]['regressionBEST'], sklearn.linear_model.Ridge):
                if output == 'Assis':
                    dicoConvert[output][0] = convertReponseAssis
                elif output == 'Repartitionmouvement_X-Y-Z':
                    dicoConvert[output][0] = convertReponseMultiple
                else:
                    dicoConvert[output][0] = convertReponseBinaire
            elif isinstance(self.dicoFonction[output]['regressionBEST'], sklearn.linear_model.LogisticRegression):
                dicoConvert[output][0] = convertLogistic
            elif isinstance(self.dicoFonction[output]['regressionBEST'], sklearn.linear_model.LogisticRegressionCV):
                dicoConvert[output][0] = convertLogistic

            # FONCTION NEW
            if self.dicoFonction[output]['regression'] is None:
                pass
            elif isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.Ridge):
                if output == 'Assis':
                    dicoConvert[output][1] = convertReponseAssis
                elif output == 'Repartitionmouvement_X-Y-Z':
                    dicoConvert[output][1] = convertReponseMultiple
                else:
                    dicoConvert[output][1] = convertReponseBinaire
            elif isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.LogisticRegression):
                dicoConvert[output][1] = convertLogistic
            elif isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.LogisticRegressionCV):
                dicoConvert[output][1] = convertLogistic

                # FONCTION SERVER
            if self.dicoFonction[output]['regressionSERVER'] is None:
                pass
            elif isinstance(self.dicoFonction[output]['regressionSERVER'], sklearn.linear_model.Ridge):
                if output == 'Assis':
                    dicoConvert[output][2] = convertReponseAssis
                elif output == 'Repartitionmouvement_X-Y-Z':
                    dicoConvert[output][2] = convertReponseMultiple
                else:
                    dicoConvert[output][2] = convertReponseBinaire
            elif isinstance(self.dicoFonction[output]['regressionSERVER'], sklearn.linear_model.LogisticRegression):
                dicoConvert[output][2] = convertLogistic
            elif isinstance(self.dicoFonction[output]['regressionSERVER'], sklearn.linear_model.LogisticRegressionCV):
                dicoConvert[output][2] = convertLogistic

        return dicoConvert

    def updateMinMax(self, dicoReponse):
        for key, valueTriple in dicoReponse.items():
            if self.dicoFonction[key]['regression'] is None:
                continue
            for i in range(len(valueTriple)):
                if valueTriple[i] is None or self.dicoMinMax[key]['max'][i] is None or self.dicoMinMax[key]['min'][i]:
                    continue
                if key != 'Assis' and self.state['Assis'][i] != 'Assis':
                    continue
                value = valueTriple[i]

                if isinstance(value, list):
                    value = float(value[1])
                if len(filter(lambda x, rep=float(value): x > rep,
                              map(float, [currentReponse[key] for currentReponse in self.lastReponse[-20:]]))) == 0 or \
                                float(value) > self.dicoMinMax[key]['max'][i]:
                    maxi = ((self.dicoMinMax[key]['numberUpdateMax'][i] * self.dicoMinMax[key]['max'][i]) + float(
                        value)) / (self.dicoMinMax[key]['numberUpdateMax'][i] + 1)
                    self.dicoMinMax[key]['max'][i] = maxi
                    self.dicoMinMax[key]['numberUpdateMax'][i] += 1
                if len(filter(lambda x, rep=float(value): x < rep,
                              map(float, [currentReponse[key] for currentReponse in self.lastReponse[-20:]]))) == 0 or \
                                float(value) < self.dicoMinMax[key]['min'][i]:
                    mini = ((self.dicoMinMax[key]['numberUpdateMin'][i] * self.dicoMinMax[key]['min'][i]) + float(
                        value)) / (self.dicoMinMax[key]['numberUpdateMin'][i] + 1)
                    self.dicoMinMax[key]['min'][i] = mini
                    self.dicoMinMax[key]['numberUpdateMin'][i] += 1

    def updateState(self, dicoReponse):

        for outputName, value in self.state.items():
            state = []
            nbCumulPosition = []
            if self.dicoFonction[outputName]['regression'] is None:
                continue
            for i in range(3):
                if dicoReponse[outputName][i] is None or self.dicoConvert[outputName][i] is None:
                    continue
                elif outputName == 'Assis':
                    stateAssis, nbAssis = self.dicoConvert[outputName][i](self.dicoMinMax, dicoReponse,
                                                                          self.state['Assis'][i], 'Assis',
                                                                          nbAssis=self.nbAssis, i=i)
                    nbCumulPosition += nbAssis
                    state += stateAssis
                else:
                    state += self.dicoConvert[outputName][i](self.dicoMinMax, dicoReponse, self.state[outputName][i],
                                                             outputName, i=i)

            self.state[outputName] = state
            if outputName == 'Assis':
                self.nbAssis = nbCumulPosition

    def getState(self):
        return self.state

    def getListCurrentOutput(self):
        return self.listCurrentOutput

    def getLastLine(self):
        return self.lastLine

    '''
    Purpose: update mean et variance de tout les indicateurs, si un des indicateurs depasse de trop loin la variance on renvoit false
    '''

    def updateData(self, listKey, listValue):
        allIndicateurOk = True
        for i in range(len(listKey)):
            self.data[listKey[i]]['mean'] = (self.numberOfUpdate * self.data[listKey[i]]['mean'] + listValue[i]) / (
                self.numberOfUpdate + 1)
            self.data[listKey[i]]['variance'] = ((self.numberOfUpdate * self.data[listKey[i]]['variance']) + (
                listValue[i] - self.data[listKey[i]]['mean']) ** 2) / (self.numberOfUpdate + 1)
            self.numberOfUpdate += 1
        return allIndicateurOk

    '''
    Purpose:  prend la ligne courante, ainsi que les dernieres ligne et retourne les réponses des differentes output
    '''

    def updateDataAndReturnAnswer(self, line):
        # print 'in update MOFO'
        try:
            ll = map(float, line)
        except Exception, e:
            print e
            print '********** IN DATA UPDATE ERROR'
            print line
            print '**********'

        dicoLine = {'alt': ll[0],
                    'ax': ll[1], 'ay': ll[2], 'az': ll[3],
                    'gx': ll[4], 'gy': ll[5], 'gz': ll[6],
                    'tx': ll[7], 'ty': ll[8], 'tz': ll[9],
                    'time': ll[10],
                    'std': 0
                    }

        # ajout de la derniere ligne
        if len(self.lastLine) > 100:
            self.lastLine.pop(0)
            self.lastReponse.pop(0)
        self.lastLine.append(dicoLine)

        # calcul des reponse
        dicoReponse = {}
        dicoReponseREMEMBER = {}
        # print 'before FOR'
        for output in self.listOutput:
            if self.dicoFonction[output]['regression'] is None:
                # aucun test associé existe donc aucune regression
                dicoReponse[output] = None
            if self.dicoFonction[output]['regressionBEST'] is None:
                # aucun test associé existe donc aucune regression
                dicoReponseREMEMBER[output] = None
            else:
                # matrice SERVER
                if self.dicoFonction[output]['indicateurSERVER'] is None:
                    # pas de fonction calculé coté serveur
                    outputInServer = False
                else:
                    outputInServer = True
                    matriceSERVER = np.array(map(float, [
                        dicoLine[indicateur.lower()] if indicateur.lower() in dicoLine else self.dicoIndicateur[
                            indicateur](dicoLine, self.lastLine, self.dicoFonction[output]['dataVariableSERVER']) for
                        indicateur in self.dicoFonction[output]['indicateurSERVER']]))
                    self.dicoFonction[output]['indicateurSERVER']
                    if self.dicoFonction[output]['pcaSERVER'] != None and \
                                    self.dicoFonction[output]['pcaSERVER'].get_params()['n_components'] != 0:
                        matriceSERVER = self.dicoFonction[output]['pcaSERVER'].transform(matriceSERVER)
                    else:
                        matriceSERVER = [matriceSERVER]

                # matrice NEW
                matrice = np.array(map(float, [
                    dicoLine[indicateur.lower()] if indicateur.lower() in dicoLine else self.dicoIndicateur[indicateur](
                        dicoLine, self.lastLine, self.dicoFonction[output]['dataVariable']) for indicateur in
                    self.dicoFonction[output]['indicateur']]))
                if self.dicoFonction[output]['pca'] != None and self.dicoFonction[output]['pca'].get_params()[
                    'n_components'] != 0:
                    matrice = self.dicoFonction[output]['pca'].transform(matrice)
                else:
                    matrice = [matrice]

                # matrice BEST
                matriceBEST = np.array(map(float, [
                    dicoLine[indicateur.lower()] if indicateur.lower() in dicoLine else self.dicoIndicateur[indicateur](
                        dicoLine, self.lastLine, self.dicoFonction[output]['dataVariableBEST']) for indicateur in
                    self.dicoFonction[output]['indicateurBEST']]))
                if self.dicoFonction[output]['pcaBEST'] != None and self.dicoFonction[output]['pcaBEST'].get_params()[
                    'n_components'] != 0:
                    matriceBEST = self.dicoFonction[output]['pcaBEST'].transform(matriceBEST)
                else:
                    matriceBEST = [matriceBEST]

                try:
                    if isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.Ridge):
                        reponseNormal = self.dicoFonction[output]['regression'].predict(matrice[0])
                    elif isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.LogisticRegression):
                        try:
                            reponseNormal = ["%.2f" % float(i * 100) for i in
                                             self.dicoFonction[output]['regression'].predict_proba(matrice[0])[0]]
                        except Exception, e:
                            print e
                    elif isinstance(self.dicoFonction[output]['regression'], sklearn.linear_model.LogisticRegressionCV):
                        reponseNormal = ["%.2f" % float(i * 100) for i in
                                         self.dicoFonction[output]['regression'].predict_proba(matrice[0])[0]]
                    else:
                        reponseNormal = self.dicoFonction[output]['regression'].predict(matrice[0])

                    if isinstance(self.dicoFonction[output]['regressionBEST'], sklearn.linear_model.Ridge):
                        reponseBest = self.dicoFonction[output]['regressionBEST'].predict(matriceBEST[0])
                    elif isinstance(self.dicoFonction[output]['regressionBEST'],
                                    sklearn.linear_model.LogisticRegression):
                        reponseBest = ["%.2f" % float(i * 100) for i in
                                       self.dicoFonction[output]['regressionBEST'].predict_proba(matriceBEST[0])[0]]
                    elif isinstance(self.dicoFonction[output]['regressionBEST'],
                                    sklearn.linear_model.LogisticRegressionCV):
                        reponseBest = ["%.2f" % float(i * 100) for i in
                                       self.dicoFonction[output]['regressionBEST'].predict_proba(matriceBEST[0])[0]]
                    else:
                        reponseBest = self.dicoFonction[output]['regressionBEST'].predict(matriceBEST[0])
                    if outputInServer:
                        try:
                            reponseServer = ["%.2f" % float(i * 100) for i in
                                             self.dicoFonction[output]['regressionSERVER'].predict_proba(
                                                 matriceSERVER[0])[0]]
                        except Exception, e:
                            reponseServer = [0, 0]
                    else:
                        reponseServer = 0
                    dicoReponse[output] = [reponseBest, reponseNormal, reponseServer]
                    if isinstance(reponseBest, list):
                        dicoReponseREMEMBER[output] = reponseBest[1]
                    else:
                        dicoReponseREMEMBER[output] = reponseBest
                except Exception, e:
                    print e
                    exit()
        # met a jour le min et max selon les reponses obtenus
        if self.first:
            self.initStatutAndMinMax(dicoReponse)
            self.first = False
        self.updateMinMax(dicoReponse)
        self.updateState(dicoReponse)

        self.lastReponse.append(dicoReponseREMEMBER)
        return dicoReponse
