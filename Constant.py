#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
DIRPATH =os.path.dirname(os.path.realpath(__file__))
#dossier d'enregistrement des tests
DIRTESTRECORD = DIRPATH + '/TestRecord'

#dossier d'enregistrement des fichier csv associé au test
DIRDATACSV = DIRPATH + '/Log/Data/'

#fichier pickle contenant tout les indicateur avec leur fonction lambda associé
DIRALLINDICATEUR = DIRPATH + '/Dictionnaire/indicateur.p'

#fichier pickle contenant une liste des indicateur classé par lvl
DIRINDICATEURLVL =DIRPATH + '/Dictionnaire/listeIndicateurName.p'

#fichier contenant tout les noms d'utilisateur actuelle  
DIRALLUSER = DIRPATH + '/Compte/ListUser.txt'

#fichier refreshData contenant tout les fichier test créer en attente de mise a jour d'indicateur
DIRREFRESHDATAFILE =DIRPATH + '/Log/RefreshData/refreshData.txt'

#dossier server 
DIRSERVER = DIRPATH + '/Server/'
